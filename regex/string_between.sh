#!/bin/bash
# Return string between two strings
in="a b start c d e end f g"
out=$(grep -oP '(?<=start).*(?=end)' <<< $in)

echo "IN: $in"
echo "OUT: $out"
echo "--------"

in=$'a b start c d e end f g \na b start c d e end f g'
out=$(grep -oP '(?<=start).*(?=end)' <<< $in)

echo -e "IN: \n$in"
echo -e "OUT: \n$out"
echo "-------"

in=$'a b start c d e end f g \n a b start c d e end f g'
out=$(grep -oP '(?<=start).*(?=end)' <<< $in)

echo -e "IN: \n$in"
echo -e "OUT: \n$out"
echo "-------"

in=$'a b c d start e \na b end c  e f g'
out=$(grep -oP '(?<=start).*(?=end)' <<< $in)

echo -e "IN: \n$in"
echo -e "OUT: \n$out"


