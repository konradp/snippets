# java
I'm new to java, so dropping my notes here

- javac: compiler
- .java: extension

## Compile and run
Compile
```
javac HelloWorld.java
```

It produces `HelloWorld.class`. Run it.

```
$ java HelloWorld
Hello World
```


## Troubleshoot
Running an app gives.
$ java HelloWorld
Error: LinkageError occurred while loading main class HelloWorld
        java.lang.UnsupportedClassVersionError: HelloWorld has been compiled by a more recent version of the Java Runtime (class file version 61.0), this version of the Java Runtime only recognizes class file versions up to 55.0
```

Need to ensure that the java and javac versions align

```
sudo update-alternatives --config java
sudo update-alternatives --config javac
```
