# Desc: For a given list of pairs, find the smaller list which covers the same range
# See: Set cover problem
# Author: Konrad Pisarczyk (gitlab.com/konradp)

# Includes
import json
import sys

# Note: Here, we define (2, 4) as the set {2, 3, 4}
# i.e. (1, 2) and (2, 3) overlap, because both contain '2'

# USAGE
# ./ranges.py

# SETTINGS
# Uncomment a single 'pairs' variable to test
pairs = [(1, 2), (2, 3), (4, 5)]
#out: [(1,3), (4, 5)]

#pairs = [(1, 4), (2, 3), (4, 5), (6, 7)]
#out: [(1, 5), (6, 7)]

#pairs = [(1,2), (3, 4), (4, 5), (6, 7)]
#out: [(1, 2), (3, 5), (6, 7)]

#pairs = [(1, 2)]
#out: [(1, 2)]

# MAIN
return_pairs = []
print("Input:", pairs)
print("------")

# Sort pairs by the first element
pairs.sort(key=lambda tup: tup[0])
print("Sorted input: %s" %pairs)

j_idx = 0
for i_idx, i in enumerate(pairs):
    if j_idx > i_idx:
        # Skip pairs already processed by inner loop
        print("INFO: skipping j")
        continue
    # /if

    print("INFO: i: ", i)
    min_val = i[0]
    max_val = i[1]

    for j in pairs[i_idx + 1:]:
        print("INFO: j:", j)
        j_idx += 1

        # check if j extends i
        # if i,j disjoint; break out
        if j[0] > max_val:
            print("INFO: breaking")
            break
        # /if

        min_val = min(i[0], j[0])
        max_val = max(i[1], j[1])
        print("INFO: current min/max: (%s, %s)" % (min_val, max_val))
    # /for

    print("INFO: final min/max: (%s, %s)" %(min_val, max_val))
    return_pairs.append((min_val, max_val))
# /for

# Print out the final set of pairs
print("------")
print("Output:", return_pairs)

