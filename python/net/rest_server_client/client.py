# Adapted from https://requests.readthedocs.io/en/latest/user/quickstart/
from requests import get, post

URL = 'http://localhost:5000'

print('GET')
r = get(f'{URL}/')
print(r.url)
print(r.status_code)
print(r.headers)
print(r.text)
print()

print('GET with urlencoded params')
r = get(f'{URL}/get_with_params',
  params={
    'key': 'value',
})
print(r.url)
print(r.text)
print()


print('GET json')
r = get(f'{URL}/get_json')
print(r.text)
print(r.json())
print(r.json()['key'])
print()


print('GET with custom header')
r = get(f'{URL}/print_header', headers={
  'user-agent': 'Made-up browser',
})
print(r.text)
print()


print('POST')
r = post(f'{URL}/post',
  data={
    'key': 'value',
})
print(r.text)
print()


print('POST with JSON')
r = post(f'{URL}/post_json',
  headers={
    'Content-Type': 'application/json',
  },
  json={
    'key': 'value',
})
print(r.text)
print()
