# Adapted from https://flask.palletsprojects.com/en/2.2.x/quickstart/
# Not all functions from quickstart are covered, only those required by
# client.py
# listen on port 5000
import json

from flask import Flask, request
from markupsafe import escape
from flask_httpauth import HTTPBasicAuth

app = Flask(__name__)

@app.route('/')
def hello():
  return 'Hello World!'


@app.route('/get_with_params')
def get_with_params():
  value = request.args.get('key')
  return f'Argument supplied: {value}'


@app.route('/get_json')
def get_json():
  return json.dumps({
    'key': 'value'
  })

@app.route('/print_header')
def print_header():
  headers = request.headers.to_wsgi_list()
  return json.dumps(headers)


@app.route('/post', methods=['POST'])
def post():
  name = request.form['key']
  return f'Hello {name}'


@app.route('/post_json', methods=['POST'])
def post_json():
  data = request.get_json()
  return json.dumps({
    'data': data,
  }, indent=2)


if __name__ == '__main__':
  app.run()
