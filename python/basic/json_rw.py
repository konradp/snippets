import json

with open('/tmp/test', 'r') as f:
  print('reading----------')
  #content = f.read()
  #print(content)
  #conids = json.loads(content)
  conids = json.load(f)
  print(conids)
  # actually, use 'r+' for both reading and writing
  with open('/tmp/test', 'w') as f:
    conids['a'] = 'b'
    print(conids)
    print(json.dumps(conids))
    f.write(json.dumps(conids))
