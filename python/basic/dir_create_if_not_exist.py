# https://www.tutorialspoint.com/How-can-I-create-a-directory-if-it-does-not-exist-using-Python
import os
def mkdir(path):
  if not os.path.exists(path):
    os.makedirs(path)

if __name__ == '__main__':
  mkdir('/tmp/test')
