import pprint
a = {
  'one': {
    'one_1': '11',
    'one_2': '12'
  }
}
b = {
  'two': 'TWO',
}
c = {
  'one': {
    'one_3': '13'
  }
}
print('a')
pprint.pprint(a)
print('b')
pprint.pprint(b)

print('update a with d')
a.update({ 'one': {'one_1': '11_new'}})
pprint.pprint(a, indent=2)
