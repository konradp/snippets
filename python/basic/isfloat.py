# Given an input, check if it's a float or not
# run with ./script.py

def is_float(i):
  try:
    if str(float(i)) == i:
      return True
    else:
      raise Exception()
  except Exception as e:
    return False

# MAIN
myvar = raw_input('What? ')
if isinstance(myvar, float):
  print('yes')
else:
  print('no')
if is_float(myvar):
  print('You got a float: %g' %float(myvar))
else:
  print('Not a float')
