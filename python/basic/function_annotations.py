# https://peps.python.org/pep-3107/
# https://peps.python.org/pep-0484/
# https://docs.python.org/3/library/typing.html
# https://realpython.com/lessons/annotations/

import sys

def greeting(name: str) -> str:
  return 'Hello ' + name


def greeting_default(name: str = 'stranger') -> str:
  return 'Hello ' + name

def get_list_repeat(name: str, number: int) -> list:
  # return a list of the name repeated a number of times
  ret = []
  for i in range(number):
    ret.append(name)
  return ret

def main():
  print(greeting('Konrad'))
  print(greeting_default())
  print(get_list_repeat('name', 3))

if __name__ == '__main__':
  sys.exit(main())
