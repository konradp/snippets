from dataclasses import dataclass

@dataclass
class Stock:
  ticker: str = None
  name: str = None

@dataclass
class StockAcceptDict:
  ticker: str = None
  name: str = None
  def __post_init__(self):
    if type(self.ticker) is dict:
      tmp = self.ticker
      self.ticker = tmp['ticker']
      self.name = tmp['name']

## TESTS
print('# s = Stock():')
s = Stock()
print(s)
print()

print("# This doesn't work right, name=None")
print("# s = Stock({'ticker': 'my ticker', 'name': 'my name'})")
s = Stock({'ticker': 'my ticker', 'name': 'my name'})
print(s)
print()

print("# This works fine")
print("# s = StockAcceptDict({'ticker': 'my ticker', 'name': 'my name'})")
s = StockAcceptDict({'ticker': 'my ticker', 'name': 'my name'})
print(s)
print()

print("# Regular use of dataclass")
print("# s = StockAcceptDict(ticker='my ticker', name='my name')")
s = StockAcceptDict(ticker='my ticker', name='my name')
print(s)
print()
