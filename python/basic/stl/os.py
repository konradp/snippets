import os

def run(cmd):
  print(f'\n=== {cmd}')
  exec(f'print({cmd})')

run('os.ctermid()')
run('os.environ')

# os.chdir(path)
# os.fchdir(fd)
# os.getcwd()

print('\n=== os.getenv(key, default=None)')
print("=== os.getenv('HOME')")
print(os.getenv('HOME'))

run('os.get_exec_path()')
run('os.getegid()')
run('os.geteuid()')
run('os.getgid()')
print('=== os.getgrouplist(user, group, /)')
run('os.getgrouplist("konrad", 1000)')
run('os.getgroups()')
run('os.getlogin()')
run('os.getpgid(0)')
run('os.getpgrp()')
run('os.getpid()')
run('os.getppid()')
# os.getpriority(which, who)
run('os.getresuid()')
print('real, effective, saved')
run('os.getresgid()')
run('os.getuid()')
# os.initgroups(username, gid, /)¶
# os.putenv(key, value, /)
# os.setegid(egid, /)
# os.seteuid(euid, /)
# os.setgid(gid, /)
# os.setgroups(groups, /)
# os.setpgrp()
# os.setpgid(pid, pgrp, /)
# os.setpriority(which, who, priority)
# os.setregid(rgid, egid, /)
# os.setresgid(rgid, egid, sgid, /)
# os.setresuid(ruid, euid, suid, /)
# os.setreuid(ruid, euid, /)
# os.getsid(pid, /)
# os.setsid()
# os.setuid(uid, /)
# os.strerror(code, /)
# os.umask(mask, /)
run('os.uname()')
# os.unsetenv(key, /)

# FILE OBJECT CREATION
print('\n\n===== FILE OBJECT CREATION')
# os.fdopen(fd, *args, **kwargs) # alias of 'open()'


# FILE DESCRIPTOR OPERATIONS
print('\n\n===== FILE DESCRIPTOR OPERATIONS')
# os.close(fd) # low-level, use the file's close() instead
# os.closerange(fd_low, fd_high, /)
# os.copy_file_range(src, dst, count, offset_src=None, offset_dst=None)
# os.device_encoding(fd)
# os.dup(fd, /)
# os.dup2(fd, fd2, inheritable=True)
# os.fchmod(fd, mode) # same as os.chmod(fd, mode)
# os.fchown(fd, uid, gid) # same as os.chown(fd, uid, gid)
# os.fdatasync(fd)
# os.fpathconf(fd, name, /)
# os.fstat(fd) # same as of.stat(fd)
# os.fstatvfs(fd, /) # same as of.statvfs(fd)
# os.fsync(fd)
# os.ftruncate(fd, length, /) # same as os.truncate(fd, length)
# os.get_blocking(fd, /)
# os.isatty(fd, /)
# os.lockf(fd, cmd, len, /)
# os.login_tty(fd, /)
# os.lseek(fd, pos, how, /)
# os.open(path, flags, mode=0o777, *, dir_fd=None) # low-level, use open() instead
# os.openpty()
# os.pipe()
# os.pipe2(flags, /)
# os.posix_fallocate(fd, offset, len, /)
# os.posix_fadvise(fd, offset, len, advice, /)
# os.pread(fd, n, offset, /)
# os.preadv(fd, buffers, offset, flags=0, /)
# os.pwrite(fd, str, offset, /)
# os.pwritev(fd, buffers, offset, flags=0, /)
# os.read(fd, n, /) # low-level, use open() and read(), readline() instead
# os.sendfile(...)
# os.set_blocking(fd, blocking, /)
# os.splice(...)
# os.readv(...)
# os.tcgetpgrp(...)
# os.tcsetpgrp(...)
# os.ttyname(...)
# os.write(...) # low-level, use open() instead
# os.writev(...)
# os.get_terminal_size(...) # low-level, use shutil.get_terminal_size() instead


# INHERITANCE OF FILE DESCRIPTORS: SKIPPED
print('\n\n===== INHERITANCE OF FILE DESCRIPTORS: SKIPPED')

# FILES AND DIRECTORIES
print('\n\n===== FILES AND DIRECTORIES: SKIPPED')
print('os.chdir(path)')
# os.chflags
print('os.chmod(path, mode, *, dif_fd=None, follow_symlinks=True)')
print('os.chown(path, uid, gid, *, dif_fd=None, follow_symlinks=True), see shutil.shown() for higher-level fn')
print('os.chroot(path)')
# os.fchdir(fd) # same as os.chdir(fd)
print('os.getcwd()')
# os.getcwdb()
# os.lchflags(...)
# os.lchmod(...)
# os.lchown(...) # same as os.chown(path, uid, gid, follow_symlinks=False)
# os.link(...) # hard link
print("os.listdir(path='.'), see a better scandir()")
# os.lstat() # like stat() but don't follow symbolic links
print('os.mkdir(path, mode=0o777, *, dir_fd=None)')
print('os.makedirs(name, mode=0o777, exist_ok=False), recursive')
print('os.mkfifo(path, mode=0o666, *, dir_fd=None), named pipe')
print('os.mknod(path, mode=0o600, device=0, *, dir_fd=None)')
# os.major(...)
# os.minor(...)
# os.makedev(...)
# os.pathconf(...)
# os.readlink(...)
# os.remove(...) # same as unlink()
print('os.removedirs(name), recursive')
print('os.rename(src, dst, *, src_dir_fd=None, dst_dir_fd=None)')
# os.renames(...)
# os.replace(...)
print('os.rmdir(path, *, dir_fd=None)')
print("os.scandir(path='.')")
print('os.stat(path, *, dir_fd=None, follow_symlinks=True)')
# os.statvfs(path)
print('os.symlink(src, dst, ...)')
# os.sync()
# os.truncate(...)
# os.unlink(...) # use remove() instead
# os.utime(...)
print('oswalk(top, topdown=True, onerror=None, followlinks=False)')
# os.fwalk(...)
# os.memfd_create(...)
# os.eventfd(...)
# os.eventfd_read(...)
# os.eventfd_write(...)


# LINUX EXTENDED ATTRIBUTES
print('\n\n===== LINUX EXTENDED ATTRIBUTES')
print('os.getxattr(...)')
print('os.listxattr(...)')
print('os.removexattr(...)')
print('os.setxattr(...)')


# PROCESS MANAGEMENT
print('\n\n===== PROCESS MANAGEMENT')
# os.abort()
# os.add_dll_directory(...)
# os.execl
# os.execle
# os.execlp
# os.execlpe
# os.execv
# os.execve
# os.execvp
# os.execvpe
# os._exit(n)
print('os.fork()')
print('os.forkpty()')
print('os.kill(pid, sig, /)')
print('os.killpg(pgid, sig, /)')
print('os.nice(increment, /)')
# os.pifd_open(...)
# os.plock(...)
# os.popen(...)
# os.posix_spawn(...) # use subprocess.run() instead
# os.posix_spawnp(...)
# os.register_at_fork(...)
# os.spawn*
# os.startfile(...)
print('os.system(command)')
print('os.times()')
print('os.wait()')
# os.waitid(idtype, id, options, /)
print('os.waitpit(pid, option, /)')
# os.wait3(...)
# os.wait4(...)
# os.waitstatus_to_exitcode(status)

# INTERFACE TO THE SCHEDULER: SKIPPED

print('MISC SYS INFO')
print('os.confstr(name, /), see confstr_names dict')
print('os.confstr_names dict')
print('os.cpu_count()')
print('os.getloadavg()')
print('os.sysconf(name, /), see os.sysconf_names dict')
print('os.sysconf_names dict')

print('DATA VALUES')
print('os.curdir')
print('os.pardir')
print('os.sep')
print('os.altsep')
print('os.extsep')
print('os.pathsep')
print('os.defpath')
print('os.linesep')
print('os.devnull')

print('\n\n===== RANDOM NUMBERS')
print('os.getrandom(size, flags=0)')
print('os.urandom(size, /), suitable for crypto')
