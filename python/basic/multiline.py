s = """
  SELECT a
  FROM b
  Where c
"""
t = """
  SELECT a
  FROM b
  Where c is %s
"""
three = '3'
u = f"""
  SELECT a
  FROM b
  Where c is {three}
"""
print(s)
print(t %'two')
print(u)
