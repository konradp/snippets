import json

class Cat:
  # Property classes
  class Coat:
    BLACK = 'Black'
    TABBY = 'Tabby'
  class Breed:
    BENGAL = 0
    PERSIAN = 1

  # Properties
  coat = Coat
  breed = Breed

  def __init__(self, **kwargs):
    # Constructor
    for key, value in kwargs.items():
      setattr(self, key, value)

  def to_json(self):
    return(json.dumps(vars(self)))

# Main
cat = Cat(
  coat=Cat.Coat.BLACK,
  breed=Cat.Breed.BENGAL,
)
print(cat.to_json())
