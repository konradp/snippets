# Given a value X and a percentage value P, find two numbers (L and H) such
# that: (p is a percentage, e.g. 4/100)
#   h = l + lp
# i.e.
#   h = l(1+p)     <- (1)
# and
#   (l + h)/2 = x  <- (2)
# 
# # Solution:
# From 2,
#   l = 2x-h <- (2a)
#   h = 2x-l <- (2b)
# 
# Substitute 2b into 1:
#    2x-l = l(1+p)
# => l(2+p) = 2x
# => l = 2x/(2+p)  <- (4)
#
# Substitute 2a into 1:
#    h = 2x - h + (2x-h)p
# => 2h + hp = 2x + 2xp
# => h(2+p) = 2x(1+p)
# => h = 2x(1+p)/(2+p)  <- (3)
# 
# So
# h = 2x(1+p)/(2+p)  <- (3)
# l = 2x/(2+p)       <- (4)
# 
# # Test
# l = 100
# x = 102
# h = 104
# p = 4/100
# # Solution
# h = 204(1+4/100)/(2+4/100)
#   = 104
# l = 204/(2+4/100)
#   = 100

def get_range_prices(average, percentage):
  # input:
  # - average: number, e.g. 102
  # - percentage: number, e.g. 4/100
  # output:
  # - a pair of numbers:
  #   - low price
  #   - high price
  x = average
  p = percentage
  return ( 2*x/(2+p), 2*x*(1+p)/(2+p) )

# Main
avg = 102
perc = 4/100
price_range = get_range_prices(avg, perc)
low = price_range[0]
high = price_range[1]
print('low', low)
print('high', high)
