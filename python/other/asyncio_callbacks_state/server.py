from pythonosc.dispatcher import Dispatcher
from pythonosc.osc_server import AsyncIOOSCUDPServer
import asyncio
import mido
import threading as th
import time
# This contains of:
# - OSC server
# - MIDI listener


osc_ip = '127.0.0.1'
osc_port = 9000
DEV = 'Arturia MiniLab mkII:Arturia MiniLab mkII MIDI 1 20:0'
state_global = 'state_global value'


# Message handlers
def osc_handle_msg(address, *args):
  print(f"DEFAULT {address}: {args}")
  print('state_global in OSC handler:', state_global)
  state_global = 'value from OSC'
def midi_handle_msg(msg):
  print('msg', msg)
  #print('state_global in MIDI handler:', state_global)
  #state_global = 'value from MIDI'


async def loop():
  # Main program loop
  for i in range(10000):
    await asyncio.sleep(1)


async def init_main():
  state_global = 'value from init_main'
  # OSC listener
  dispatcher = Dispatcher()
  dispatcher.set_default_handler(osc_handle_msg)
  osc_server = AsyncIOOSCUDPServer((osc_ip, osc_port),
    dispatcher, asyncio.get_event_loop())
  transport, protocol = await osc_server.create_serve_endpoint()
  await loop()
  transport.close()


if __name__ == '__main__':
  state_global = 'value from __main__'
  # MIDI listener
  port = mido.open_input(DEV, callback=midi_handle_msg)
  asyncio.run(init_main())
