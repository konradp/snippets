# osc_and_midi
This example combines MIDI and OSC (with asyncio). Here I study the variable scope and how to manage the state.

Install requirements.
```
pip3 install -r requirements.py
```
