import mido

class ArturiaColors:
  def __init__(self, dev):
    # TODO: Maybe ArturiaColours should inherit from mido.output
    self.output = mido.open_output(dev)


  def set_pad_color(pad, color):
    msg = self._color_msg(pad, color)
    self.output.send(msg)

  def _color_msg(pad, color):
    # create a MIDI colour message
    # pad = 0,1,2,...
    # color: black/red/green/yellow/blue/magenta/cyan/white
    colors = {
      'black': 0x00,
      'red': 0x01,
      'green': 0x04,
      'yellow': 0x05,
      'blue': 0x10,
      'magenta': 0x11,
      'cyan': 0x14,
      'white': 0x7F,
    }
    if color in colors:
      color = colors[color]
    else:
      return Exception('No such color')
    pad = 0x70 + pad
    msg = mido.Message('sysex', data=[
      0x00,0x20,0x6B,0x7F,0x42,0x02,0x00,0x10,pad,color,
    ])
    return msg

