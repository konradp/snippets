from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.dispatcher import Dispatcher
import asyncio

state_global = 'Some value'

def osc_handle_msg(address, *args):
  global state_global
  print('state_global in OSC handler:', state_global)
  state_global = 'New value'

async def loop():
  for i in range(10000):
    await asyncio.sleep(1)

async def init_main():
  dispatcher = Dispatcher()
  dispatcher.set_default_handler(osc_handle_msg)
  svr = AsyncIOOSCUDPServer(('127.0.0.1', 9000),
    dispatcher, asyncio.get_event_loop())
  transport, protocol = await svr.create_serve_endpoint()
  await loop()
  transport.close()

if __name__ == '__main__':
  state_global = 'value from __main__'
  asyncio.run(init_main())
