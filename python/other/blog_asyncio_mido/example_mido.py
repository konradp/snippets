import mido
import sys
import time

state = 'Some state value'

def midi_handle_msg(msg):
  global state
  print('msg', msg)
  print('midi_handle_msg: state:', state)
  state = 'New value'

if __name__ == '__main__':
  if len(sys.argv) != 2:
    print('MIDI devices:')
    devices = mido.get_input_names()
    for device in devices:
      print('-', device)
    exit(1)
  dev = sys.argv[1]
  port = mido.open_input(dev, callback=midi_handle_msg)
  while True:
    time.sleep(1)
