# Token from: https://support.atlassian.com/atlassian-account/docs/manage-api-tokens-for-your-atlassian-account/
# https://id.atlassian.com/manage-profile/security/api-tokens

import requests
from requests.auth import HTTPBasicAuth

# Your JIRA details
jira_url = ''
api_endpoint = '/rest/api/3/search'
username = ''
api_token = ''

# JQL to find issues with comments by you
#jql = 'comment.author = currentUser()'
#jql = 'assignee = currentUser() AND issueFunction in commented("by currentUser()")'
jql = 'assignee = currentUser() OR reporter = currentUser()'

url = f'{jira_url}{api_endpoint}'
headers = {
    'Accept': 'application/json'
}
auth = HTTPBasicAuth(username, api_token)
params = {
    'jql': jql,
    'fields': 'summary,comment',
    'maxResults': 3000,
}
response = requests.get(url, headers=headers, params=params, auth=auth)

# Process the response
if response.status_code == 200:
  issues = response.json()['issues']
  for issue in issues:
      #print(issue)
      for comment in issue['fields']['comment']['comments']:
          if comment['author']['emailAddress'] == username:
              print(f"==== {issue['key']} {issue['fields']['summary']}")
              print(comment['created'])
              #print(comment['body']['content'])
              for item in comment['body']['content']:
                  if item['type'] == 'paragraph':
                      for inner_item in item['content']:
                          if inner_item['type'] == 'text':
                              print(inner_item['text'])
                  #print(item)
                  #if item['type'] == 'text':
                  #    print(item['text'])
              #print(comment)
else:
  print(f"Error: {response.status_code} - {response.text}")

