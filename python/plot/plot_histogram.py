#!/usr/bin/env python3
import matplotlib.pyplot as plt
import math
import sys

if len(sys.argv) < 3:
  print('ERROR: Insufficient number of arguments')
  print('Usage: ./script FILE BINS [MAX]')
  exit(1)
F_NAME = sys.argv[1]
BINS = int(sys.argv[2])
max_defined = False
if len(sys.argv) == 4:
  max_defined = True
  MAX = float(sys.argv[3])

with open(F_NAME,'r') as f:
  x = [float(line.rstrip('\n')) for line in f]
  if max_defined:
    x = [line for line in x if line <= MAX]

print(x)

# Plot
plt.clf()
counts, bin_edges, patches = plt.hist(x, bins=BINS, edgecolor='black', color='lightgray')
total = sum(counts)  # Total count for percentage calculation
for count, edge, patch in zip(counts, bin_edges[:-1], patches):
    # Calculate the x position (center of the bar)
    x_position = edge + (bin_edges[1] - bin_edges[0]) / 2
    # Add the label with count and percentage
    percentage = (count / total) * 100
    plt.text(x_position, count, f'{int(count)}\n({percentage:.1f}%)', 
             ha='center', va='bottom', fontsize=8, color='black')


plt.ylabel('count')
plt.xlabel('price')
plt.tight_layout()
plt.savefig('histogram.png')
plt.show()
