import datetime
import matplotlib.dates as mdates
import matplotlib.pyplot as plot

dates = [
  '2020-01-01',
  '2020-01-02',
  '2020-01-03',
  '2020-01-04',
]
x = [datetime.datetime.strptime(d,'%Y-%m-%d').date() for d in dates]
y = [ 1 for i in range(4) ]
y2 = [ 2 for i in range(4) ]
axes = plot.gca() # Get axes
formatter = mdates.DateFormatter('%Y-%m-%d')
axes.xaxis.set_major_formatter(formatter)

plot.scatter(x, y)
plot.scatter(x, y2)
plot.show()
