import json
import matplotlib.pyplot as plt
import math
import pprint
import sys

field_pick = 'industry'

if len(sys.argv) < 2:
  print('Provide a file and max number to show')
  print('./script FILE MAX')
  exit(1)
fName = sys.argv[1]

field_count = {}
with open(fName,'r') as f:
  data = json.loads(f.read())
  for _, info in data.items():
    value = info[field_pick]
    if value not in field_count:
      field_count[value] = 0
    field_count[value] = field_count[value] + 1

field_count_sorted = sorted(field_count.items(), key=lambda x: x[1], reverse=True)
#industries = field_count_sorted[0:]
pprint.pprint(field_count_sorted)
x = field_count_sorted
labels = [ i[0] for i in x]
values = [ i[1] for i in x]

# PLOT
plt.clf()
fig1, ax1 = plt.subplots()
ax1.pie(values, labels=labels, startangle=90, autopct='%1.1f%%')
ax1.axis('equal')
#plt.ylabel('count')
#plt.xlabel('price')
plt.savefig('out.png')
plt.show()
