#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import sys
from matplotlib.patches import Patch

if len(sys.argv) < 3:
  print('ERROR: Insufficient number of arguments')
  print('Usage: ./script FILE TITLE [MAX]')
  exit(1)
F_NAME = sys.argv[1]
TITLE = sys.argv[2]
max_defined = False
if len(sys.argv) == 4:
  max_defined = True
  MAX = int(sys.argv[3])


with open(F_NAME,'r') as f:
  data = [float(line.rstrip('\n')) for line in f]
  if max_defined:
    data = [line for line in data if line <= MAX]

#data = [
#    np.random.normal(70, 5, 200)
#]

plt.figure(figsize=(8, 2))
box = plt.boxplot(data, vert=False, patch_artist=True, boxprops=dict(facecolor='lightgray'))

# Labels
whisker_min = box['whiskers'][0].get_xdata()[1]
whisker_max = box['whiskers'][1].get_xdata()[1]

box_path = box['boxes'][0].get_path().vertices
q1 = box_path[0, 0]
q3 = box_path[2, 0]
median = box['medians'][0].get_xdata()[0]

stats_labels = [
    f'min: {whisker_min:.1f}',
    f'Q1: {q1:.1f}',
    f'median: {median:.1f}',
    f'Q3: {q3:.1f}',
    f'max: {whisker_max:.1f}',
]
plt.gca().legend(
    labels=stats_labels,
    loc='center left',
    #title='Statistics',
    fontsize=10,
    handletextpad=0,
    handlelength=0,
    frameon=False,
    bbox_to_anchor=(1, 0.5),
)


# Axes
plt.yticks([1], [''])
#plt.xlabel('values')
plt.xlabel(TITLE)
#plt.title('Box Plot Example')
plt.title(F_NAME)
plt.tight_layout()
plt.savefig('boxplot.png')
#plt.show()
