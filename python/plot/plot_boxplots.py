#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np

# Example data
data = [
    np.random.normal(50, 10, 200),  # Category 1: Mean=50, SD=10
    np.random.normal(60, 15, 200),  # Category 2: Mean=60, SD=15
    np.random.normal(70, 5, 200)    # Category 3: Mean=70, SD=5
]

# Create a box plot
plt.boxplot(data, patch_artist=True, boxprops=dict(facecolor='lightblue'))

# Add labels
plt.xticks([1, 2, 3], ['Category 1', 'Category 2', 'Category 3'])  # X-axis labels
plt.ylabel('Values')  # Y-axis label
plt.title('Box Plot Example')  # Title

# Show the plot
plt.show()
