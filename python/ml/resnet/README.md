Notes from
https://arxiv.org/pdf/1809.04356.pdf

# Residual Network
- 11 layers
  - 9 layers: convolutional, in three residual blocks
  - GAP layer: average time series across time dimension
  - softmax: # of neurons = # of classes

notes:
- shortcut residual connection between consec. conv. layers
- linear shortcut is added to link the output of residual block to its input, so gradient flows directly through these connections <- reduces vanishing gradient effect
- each convolution has 64 filters, and ReLU activaion, preceeded by batch normalisation

- 1st convolution: filter length = 8
- 2nd convolution: filter length = 5
- 3rd convolution: filter length = 3
