from pythonosc.dispatcher import Dispatcher
from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.udp_client import SimpleUDPClient
import asyncio

# server is the SooperLooper
# client is our app listener
# osc_client is our app client connecting to SL
# osc_server is our app listener

ip_server = '127.0.0.1'
port_server = 9951

ip_client = '127.0.0.1'
port_client = 9000
addr_client = f"{ip_client}:{port_client}"

def send_ping():
  # Client
  osc_client = SimpleUDPClient(ip_server, port_server)
  osc_client.send_message("/ping", [
    f"{addr_client}",
    "/ping"
  ])

# Server
def handle_ping(address, *args):
  print(f"{address}: {args}")

def handle_default(address, *args):
  print(f"DEFAULT {address}: {args}")

async def loop():
  # Main program loop
  for i in range(10):
    print(f"Loop {i}")
    if i == 2:
      print('Sending')
      send_ping()
    await asyncio.sleep(1)


async def init_main():
  # Server
  osc_server = AsyncIOOSCUDPServer((ip_client, port_client),
    dispatcher, asyncio.get_event_loop())
  transport, protocol = await osc_server.create_serve_endpoint()
  await loop()
  transport.close()


## MAIN
dispatcher = Dispatcher()
dispatcher.map('/ping', handle_ping)
dispatcher.set_default_handler(handle_default)

asyncio.run(init_main())
