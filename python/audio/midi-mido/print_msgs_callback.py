import mido
import pprint
import time

DEV = 'Arturia MiniLab mkII:Arturia MiniLab mkII MIDI 1 20:0'

def print_msg(msg):
  print('msg', msg)

port = mido.open_input(DEV, callback=print_msg)

  # Receive single message
  #msg = inport.receive()
  #for msg in inport:
  #  print('msg', msg)
  # Non-blocking
  #for msg in inport.iter_pending():
  #  print('msg', msg)

#  for msg in inport:
#    print('msg', msg)
while True:
  time.sleep(1)

