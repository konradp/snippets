import mido
import threading as th
import time

DEV = 'Arturia MiniLab mkII:Arturia MiniLab mkII MIDI 1 20:0'
COUNT_PADS = 8
DURATION_LONG_PRESS = 0.5
state_pads = [False]*COUNT_PADS
pad_timers = [None]*COUNT_PADS

def check_longpress(pad):
  if state_pads[pad]:
    print('long press on', pad)

def print_msg(msg):
  pad = msg.note - 35 - 1 # 0, 1, 2
  if 0 <= pad <= 7 \
    and (msg.type in ['note_on', 'note_off' ]):
    if msg.type == 'note_on':
      pad_timers[pad] = th.Timer(DURATION_LONG_PRESS,
        check_longpress, None, { 'pad': pad }
      )
      pad_timers[pad].start()
      state_pads[pad] = True
    elif msg.type == 'note_off':
      pad_timers[pad].cancel()
      state_pads[pad] = False

# MAIN
port = mido.open_input(DEV, callback=print_msg)

while True:
  time.sleep(1)
