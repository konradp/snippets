import mido
import pprint

DEV = 'Arturia MiniLab mkII:Arturia MiniLab mkII MIDI 1 20:0'

with mido.open_input(DEV) as inport:
  pprint.pprint(mido.get_input_names())

  # Receive single message
  #msg = inport.receive()
  #for msg in inport:
  #  print('msg', msg)
  # Non-blocking
  #for msg in inport.iter_pending():
  #  print('msg', msg)

  for msg in inport:
    print('msg', msg)

