import mido
import pprint
import time

# color:
# 00 - black (off)
# 01 - red
# 04 - green
# 05 - yellow
# 10 - blue
# 11 - magenta
# 14 - cyan
# 7F - white

DEV = 'Arturia MiniLab mkII:Arturia MiniLab mkII MIDI 1 20:0'

def print_msg(msg):
  print('msg', msg)

def color_msg(button, color):
  button = 0x70 + button
  msg = mido.Message('sysex', data=[
    0x00,
    0x20,
    0x6B,
    0x7F,
    0x42,
    0x02,
    0x00,
    0x10,
    button,
    color,
  ])
  return msg

outputs = mido.get_output_names()
pprint.pprint(outputs)

port = mido.open_output(outputs[1])
button = 2
color = 0x10
msg = color_msg(button, color)
port.send(msg)
time.sleep(2)
