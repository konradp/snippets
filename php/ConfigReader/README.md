# ConfigReader
A PHP snippet supporting various ways of supplying a config file.

It supports the below ways of specifying the config file path:

- Path passed to the constructor
- A named constant passed to constructor
