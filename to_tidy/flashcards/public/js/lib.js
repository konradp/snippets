function getUrl(url, isJson) {
  var req = new XMLHttpRequest();
  return new Promise((resolve, reject) => {
    req.onreadystatechange = function() {
      //authReceived = true;
      if (req.readyState == 4) {
      } else {
        return;
      }
      if (req.status >= 200 && req.status < 300) {
        var r = req.responseText;
        try {
          if (isJson) {
            resolve(JSON.parse(r));
          } else {
            resolve(r);
          }
        } catch(err) {
          reject('Invalid data: ' + r + err);
        }
      } else {
        reject({
          status: req.status,
          statusText: req.statusText
        });
      }
    };
    req.open('GET', url);
    req.send();
  });
};
