// Three modes:
// - settings/home
// - practice + summary screen
// - test + summary screen
// Settings/home:
// - choose which mode: practice/test
// - settings

class App {
  cards = null;
  cardIndex = null;
  cardRevealed = false;
  cardContentHint = null; // 0, 1, 2, 3, e.g. 1
  cardContentRevealed = null; // e.g. [ 2, 3 ]
  settings = {
    mode: null, // practice/test
    number: null, // 10/10-20/all
    repeat: null, // 1
    hint: null // 0
  };

  constructor() {
    // Load cards and show settings view
    // Load cards
    this.GetCards('HSK1')
      .then((cards) => {
        this.cards = cards;
        console.log(cards);
        // Show home/config
        this.GetTemplate('config')
          .then((template) => {
            // Render template
            var view = document.getElementById('content');
            view.innerHTML = template;
            // Override hint card options from template with values from 'cards'
            var optsHtml = document.getElementById('hintoptions');
            var opts = this.cards.components;
            // Clear test options
            optsHtml.innerHTML = '';
            var isFirst = true;
            var i = 0;
            opts.forEach((e) => {
              var div = document.createElement('div');
              // Input
              var input = document.createElement('input');
              input.id = 'hintcard' + i;
              input.type = 'radio';
              div.appendChild(input);
              if (i == 0) {
                input.checked = 'true';
              }
              // Label
              var label = document.createElement('label');
              label.for = 'hintcard' + i;
              label.innerHTML = e;
              div.appendChild(label);
              // Append all
              optsHtml.appendChild(div);
              i++;
            })
          }) //template
      }) //cards
  }

  // Control functions
  Submit = (e) => {
    e.preventDefault();
    console.log(this.cards);
    var view = document.getElementById('content');
    this.GetTemplate('practice')
      .then((template) => {
        view.innerHTML = template;
        this.Next();
      })
  }

  Next() {
    // TODO: These are rigged, 0,1,2 etc. Read these from Settings
    // TODO: Make these large
    var card = document.getElementById('card');
    console.log('idx:', this.cardIndex, 'revealed:', this.cardRevealed);
    if (this.cardIndex == null) {
      this.cardIndex = 0;
    }
    // Reveal card or go to next card
    if (!this.cardRevealed) {
      // Show hint card
      this.cardRevealed = !this.cardRevealed;
      console.log(this.cards.cards[this.cardIndex][0]);
      card.innerHTML = this.cards.cards[this.cardIndex][0];
    } else {
      // Reveal card
      var components = this.cards.components;
      var c = this.cards.cards[this.cardIndex]
      card.innerHTML = '<b>' + components[1] + '</b><br>' + c[1] + '<br><br>'
        + '<b>' + components[2] + '</b><br>' + c[2];
      this.cardIndex++;
      this.cardRevealed = false;
    }

    console.log('NEXT');
  }

  // Helper methods
  GetCards(name) {
    // Get cards data
    return new Promise((resolve, reject) => {
      getUrl('cards/' + name + '.json', true)
        .then((data) => { resolve(data); })
    });
  }

  GetTemplate(template) {
    // Get page template
    return new Promise((resolve, reject) => {
      getUrl('templates/' + template + '.html', false)
        .then((data) => { resolve(data); })
    });
  }

} // class App
