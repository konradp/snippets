var express = require('express');
var app = express();
var path = require('path');
var port = 8080;

// Serve static pages
app.use(express.static('public'))

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
