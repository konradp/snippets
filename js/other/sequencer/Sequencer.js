
class Sequencer {

constructor() {
    // Private
    this.pBpm = 80;
    console.log("Constructed");
}

// Control functions: start/pause/stop
Start() {
    console.log("Started")
}

Pause() {
    console.log("Paused")
}

Stop() {
    console.log("Stopped")
}

// Setters
SetBpm(value) {
    console.log("Setting BPM to" + value);
}

// Getters
GetBpm() {
    console.log("BPM is: " + this.pBpm)
}


} // class Sequencer
