var path = require('path');

// Convert object to url query string
// Example: { one: two, three: four }
// converts to ?one=two&three=four
function urlparams(data) {
  var i = 0,
      encoded = '?'
  Object.keys(data).forEach((key) => {
    if (i > 0) encoded += '&';
    encoded += key + '=' + data[key];
    i++;
  });
  return encoded;
}

function die(msg) {
  if (msg != undefined) { console.log(msg); }
  process.exit(1);
}

function usage() {
  var name = path.basename(process.argv[1]);
  console.log("Convert object to url query string");
  console.log("Usage: node", name, "JSONDATA");
  console.log("Example: node", name, "'{ one: two, three: four }'");
}

// Main
var args = process.argv.slice(2);
if (args[0] == undefined) die(usage());
try {
  // Validate JSON
  JSON.parse(args[0]);
} catch (e) {
  die("Invalid JSON", e);
}
console.log(urlparams(JSON.parse(args[0]))); // Convert
