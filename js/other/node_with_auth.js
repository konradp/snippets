'use strict';
const express = require('express'),
  app = express(),
  bearerToken = require('express-bearer-token'),
  bodyParser = require('body-parser');

var auth = require('basic-auth'),
  fs = require('fs'),
  https = require('https'),
  port = 443;

// SSL
var privateKey  = fs.readFileSync('server.key', 'utf8');
var certificate = fs.readFileSync('server.crt', 'utf8');
var credentials = { key: privateKey, cert: certificate };

// Config
const adminApiUrl = 'localhost'
// A hack for self-signed certs
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0

// Set the views directory and template engine
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
//app.use(express.static('views'));
app.use(express.static('public'));
app.use(express.static(__dirname + '/tags'));
app.use(bearerToken());
app.use(bodyParser.json()); // Parse POST requests
app.locals.pretty = true; // HTML pretty print

// API: Unauthenticated
// in: Auth creds
// out: Bearer token
app.post('/auth', function (req, res) {
  var creds = auth(req)
  if (!creds || !checkAuth(creds.name, creds.pass)) {
    res.statusCode = 403;
    res.setHeader('WWW-Authenticate', 'Basic realm="example"')
    res.end({'msg': 'Access denied'})
  } else {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({ token: genToken() }));
  }
});

// Riot tag route handler
app.get('/tags/:name.tag', function (req, res) {
    res.render('../client/tag-' + req.params.name);
});

// Ensure all endpoints are authenticated
app.use(function (req, res, next) {
  // TODO: Check if token valid
  if(!req.token) {
    res.render('login');
  }
  next();
})

app.get('/', function (req, res) {
  res.render('index');
});

app.post('/api/create', (req, res) => {
    console.log("other")
    res.send('index');
});

app.get('/api/list', (req, res) => {
    console.log("other")
    res.send('index');
});

/// MAIN
// Start listening for connections
var server = https.createServer(credentials, app);
server.listen(port, function (err) {
  if (err) {
    console.error('Cannot listen on port ' + port, err);
  }
  console.log('App listening on port ' + port);
});

// Helper functions
// Basic function to validate credentials for example
function checkAuth(name, pass) {
  // TODO: Check against DB and/or Google oAuth
  return (name === 'kpisarczyk@gmail.com' && pass === 'a');
}

function genToken() {
  return "someToken";
}
