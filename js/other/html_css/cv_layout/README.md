# cv layout

```
npm install
npm run
```

should produce layout like this:

```
|-------------------|
|       HEADER      |
|--------------------
|         |         |
|   COL1  |   COL2  |
|         |         |
---------------------
|       FOOTER      |
---------------------
```
where COL1 and COL2 are vertically scrollable.

# flexbox

container:

- `display: flex;`
- `flex-flow: FLEX-DIRECTION FLEX-WRAP;`
    - flex-direction: **row**, row-reverse, column, column-reverse
    - flex-wrap: wrap, **nowrap**

flex items:

- `flex: FLEX-GROW FLEX-SHRINK FLEX-BASIS`
