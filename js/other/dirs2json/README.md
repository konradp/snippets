# dirs2json

Dir structure.
```
skills/
  cloud platforms/
    overview.html
    aws.html
```


```
[
  {
    name: skills,
    type: dir,
    children: [
      {
        name: cloud platforms,
        type: dir,
        children: [
          {
            name: overview.html
            type: file
          },
          {
            name: aws.html,
            type: file
          }
        ]
      }
    ]
  }
]
```
