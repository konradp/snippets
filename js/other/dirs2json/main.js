// Walk the dir structure of the 'contents' dir and convert it to JSON
// Note: .md files are renamed to .html files
import {
  readdirSync,
  statSync
} from 'node:fs';

let tree = [];


function WalkDir(dir, tree) {
  const entries = readdirSync(dir);
  for (const fName of entries) {
    let path = dir + '/' + fName;
    let s = statSync(path);
    let type = null;
    let e = null;
    e = {
      name: fName,
      type: type,
      url: path,
    };
    if (s.isDirectory()) {
      e.children = WalkDir(path, []);
      e.type = 'dir';
    } else {
      e.type = 'file';
      if (!fName.endsWith('.md')) {
        console.log('WARN: Not a markdown file:', path);
        continue;
      }
      let newName = fName.split('.')
      if (newName.length != 2) {
        console.log('WARN: Too many dots in filename:', path);
        continue;
      }
      // Replace file.md with file.html in url
      path = path.split('/');
      path[path.length-1] = [newName[0],'html'].join('.');
      e.url = path.join('/');
    }
    tree.push(e);
  }
  return tree;
}; // WalkDir


// MAIN
tree = WalkDir('contents', tree);
console.log('OUT', JSON.stringify(tree, null, 2));
