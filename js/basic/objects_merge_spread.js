// Merge two objects

let a = {
  one: {
    f1: '1',
    f2: '2',
  },
  two: {},
};
let b = {
  one: {
    f1: 'one',
    f3: '3',
  },
  three: 3
};
let c = { ...a, ...b };
console.log(c);
