window.addEventListener('load', function() {
  const canvas = document.getElementById("renderCanvas"); // Get the canvas element
  const engine = new BABYLON.Engine(canvas, true); // Generate the BABYLON 3D engine

  // Add your code here matching the playground format
  const createScene = function() {
  const scene = new BABYLON.Scene(engine);

  // House
  faceUV = [];
  faceUV[0] = new BABYLON.Vector4(0.5, 0.0, 0.75, 1.0); //rear face
  faceUV[1] = new BABYLON.Vector4(0.0, 0.0, 0.25, 1.0); //front face
  faceUV[2] = new BABYLON.Vector4(0.25, 0, 0.5, 1.0); //right side
  faceUV[3] = new BABYLON.Vector4(0.75, 0, 1.0, 1.0); //left side
  const house = BABYLON.MeshBuilder.CreateBox("box", {
    width: 1,
    height: 1,
    faceUV: faceUV,
    wrap: true,
  });
  house.position = new BABYLON.Vector3(2, 0.5, 1);
  house.rotation.y = 3*Math.PI/4;
  const ground = BABYLON.MeshBuilder.CreateGround("ground", {width:10, height:10});
  // Roof
  const roof = BABYLON.MeshBuilder.CreateCylinder("roof", {
    diameter: 1.5,
    height: 1.2,
    tessellation: 3,
  });
  roof.position = new BABYLON.Vector3(2, 1.20, 1);
  roof.scaling.x = 0.60;
  roof.rotation.z = Math.PI/2;
  roof.rotation.y = 3*Math.PI/4;

  // Materials
  const groundMat = new BABYLON.StandardMaterial("groundMat");
  groundMat.diffuseColor = new BABYLON.Color3(0, 0.5, 0);
  ground.material = groundMat;
  const roofMat = new BABYLON.StandardMaterial("roofMat");
  roofMat.diffuseTexture = new BABYLON.Texture("https://assets.babylonjs.com/environments/roof.jpg", scene);
  const houseMat = new BABYLON.StandardMaterial("houseMat");
  houseMat.diffuseTexture = new BABYLON.Texture("https://assets.babylonjs.com/environments/cubehouse.png");
  roof.material = roofMat;
  house.material = houseMat;


  // Camera and light
  const camera = new BABYLON.ArcRotateCamera("camera", -Math.PI / 2, Math.PI / 2.5, 15, new BABYLON.Vector3(0, 0, 0));
  camera.attachControl(canvas, true);
  const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(1, 1, 0));
  return scene;
  };

  const scene = createScene(); //Call the createScene function

  // Register a render loop to repeatedly render the scene
  engine.runRenderLoop(function () {
  scene.render();
  });
});

// Watch for browser/canvas resize events
window.addEventListener("resize", function () {
  engine.resize();
});
