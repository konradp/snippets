window.addEventListener('load', () => {
  console.log('test');
  let body = document.body;
  let btn = document.createElement('button');
  btn.innerHTML = 'list';
  body.appendChild(btn);
  btn.addEventListener('click', onClick);
  let btnListInstruments = document.createElement('button')
});


function send(cmd) {
  postData('/cmd', { cmd: cmd }).then((data) => {
    console.log('RESPONSE', data.stdout);
  });

}

async function postData(url = "", data = {}) {
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  return response.json();
}

function onClick() {
  postData('/cmd', { cmd: "list" }).then((data) => {
    console.log('RESPONSE', data);
  });
}
