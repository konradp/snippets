const express = require('express');
const { execSync } = require('child_process');

const app = express();

const port = 8000;
app.use(express.static('public'))
app.use(express.json());


app.get('/info', (req, res) => {
  res.send({ msg: 'Test' })
});

app.post('/cmd', (req, res) => {
  console.log('CMD:', req.body.cmd);
  let out = cmd(req.body.cmd);

  console.log('out', out);
  res.send({
    response: 'OK',
    stdout: out,
  });


});

function cmd(cmd) {
  execSync('echo -n > yosh.out');
  execSync(`tmux send-keys -t yosh "${cmd}" Enter`);
  let out = execSync('cat yosh.out', { encoding: 'utf8'})
  console.log('OUT1', out);
  out = out.split(/\r?\n/);
  out.shift();
  out.shift();
  out.pop();
  out.pop();
  out.pop();
  console.log('OUT', out);
  execSync(`tmux send-keys -t yosh / Enter`);
  return out;
  //return execSync('cat yosh.out').toString();


  //exec('cat *.js bad_file | wc -l', (error, stdout, stderr) => {
  //  if (error) {
  //    console.error(`exec error: ${error}`);
  //    return;
  //  }
  //  console.log(`stdout: ${stdout}`);
  //  console.log(`stderr: ${stderr}`);
  //});
}


// NOTES
// l b <- list banks
// l i 15 <- list instruments in bank 15
// s p 1 i 10 <- set part 1 to instrument 10
// s p 1 i 10 b 15 <- same
// s b 10 <- set bank to 10

// MAIN
app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
