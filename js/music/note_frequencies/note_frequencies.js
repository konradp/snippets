let notes = [];

function midiToFreq(midiNote) {
  // P_n = 440*2^((n-49)/12)
  // Ref: https://en.wikipedia.org/wiki/Equal_temperament#Calculating_absolute_frequencies
  // Ref: https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
  let freq = 440 * ( Math.pow(2, (midiNote-69)/12) );
  return freq;
}

function createNoteTable() {
  for (var i = 0; i <= 127; i++) {
    notes.push(+midiToFreq(i).toFixed(2));
  }
}

createNoteTable();
console.log(notes);
