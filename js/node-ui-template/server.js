const express = require('express');
const app = express();
const port = 8080;

// Serve static pages
app.use(express.static('public'))

// Endpoints
app.get('/info', (req, res) => {
  res.send({ msg: 'Test' })
});

// Start server
app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
