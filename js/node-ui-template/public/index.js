function getInfo() {
  var url = '/info';
  return new Promise((resolve, reject) => {
    createRequest('GET', url)
      .then((res) => { resolve(res); })
      .catch((err) => { reject(err); });
  });
}

getInfo()
  .then((r) => {
    var main = document.getElementById('main');
    main.innerHTML = JSON.stringify(r);
  })
