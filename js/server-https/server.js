// Config
var port = 5000;

// Include
var express = require('express');
var app = express();
var fs = require('fs');
var https = require('https');
var path = require('path');

app.all('*', (req, res) => {
  //var r = req;
  var r = req;
  res.send({
    rawHeaders: r.rawHeaders,
    url: r.url,
    method: r.method,
    statusCode: r.statusCode,
    baseUrl: r.baseUrl,
    originalUrl: r.originalUrl,
  });
});

// we will pass our 'app' to 'https' server
https.createServer({
    key: fs.readFileSync('./localhost.key'),
    cert: fs.readFileSync('./localhost.crt'),
    passphrase: 'YOUR PASSPHRASE HERE'
}, app)
.listen(port, () => {
  console.log('Listening on https://localhost:'+port);
});
