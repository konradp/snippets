# server-https
Server which returns request info sent to it.

## Prerequisites
Create cert.
```
./create_cert.sh
```

## Run
Run server.
```
npm install
npm start
```
Make a call to the server.
```
curl -s https://localhost:5000/one/two | jq
```

Example
```
$ curl -s https://localhost:5000/one/two -k | jq
{
  "rawHeaders": [
    "Host",
    "localhost:5000",
    "User-Agent",
    "curl/7.52.1",
    "Accept",
    "*/*"
  ],
  "url": "/one/two",
  "method": "GET",
  "statusCode": null,
  "baseUrl": "",
  "originalUrl": "/one/two"
}
```
