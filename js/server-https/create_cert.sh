#!/bin/bash
openssl req \
  -nodes \
  -new -x509 \
  -keyout localhost.key \
  -out localhost.crt \
  -subj "/C=US/ST=YourState/L=YourCity/O=Example-Certificates/CN=localhost.local"
