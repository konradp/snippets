// Walk the dir structure of 'content' dir, and convert
// all .md files to .html. Output the .html files to
// the 'public' dir.
import {
  mkdirSync,
  readdirSync,
  readFileSync,
  statSync,
  writeFileSync,
} from 'node:fs';
import showdown from 'showdown';


var dir_input = 'content';
var dir_output = 'public';
const converter = new showdown.Converter();


function ToHTML(filepath) {
  const text = readFileSync(filepath, { encoding: 'utf8' });
  
  let html      = converter.makeHtml(text);
  return html + '\n';
}


function MkDir(dir) {
  try {
    let s = statSync(dir);
    if (s && s.isDirectory()) {
      return true;
    } else {
      console.log('ERROR');
    }
  } catch (error) {
    mkdirSync(dir, { recursive: true });
  }
}


function WalkDir(dir) {
  const entries = readdirSync(dir);
  for (const fileName of entries) {
    let path = dir + '/' + fileName;
    let s = statSync(path);
    if (s.isDirectory()) {
      WalkDir(path);
    } else {
      if (fileName.endsWith('.md')) {
        let dir_new = dir.split('/');
        dir_new[0] = dir_output;
        dir_new = dir_new.join('/');
        MkDir(dir_new);
        const pathNew = dir_new + '/' + fileName.replace('.md', '.html');
        const html = ToHTML(path);
        console.log('CREATE:', pathNew);
        writeFileSync(pathNew, html);
      }
    }
  }
}


// MAIN
MkDir(dir_output);
WalkDir(dir_input);
