var express = require('express');
var app = express();
var port = 8081;

app.use(express.static('public');
app.get('/', (req, res) => res.send('Test'))
app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
