# simple-synth
Play musical pitches using the Web Audio API.

This is inspired by the MDN article:  
https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Simple_synth

The app requirements:

UI:
  - (/) number input, two decimal places
    - (/) html
    - (/) js
  - (/) button input: C, C#, D, D#, ...
    - (/) html
    - (x) js
  - (/) select input: octave: 0, 1, 2, 3
    - (/) html
    - (x) js
  - (/) button: start
    - (/) html
    - (/) js
  - (/) button: stop
    - (/) html
    - (/) js
  - (/) button: play_0.5s
    - (/) html
    - (/) js
  - (/) slider: volume
    - (/) html
    - (/) js
  - (/) select input: waveform
    - (/) html
    - (/) js

functionality:
- (/) calculate frequencies from notes  
  https://pages.mtu.edu/~suits/NoteFreqCalcs.html
- (/) timer for 0.5s
- (/) play button:
    - (/) play_0.5s
    - (/) start
    - (/) stop

The inputs set the pitch of the note (can be manually input, or set to a musical note, such as C1). The start and stop buttons start and stop playing the note. The play_1s button plays the note for one second.

## Notes from article
- we take the volume slider directly from the article
