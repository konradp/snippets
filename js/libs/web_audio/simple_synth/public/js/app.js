const audioContext = new AudioContext();
let mainGainNode = null;
let osc = null;
let is_playing = false;

let wavePicker = null;
let volumeControl = null;
let noteFreq = null;
//let customWaveform = null;
let sineTerms = null;
let cosineTerms = null;

window.onload = function() {
  wavePicker = document.querySelector("select[name='waveform']");
  volumeControl = document.querySelector("input[name='volume']");
  setup();

  // Buttons
  const btn_start = document.querySelector("input[name='start']");
  btn_start.addEventListener('click', playOsc, false);
  const btn_stop = document.querySelector("input[name='stop']");
  btn_stop.addEventListener('click', stopOsc, false);
  btn_play_s = document.querySelector("input[name='play']");
  btn_play_s.addEventListener('click', playOscSecond, false);

  // Drop-downs to update notes
  // TODO
  const select_note = document.querySelector("select[name='note']");
  select_note.addEventListener('change', changeNoteOctave, false);
  const select_octave = document.querySelector("select[name='octave']");
  select_octave.addEventListener('change', changeNoteOctave, false);
  const input_freq = document.querySelector("input[name='frequency']");
  input_freq.addEventListener('input', changeFreq, false);
}


function setup() {
  volumeControl.addEventListener('input', changeVolume, false);

  mainGainNode = audioContext.createGain();
  mainGainNode.connect(audioContext.destination);
  mainGainNode.gain.value = volumeControl.value;

  sineTerms = new Float32Array([0, 0, 1, 0, 1]);
  cosineTerms = new Float32Array(sineTerms.length);
  customWaveform = audioContext.createPeriodicWave(cosineTerms, sineTerms);

  changeVolume();
}

function playOscSecond() {
  // play oscillator and start timer to stop it after some time
  playOsc();
  setTimeout(() => {
    stopOsc();
  }, 500);
}

function playOsc() {
  if (is_playing) {
    return;
  }
  // Get settings: get freq from the DOM and start oscillator
  osc = audioContext.createOscillator();
  osc.connect(mainGainNode);
  const freq = document.querySelector("input[name='frequency']").value;
  const type = wavePicker.options[wavePicker.selectedIndex].value;
  if (type === "custom") {
    osc.setPeriodicWave(customWaveform);
  } else {
    osc.type = type;
  }
  osc.frequency.value = freq;
  osc.start();
  is_playing = true;
}

function stopOsc() {
  osc.stop();
  delete osc;
  is_playing = false;
}


////// CONTROLS //////
function changeFreq() {
  const freq = document.querySelector("input[name='frequency']");
  osc.frequency.value = freq.value;
}


function changeNoteOctave() {
  const note = document.querySelector("select[name='note']").value;
  const octave = document.querySelector("select[name='octave']").value;
  const input_freq = document.querySelector("input[name='frequency']");
  const freq = noteToFreq(note, octave);
  input_freq.value = freq;
  osc.frequency.value = freq;
}


function changeVolume() {
  mainGainNode.gain.value = volumeControl.value
}


function noteToFreq(note, octave) {
  // note = e.g. 'C' or 'C#'
  // octave 0, 1, 2, 3, 4. The C4 is the middle C
  // ref: https://pages.mtu.edu/~suits/NoteFreqCalcs.html
  // f_n = f_0 * (a)^n
  // where
  //  f_0: freq of A4 = 440
  //  n: # of half-steps that we are from f_0
  //  a: (2)^(1/12)
  const notes = [ 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
  const n = notes.indexOf(note) - 9 + 12*(octave - 4);
  const a = Math.pow(2, 1/12);
  return 440 * Math.pow(a, n);
}
