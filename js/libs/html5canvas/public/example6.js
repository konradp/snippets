class Shape {
  constructor(ctx, type) {
    this.ctx = ctx;
    this.type = type;
  };

  draw() { // Draw square
    var c = this.ctx;
    var w = c.canvas.width;
    var h = c.canvas.height;
    c.fillStyle = 'rgb(0, 0, 200)'; // blue
    console.log(c.canvas.width/2);
    c.beginPath();
    c.fillRect(w/2, h/2, h/4, h/4);
  };
};

class Plot {
  shapeType = 'vert';
  constructor(ctx) {
    if (ctx != null) this.ctx = ctx;
    ctx.color = 'blue';
    ctx.canvas.onmousemove = this.OnMouseMove;
  }

  clear() {
    var c = this.ctx;
    c.clearRect(0, 0, c.canvas.width, c.canvas.height);
    c.strokeStyle = 'rgb(0, 0, 0)'; // black
    c.strokeRect(0, 0, c.canvas.width, c.canvas.height);
  }

  draw() {
    this.clear();
    var c = new Shape(this.ctx, this.shapeType);
    c.draw();
  };

  OnMouseMove(e) {
    var canvas = document.getElementById('canvas');
    var c = canvas.getContext('2d');
    var w = c.canvas.width;
    var h = c.canvas.height;
    var rect = this.getBoundingClientRect();
    var x = e.clientX - rect.left,
      y = (e.clientY - rect.top)/2; // Why divide by 2?

    c.beginPath();
    var r = c.rect(w/2, h/2, h/4, h/4);

    if (c.isPointInPath(x, y)) {
      c.fillStyle = 'rgb(0, 0, 200)'; // blue
    } else {
      c.fillStyle = 'rgb(0, 200, 0)'; // green
    }
    c.fill();
  }
};

function drawPlot() {
  var canvas = document.getElementById('canvas');
  if (canvas.getContext) {
    if (plot == null) plot = new Plot(canvas.getContext('2d'));
    plot.draw();
  }
}

// MAIN
var plot = null;
drawPlot();
