// Draw a OHLC box/candle
var candleWidth = 10;
var maxY = 100;
var offset = 10;
var kPlots = [
  { "name": "plot1", },
  { "name": "plot2", },
  { "name": "plot3" },
];

for (var i in kPlots) {
  var plots = document.getElementById('plots');
  var c = document.createElement('canvas');
  c.id = kPlots[i].name;
  c.style = 'width:50%; height:50%; display: block;';
  plots.appendChild(c);
  if (c.getContext) {
    kPlots[i].context = c.getContext('2d');
  } else {
    console.log('ERROR');
  }
}

drawShapes('square');

function drawShapes(type) {
  if (type != 'square' && type != 'circle') {
    console.log('Invalid type', type);
  }
  var plots = document.getElementById('plots');
  for (var i in kPlots) {
    if (kPlots[i].plot ==  null) {
      kPlots[i].plot = new Plot(kPlots[i].context);
    }
    var p = kPlots[i].plot;
    p.clear();
    p.setType(type);
    p.draw();
  }
}
