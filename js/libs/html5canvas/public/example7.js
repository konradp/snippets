class Example7 {
  constructor(canvas_id) {
    this.canvas = document.getElementById(canvas_id);
    this.context = this.canvas.getContext('2d');
    this.circle = null;
    this.isDragging = false;
    this.mousePos = { x: 100, y: 100, };

    this.Draw();

    // callbacks
    this.canvas.addEventListener('mousedown', (e) => {
      const mouse = this.GetMousePos(e);
      const c = this.context;
      if (c.isPointInPath(this.circle, mouse.x, mouse.y)) {
        console.log('yes');
        this.isDragging = true;
      } else {
        console.log('no');
      }
    });
    this.canvas.addEventListener('mouseup', () => {
      this.isDragging = false;
    });
    this.canvas.addEventListener('mousemove', (e) => {
      if (this.isDragging) {
        this.mousePos = this.GetMousePos(e);
        this.Draw();
      }
    });
  }

  GetMousePos(e) {
    var rect = this.canvas.getBoundingClientRect();
    return {
      x: e.clientX - rect.left,
      y: e.clientY - rect.top
    };
  }

  Draw() {
    console.log('draw'); // DEBUG
    const c = this.context;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.circle = new Path2D();
    c.fillStyle = 'blue';
    this.circle.arc(this.mousePos.x, this.mousePos.y,
      10, 0, 2 * Math.PI);
    c.fill(this.circle);
  }
}; // class Example7
