var navbar_data = {
  "example1": "Draw line",
  "example2": "Draw squares",
  "example3": "Draw candlestick chart",
  "example4": "Button changing multiple images",
  "example5": "Button changing single image",
  "example6": "Mouse hover changing single image",
  "example7": "Move point with the mouse",
  "example8": "Move arc points with the mouse",
}

var navbar = document.getElementById("navbar");
var list = document.createElement("ul");
for (var name in navbar_data) {
  var li = document.createElement("li");
  var a = document.createElement("a");
  a.href = name + ".html";
  a.innerHTML = name + ": " + navbar_data[name];
  li.appendChild(a);
  list.appendChild(li);
}
navbar.appendChild(list);
