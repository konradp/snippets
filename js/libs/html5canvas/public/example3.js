// Draw a OHLC box/candle

var candleWidth = 10;
var maxY = 100;
var offset = 10;
var spacing = 2;

class CandleTest {
  constructor(ctx, x, open, high, low, close) {
    this.open = open;
    this.high = high;
    this.low = low;
    this.close = close;
    this.ctx = ctx;
    this.cHeight = ctx.canvas.height;
    this.x = x;
  }

  draw() {
    var higher = 0;
    var lower = 0;
    if (this.close >= this.open) {
      ctx.fillStyle = 'rgb(0, 200, 0)';
      higher = this.close;
      lower = this.open;
    } else {
      higher = this.open;
      lower = this.close;
      ctx.fillStyle = 'rgb(200, 0, 0)';
    }

    // Candle body
    ctx.fillRect(
      this.x*offset,
      this.translate(this.close),
      candleWidth,
      (this.cHeight/maxY)*(this.close - this.open)
      // = this.translate(this.open) - this.translate(this.close)
    );

    // Candle wick: high
    ctx.moveTo(this.x*offset + candleWidth/2, this.translate(higher));
    ctx.lineTo(this.x*offset + candleWidth/2, this.translate(this.high))
    ctx.stroke();
    // Candle wick: low
    ctx.moveTo(this.x*offset + candleWidth/2, this.translate(lower));
    ctx.lineTo(this.x*offset + candleWidth/2, this.translate(this.low))
    ctx.stroke();
  };

  translate(value) {
    var cHeight = this.ctx.canvas.height;
    // Translate value to position on canvas and normalise over maxY
    // Note: Canvas origin is at top left
    return cHeight - value/maxY * cHeight;
  };
}; //CandleTest class

var canvas = document.getElementById('canvas');
if (canvas.getContext) {
  var ctx = canvas.getContext('2d');
  // Bounding box
  ctx.strokeRect(0, 0, ctx.canvas.width, ctx.canvas.height);

  var data = [
    [ 20, 100, 10, 50 ],
    [ 50, 60, 10, 20 ],
    [ 50, 80, 40, 70 ],
    [ 40, 90, 30, 80 ],
  ];
  for (var i in data) {
    var c = new CandleTest(ctx, i*spacing,
      data[i][0], data[i][1], data[i][2], data[i][3], data[i][4]);
      c.draw();
  }
}
