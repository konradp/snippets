class Plot {
  data = [];
  spacing = 2;
  radius = 10;
  shapeType = 'circle';
  constructor(ctx) {
    if (ctx != null) {
      this.ctx = ctx;
    } else {
      console.log('ERROR');
    }
  }

  setType(type) {
    this.shapeType = type;
  };

  clear() {
    // Clear and draw bounding box
    var c = this.ctx;
    c.clearRect(0, 0, c.canvas.width, c.canvas.height);
    c.strokeRect(0, 0, c.canvas.width, c.canvas.height);
  }

  draw() {
    // Clear
    this.clear();

    // Draw data
    for (var i in [ 1, 2, 3, 4, 5 ]) {
      var c = new Shape(
        this.ctx,
        this.shapeType,
        i*this.spacing,
        this.radius,
      );
      c.draw();
    }
  };
}; //class Plot
