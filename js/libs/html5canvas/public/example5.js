class Shape {
  // Draw Shape: horizontal or vertical line
  constructor(canvas, type) {
    this.canvas = canvas;
    this.type = type;
  };

  draw() { // Draw either a horizontal or vertical line
    var c = this.canvas;
    c.strokeStyle = 'rgb(0, 0, 200)'; // blue
    c.beginPath();
    c.moveTo(c.canvas.width/2, 0);
    if (this.type == 'vert') {
      c.lineTo(c.canvas.width, c.canvas.height);
    } else if (this.type == 'horiz') {
      c.lineTo(0, c.canvas.height);
    }
    c.stroke();
  };
};

class Plot {
  shapeType = 'vert';
  constructor(canvas) {
    if (canvas != null) this.canvas = canvas;
  }

  setType(type) {
    this.shapeType = type;
  };

  clear() {
    var c = this.canvas.canvas;
    this.canvas.clearRect(0, 0, c.width, c.height);
  }

  draw() {
    this.clear();
    var c = new Shape(this.canvas, this.shapeType);
    c.draw();
  };
};

function drawPlot(pType) {
  var canvas = document.getElementById('canvas');
  if (canvas.getContext) {
    if (plot == null) plot = new Plot(canvas.getContext('2d'));
    plot.setType(pType);
    plot.draw();
  }
}

// MAIN
var plot = null;
drawPlot('vert');
