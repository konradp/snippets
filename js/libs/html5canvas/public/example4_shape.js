// Draw Shape: square/circle
class Shape {
  constructor(ctx, type, x, radius) {
    // Params
    this.ctx = ctx;
    this.x = x; // Spacing
    this.type = type;
    this.radius = radius;
    // Other
    this.cHeight = ctx.canvas.height;
    this.offset = 20;
    this.sY = 40; // Shape Y coordinate
  };

  draw() {
    var c = this.ctx;
    c.fillStyle = 'rgb(0, 200, 0)';

    // Shape body
    if (this.type == 'square') {
      c.beginPath();
      c.moveTo(this.x*this.offset + 10, 5);
      c.lineTo(this.x*this.offset + 25, 30);
      c.fillRect(this.x*this.offset, this.sY, this.radius*2, this.radius*2);
    } else {
      c.beginPath();
      c.moveTo(this.x*this.offset + 5, 5);
      c.arc(this.x*this.offset, this.sY, this.radius, 0, 2*Math.PI);
      c.fill();
    }
    // Wick
    c.strokeStyle = 'rgb(100, 0, 0, 200)'; // blue
    c.stroke();
  };
}; //class Shape
