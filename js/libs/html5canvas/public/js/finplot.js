// Draw a OHLC box/candle
class Candle {
  constructor(ctx, x, open, high, low, close) {
    // Check
    if (ctx == null) throw 'Context undefined';
    if (x == null) throw 'X undefined';
    if (open == null) throw 'open undefined';

    // Params
    this.ctx = ctx;
    this.x = x; // Spacing
    this.open = open;
    this.high = high;
    this.low = low;
    this.close = close;
    // Other
    this.cHeight = ctx.canvas.height;
    this.candleWidth = 10;
    this.maxY = 100;
    this.offset = 10;
  };

  draw() {
    //var c     = this.canvas.getContext('2d'),
    var c     = this.ctx,
        cw    = this.candleWidth,
        open  = this.open,
        high  = this.high,
        low   = this.low,
        close = this.close;

    // Candlestick: red/green
    if (close >= open) {
      c.fillStyle = 'rgb(0, 200, 0)'; // green
      var higher = close;
      var lower = open;
    } else {
      c.fillStyle = 'rgb(200, 0, 0)'; // red
      var higher = open;
      var lower = close;
    }

    // Candlestick: draw
    c.beginPath();
    c.fillRect(
      this.x*this.offset,
      this.translate(close),
      this.candleWidth,
      (this.cHeight/this.maxY)*(close - open)
      // = this.translate(this.open) - this.translate(this.close)
    );

    // Wick: high
    c.beginPath();
    c.strokeStyle = 'rgb(0, 0, 200)'; // blue
    c.moveTo(
      this.x*this.offset + this.candleWidth/2,
      this.translate(higher)
    );
    c.lineTo(
      this.x*this.offset + this.candleWidth/2,
      this.translate(this.high)
    );
    c.stroke();

    // Wick: low
    c.beginPath();
    c.moveTo(
      this.x*this.offset + this.candleWidth/2,
      this.translate(lower)
    );
    c.lineTo(
      this.x*this.offset + this.candleWidth/2,
      this.translate(this.low)
    );
    c.stroke();
  };

  translate(value) {
    var cHeight = this.cHeight;
    // Translate value to position on canvas and normalise over maxY
    // Note: Canvas origin is at top left
    return cHeight - value/this.maxY * cHeight;
  };
}; //class Candle

/***** PLOT *****/
class Plot {
  ctx = null;
  data = [];
  spacing = 2;
  constructor(ctx) {
    this.ctx = ctx;
  }

  setData(data) {
    this.data = data;
  };

  clear() {
    var c = this.ctx;
    var width = c.canvas.width;
    var height = c.canvas.height;
    c.clearRect(0, 0, width, height); // Clear
    c.strokeRect(0, 0, width, height); // Bounding box
  }

  draw() {
    this.clear();
    // Draw data
    for (var i in this.data) {
      var c = new Candle(this.ctx, i*this.spacing,
        // date
        this.data[i][1], // open
        this.data[i][2], // high
        this.data[i][3], // low
        this.data[i][4], // close
      );
      c.draw();
    }
  };
}; //class Plot
