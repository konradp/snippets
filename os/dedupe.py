import hashlib
import json
import os
from os.path import join, getsize
import sys

# md5 generation lifted from:
# - https://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file


def usage():
  print(f'Usage: {sys.argv[0]} DIR_PATH [EXCLUSIONS]')
  print(f'Example: {sys.argv[0]} /home/user')
  print(f'Example: {sys.argv[0]} /home/user "txt,exe,mp3"')
  print(f'Example: {sys.argv[0]} /music "au,bankdir,md,mid,peak,ttl"')
  exit(0)


def get_md5(fname):
  hash_md5 = hashlib.md5()
  with open(fname, 'rb') as f:
    for chunk in iter(lambda: f.read(4096), b''):
      hash_md5.update(chunk)
  return hash_md5.hexdigest()


def main():
  # Parse args
  if len(sys.argv) not in [2, 3]:
    usage()
  dir_path = sys.argv[1]
  exclusions = 'au,bankdir,md,mid,peak,ttl'.split(',')
  if len(sys.argv) == 3:
    exclusions = sys.argv[2].split(',')

  duplicates = {}
  md5s = {}
  for root, dirs, files in os.walk(dir_path):
    if '.git' in dirs:
      # Skip .git directories
      dirs.remove('.git')
    for fname in files:
      breaking = False
      for ext in exclusions:
        if fname.endswith('.' + ext):
          breaking = True
          break
      if breaking:
        continue
      fpath = f'{root}/{fname}'
      md5 = get_md5(fpath)
      if md5 not in md5s:
        md5s[md5] = []
      md5s[md5].append(fpath)

  # Check for duplicates
  for md5, paths in md5s.items():
    if len(paths) != 1:
      duplicates[md5] = paths

  print(json.dumps(duplicates, indent=2))


if __name__ == '__main__':
  main()
