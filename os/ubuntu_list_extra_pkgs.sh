#!/bin/bash
ARCH=$(arch)
if [[ "$ARCH" == "x86_64" ]]; then
  ARCH=amd64
fi

read _ VER _ <<< `lsb_release -ds`
echo $VER
echo $ARCH

# Installed pkgs
dpkg-query --show > installed
sort installed > s
mv s installed

# This file is tab-separated
curl -s http://releases.ubuntu.com/$VER/ubuntu-$VER-desktop-$ARCH.manifest \
  -o manifest
sort manifest > s
mv s manifest
