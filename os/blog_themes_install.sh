#!/bin/bash
# Run this in a blog root dir to install, change, or update hugo themes
read -r -d '' VAR <<EOF
  archie https://github.com/athul/archie
  coder https://github.com/luizdepra/hugo-coder
  compost https://github.com/canstand/compost
  devise https://github.com/austingebauer/devise
  eiio https://github.com/leonhe/hugo_eiio
  etch https://github.com/LukasJoswiak/etch
  gokarna https://github.com/526avijitgupta/gokarna
  holy https://github.com/serkodev/holy
  hugo-bearblog https://github.com/janraasch/hugo-bearblog
  hugo-bearcub https://github.com/clente/hugo-bearcub
  HugoTeX https://github.com/kaisugi/HugoTeX
  lowkey https://github.com/nixentric/Lowkey-Hugo-Theme
  minimal-bootstrap https://github.com/zwbetz-gh/minimal-bootstrap-hugo-theme
  nostyleplease https://github.com/Masellum/hugo-theme-nostyleplease
  papermod https://github.com/adityatelange/hugo-PaperMod
  risotto https://github.com/joeroe/risotto
  sk1 https://github.com/J-Siu/hugo-theme-sk1
  techlab https://github.com/sefeng211/techlab-hugo-theme
  vanilla-bootstrap https://github.com/zwbetz-gh/vanilla-bootstrap-hugo-theme
  xmin    https://github.com/yihui/hugo-xmin
  yinyang https://github.com/joway/hugo-theme-yinyang
EOF


# Choice: 1) install themes, 2) switch theme
echo Current theme
grep theme config.toml

while [[ $OPT != "1" && $OPT != "2" ]]; do
  echo Options:
  echo 1\) Install themes
  echo 2\) Switch theme
  echo 3\) Update themes
  read -p '> ' OPT
done

echo $OPT

if [[ $OPT == "1" ]]; then
  # Install themes
  while read THEME REPO; do
    if [[ ! -d themes/$THEME ]]; then
      echo Install $THEME from $REPO
      git submodule add $REPO themes/$THEME --force
    fi
  done <<< $VAR
elif [[ $OPT == "2" ]]; then
  # Switch theme
  echo Not implemented
  echo Current theme
  grep theme config.toml
  echo Pick theme:
  for THEME in $(ls -1 themes/); do
    echo \ \ - $THEME
  done

  read -p '> ' THEME
  echo You picked $THEME
  sed -i "s/theme = .*/theme = \"$THEME\"/" config.toml
elif [[ $OPT == "3" ]]; then
  # Update themes
  echo Update themes
  git submodule update --remote --merge
fi
