#!/usr/bin/bash
DIRS=$(find -name \.git)
for DIR in $DIRS; do
  DIR=$(dirname $DIR)
  pushd $DIR >/dev/null
  RES=$(git status --short)
  if [[ ! -z "$RES" ]]; then
    echo ==== $DIR
    git status --short
  fi
  popd >/dev/null
done
