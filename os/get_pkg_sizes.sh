#!/bin/bash
dpkg-query --show \
  --showformat \
  '${Installed-Size} \t ${Package} ${binary:Synopsis}\n' \
  | sort -n
