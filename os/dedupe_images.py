# Given an output from the 'dedupe' program, open an UI showing each image
# which gives an ability to delete the duplicates
import sys
import json
import pathlib
import pprint
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QCheckBox
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt

class DuplicateImageSelector(QMainWindow):
    def __init__(self, duplicate_sets):
        super().__init__()
        self.setWindowTitle("Duplicate Image Selector")
        self.setGeometry(100, 100, 800, 600)
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.layout = QVBoxLayout()
        self.central_widget.setLayout(self.layout)

        self.duplicate_sets = duplicate_sets
        self.current_set_index = 0

        self.image_label = QLabel()
        self.checkbox_widget = QWidget()
        self.checkbox_layout = QVBoxLayout(self.checkbox_widget)
        self.layout.addWidget(self.image_label)
        self.layout.addWidget(self.checkbox_widget)

        self.show_duplicate_set(self.current_set_index)

    def show_duplicate_set(self, set_index):
        if set_index < 0 or set_index >= len(self.duplicate_sets):
            return
        self.current_set_index = set_index
        image_paths = self.duplicate_sets[set_index]
        self.image_label.clear()
        self.image_label.setAlignment(Qt.AlignCenter)
        if image_paths:
            image = QImage(image_paths[0])
            pixmap = QPixmap.fromImage(image)
            self.image_label.setScaledContents(True)
            self.image_label.setPixmap(pixmap)
        self.update_checkboxes(image_paths)

    def update_checkboxes(self, image_paths):
        for i in reversed(range(self.checkbox_layout.count())):
            widget = self.checkbox_layout.itemAt(i).widget()
            widget.deleteLater()

        for index, path in enumerate(image_paths):
            checkbox = QCheckBox(f"{path}")
            self.checkbox_layout.addWidget(checkbox)


    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Return:
            # Pressed return: collect selected items
            self.selected_paths = []
            for i in range(len(self.duplicate_sets[self.current_set_index])):
                print('I:', i)
                checkbox = self.checkbox_layout.itemAt(i).widget()
                if checkbox.isChecked():
                    self.selected_paths.append(checkbox.text())

            for path in self.selected_paths:
                print(f'DELETE: {path}')
                pathlib.Path(path).unlink()
            # Go to the next set if available
            if self.current_set_index < len(self.duplicate_sets) - 1:
                self.show_duplicate_set(self.current_set_index + 1)
            else:
              self.close()

        if event.key() == Qt.Key_Escape:
            self.close()

        # Number pressed
        key = event.key() - Qt.Key_1
        if 0 <= key < len(self.duplicate_sets[self.current_set_index]):
            checkbox = self.checkbox_layout.itemAt(key).widget()
            checkbox.setChecked(not checkbox.isChecked())

if __name__ == "__main__":
    app = QApplication(sys.argv)
    # Parse args
    if len(sys.argv) < 2:
        print('ERROR: Provide input file with transaction IDs')
        print(f'Usage: python3 {sys.argv[0]} FILE')
        print(f'Usage: python3 {sys.argv[0]} FILE debug')
        exit(1)
    file_in = sys.argv[1]

    duplicate_sets = []
    with open(file_in) as f:
        data = json.loads(f.read())
        #print(data)
        for _, files in data.items():
            print(files)
            is_img = False
            all_files_exist = True
            for file in files:
                if file.endswith('.jpg') or \
                    file.endswith('.JPG') or \
                    file.endswith('.jpeg') or \
                    file.endswith('.png') or \
                    file.endswith('.PNG') or \
                    file.endswith('.tif') or \
                    file.endswith('.TIF'):
                    print('IS IMAGE', file)
                    is_img = True
                if not pathlib.Path(file).is_file():
                    all_files_exist = False
            if is_img and all_files_exist:
              duplicate_sets.append(files)
    pprint.pprint(duplicate_sets)
    window = DuplicateImageSelector(duplicate_sets)
    window.show()
    sys.exit(app.exec_())
