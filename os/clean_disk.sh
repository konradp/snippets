#!/bin/bash

echo == App cache
echo == chromium
rm -r ~/.cache/chromium/Default/Cache/Cache_Data/*

echo == chrome
rm -r ~/.cache/google-chrome/Default/Cache/Cache_Data/*
rm -r ~/.cache/google-chrome/Default/Code\ Cache/*

echo == docker
docker system prune -a

echo == spotify
rm -r ~/.cache/spotify/Data
rm -r ~/.cache/spotify/Storage
rm -r ~/.cache/spotify/Browser
