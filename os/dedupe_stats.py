# Given the output from the 'dedupe' program, calculate how much space
# would be saved if the duplicates were removed
import json
import sys
from os.path import getsize

def get_size(size, unit='bytes'):
  # ref: https://datagy.io/python-file-size/
  #file_size = getsize(file_path)
  exponents_map = {'bytes': 0, 'kb': 1, 'mb': 2, 'gb': 3}
  if unit not in exponents_map:
    raise ValueError("Must select from \
    ['bytes', 'kb', 'mb', 'gb']")
  else:
    size = size / 1024 ** exponents_map[unit]
    return round(size, 3)

def main():
  if len(sys.argv) < 2:
     print('ERROR: Provide input file with transaction IDs')
     print(f'Usage: python3 {sys.argv[0]} FILE')
     print(f'Usage: python3 {sys.argv[0]} FILE debug')
     exit(1)
  file_in = sys.argv[1]

  total_size = 0
  total_size_after_save = 0
  total_size_saved = 0
  with open(file_in) as f:
    data = json.loads(f.read())
    for _, files in data.items():
      file = files[0]
      size = getsize(file)
      total_size += size*len(files)
      total_size_after_save += size
      total_size_saved += size*( len(files)-1 )

  print('total size:', get_size(total_size, 'mb'), 'MB')
  print('total size after save:', get_size(total_size_after_save, 'mb'), 'MB')
  print('total size saved:', get_size(total_size_saved, 'mb'), 'MB')

if __name__ == "__main__":
  main()
