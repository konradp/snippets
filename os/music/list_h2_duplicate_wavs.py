# - Find all duplicate .wav files
# - for each duplicate file, print hydrogen songs which use is
#
# md5 generation lifted from:
# - https://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file
import hashlib
import json
import os
from os.path import join, getsize
from glob import glob
import sys
import xml.etree.ElementTree as ET


def usage():
  print(f'Usage: {sys.argv[0]} DIR_PATH')
  print(f'Example: {sys.argv[0]} /music')
  print('Warning: DIR_PATH must be a full path')
  exit(0)


def get_md5(fname):
  hash_md5 = hashlib.md5()
  with open(fname, 'rb') as f:
    for chunk in iter(lambda: f.read(4096), b''):
      hash_md5.update(chunk)
  return hash_md5.hexdigest()


def main():
  # Parse args
  if len(sys.argv) != 2:
    usage()
  dir_path = sys.argv[1]
  result = {}

  # Get all wav files and deduplicate
  wav_files = glob(f'{dir_path}/**/*.wav', recursive=True)
  for wav_file in wav_files:
    md5 = get_md5(wav_file)
    if md5 not in result:
      result[md5] = {}
    result[md5][wav_file] = []

  # Remove the ones which are not duplicates
  result = {
    md5: files
    for md5, files in result.items()
    if len(files) > 1
  }
  #print(json.dumps(result, indent=2))

  # Get h2songs
  h2song_files = glob(f'{dir_path}/**/*.h2song', recursive=True)
  for f in h2song_files:
    bad_samples = []
    tree = ET.parse(f)
    root = tree.getroot()
    samples = root.findall('.//instrumentComponent//filename')
    for sample in samples:
      sample_path = sample.text
      if sample_path is None:
        continue
      # Find and append path to the result
      for md5, files in result.items():
        #print(files)
        if sample_path in files:
          if f not in result[md5][sample_path]:
            result[md5][sample_path].append(f)

  for md5, _ in result.items():
    print()
    print()
    print()
    print('==', md5)
    for sample, h2songs in result[md5].items():
      print(f'{sample}')
      for h2song in h2songs:
        print(f'  {h2song}')
#  print(json.dumps(result, indent=2))


if __name__ == '__main__':
  main()
