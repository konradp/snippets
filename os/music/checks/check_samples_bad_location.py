# Usage: $0 DIR
# It does:
# - find all .h2song files in the $DIR
# - for each of these
#   - get sample paths
#   - if the path does not start with /music/samples, then print the h2song
#     file and these bad paths
import glob
import sys
import xml.etree.ElementTree as ET
from os.path import exists

def main():
  path_dir = '/music'
  if len(sys.argv) == 2:
    if sys.argv[1] == '-1':
      print(f'Usage: {sys.argv[0]} DIR')
    path_dir = sys.argv[1]
  h2song_files = glob.glob(f'{path_dir}/**/*.h2song', recursive=True)
  for f in h2song_files:
    try:
      tree = ET.parse(f)
    except Exception as e:
      print(f'Exception parsing file {f}', e)
    root = tree.getroot()
    samples = root.findall('.//instrumentComponent//filename')
    paths = []
    for sample in samples:
      path = sample.text
      if path is None:
        continue
      if not exists(path):
        continue
      if not path.startswith('/music/samples'):
        paths.append(path)
    if len(paths) != 0:
      # If found samples, print
      print(f)
      for path in paths:
        print('  ', path)


if __name__ == "__main__":
  main()
