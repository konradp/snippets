# Every /music/song/* should have an info.yml file
import glob
import os
import sys

dir_albums = '/music/songs/albumy'
dir_no_albums = '/music/songs/bez_albumow'

def get_parent_dir(file_path):
  parent_directory = os.path.abspath(os.path.join(file_path, os.pardir))
  return parent_directory

def listdir(path):
  return [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]


def main():
  # Check songs with albums
  global dir_albumy
  global dir_no_albums
  albums = listdir(dir_albums)
  for album in albums:
    if album == 'other':
      continue
    dir_album = os.path.join(dir_albums, album)
    songs = sorted(listdir(dir_album))
    for song in songs:
      file_song_info = os.path.join(dir_album, song, 'info.yml')
      if not os.path.exists(file_song_info):
        print(file_song_info)

  # Check songs without albums
  songs = sorted(listdir(dir_no_albums))
  for song in songs:
    if song in ['00BITY', '00ardour_sessions', '00h2songs', '00rosegarden', '00to_extract_samples', '20230603_gh_01', 'RAP']:
      continue
    file_song_info = os.path.join(dir_no_albums, song, 'info.yml')
    if not os.path.exists(file_song_info):
      print(file_song_info)
  exit(0)


if __name__ == '__main__':
  main()
