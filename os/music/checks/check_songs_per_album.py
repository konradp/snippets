# Every .mp3 in /music/recordings/ should have a corresponding /music/songs/
import glob
import os
import sys


def get_parent_dir(file_path):
  parent_directory = os.path.abspath(os.path.join(file_path, os.pardir))
  return parent_directory


def main():
  print('For each album in /music/recordings, count how many mp3s are in the album')
  dir_albums = '/music/recordings/albumy'
  albums = sorted(os.listdir(dir_albums))
  exclusions = [
    'ZBONUS',
    '00_colab',
    '00_covers',
  ]
  for exclusion in exclusions:
    if exclusion in albums:
      albums.remove(exclusion)
  
  for album in albums:
    #print('==', album)
    rec_files = glob.glob(f'{dir_albums}/{album}/*.mp3', recursive=False)
    count = len(rec_files)
    if count == 12:
      suffix = ''
    elif count < 12:
      suffix = f'++++++++++ +{12 - count}'
    elif count > 12:
      suffix = f'----------- -{count-12}'
    print(f'{album}: {len(rec_files)} {suffix}')


if __name__ == '__main__':
  main()
