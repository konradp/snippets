# Find all .h2song files in the current dir
# if the filename is different than the dirname, raise a warning
import glob
import os
import sys


def get_parent_dir(file_path):
  parent_directory = os.path.abspath(os.path.join(file_path, os.pardir))
  return parent_directory


def main():
  SUCCESS = True
  if len(sys.argv) not in [1, 2]:
    print(f'Usage: {sys.argv[0]} [DIR]')
    exit(1)
  dir_path = '/music'
  if len(sys.argv) == 2:
    dir_path = sys.argv[1]
  pattern = os.path.join(dir_path, '**', '*.h2song')
  h2song_files = glob.glob(pattern, recursive=True)

  # Replace '/path/to/your/directory' with the actual path to the root directory where you want to start searching
  if h2song_files is None:
    print('No .h2song files in the dir specified')
    return

  print('h2 songs with incorrect filenames:')
  for file_path in h2song_files:
    file_dir = get_parent_dir(file_path)
    file_name = os.path.basename(file_path)
    file_name, _ = os.path.splitext(file_name)
    file_dir_name = os.path.basename(file_dir)
    if not file_name.startswith(file_dir_name):
      print(file_path)
      SUCCESS = False
  if SUCCESS:
    exit(0)
  else:
    exit(1)


if __name__ == '__main__':
  main()
