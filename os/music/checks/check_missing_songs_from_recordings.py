# Every .mp3 in /music/recordings/ should have a corresponding /music/songs/
import glob
import os
import sys


def get_parent_dir(file_path):
  parent_directory = os.path.abspath(os.path.join(file_path, os.pardir))
  return parent_directory


def main():
  print('If there is an .mp3 in /music/recordings/album/song1.mp3, then we need to have /music/songs/album/song1/')
  print('Missing dirs:')
  dir_recs = '/music/recordings/albumy'
  dir_songs = '/music/songs/albumy'
  #pattern = os.path.join(dir_path, '**', '*.h2song')
  albums = os.listdir(dir_recs)
  exclusions = [
    'ZBONUS',
    '00_colab',
    '00_covers',
  ]
  for exclusion in exclusions:
    albums.remove(exclusion)
  
  for album in albums:
    #print('==', album)
    rec_files = glob.glob(f'{dir_recs}/{album}/*.mp3', recursive=False)
    for rec_file in rec_files:
      #print('rec', rec)
      song_name = os.path.basename(rec_file)
      song_name, _ = os.path.splitext(song_name)
      song_name = song_name.replace('nickyfow_', '')
      #print('f', file_name)
      if not os.path.exists(f'{dir_songs}/{album}/{song_name}'):
        print(f'{dir_songs}/{album}/{song_name}/')
        #sys.


  exit(0)
  #h2song_files = glob.glob(f'{dir_recs}')

  # Replace '/path/to/your/directory' with the actual path to the root directory where you want to start searching
  if h2song_files is None:
    print('No .h2song files in the dir specified')
    return

  print('== h2 songs with incorrect filenames:')
  for file_path in h2song_files:
    file_dir = get_parent_dir(file_path)
    file_dir_name = os.path.basename(file_dir)
    if not file_name.startswith(file_dir_name):
      print(file_path)


if __name__ == '__main__':
  main()
