# Usage: $0 DIR
# It does:
# - find all .h2song files in the $DIR
# - for each of these:
#   - highlight any paths of the samples which don't exist
import glob
import sys
from os.path import exists
import xml.etree.ElementTree as ET

def main():
  path_dir = '/music'
  h2song_files = glob.glob(f'{path_dir}/**/*.h2song', recursive=True)
  for f in h2song_files:
    bad_samples = []
    tree = ET.parse(f)
    root = tree.getroot()
    samples = root.findall('.//instrumentComponent//filename')
    for sample in samples:
      path = sample.text
      if path is None:
        continue
      if not exists(path):
        bad_samples.append(path)
    if len(bad_samples) != 0:
      # If found bad samples, print
      print(f)
      for sample in bad_samples:
        print('  ', sample)


if __name__ == "__main__":
  main()
