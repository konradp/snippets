#!/bin/bash
SCRIPT_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
echo $SCRIPT_DIR
SUCCESS="true"
for CHECK in $SCRIPT_DIR/*.py; do
  echo ===== $(basename $CHECK)
  python3 $CHECK
  echo
  if [ $? -ne 0 ]; then
    SUCCESS="false"
    continue
  fi
done

echo
if [[ "$SUCCESS" = "true" ]]; then
  echo "OK: All checks passed"
else
  echo "ERROR: Some checks failed"
  exit 1
fi
