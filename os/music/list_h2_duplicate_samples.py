# Usage: $0 DIR
# It does:
# - find all .h2song files in the $DIR
# - for each of these:
#   - get the sample paths used
# - find all duplicate files in the $DIR
#   - print the duplicate files and the .h2song files that use them
# Example
# {
#   'md5hash123123': {
#     '/music/samples/sample1.wav': [
#       '/music/songs/song1.h2song',
#       '/music/songs/song2.h2song'
#     ],
#     '/music/samples/sample1_duplicate.wav': None,
#   },
#   'md5hash123124': { ...
# ...
# }
import glob
import hashlib
import json
import sys
import xml.etree.ElementTree as ET
from os.path import exists


def get_h2_samples(file):
  # Return list of samples used in the h2song file
  paths = []
  try:
    tree = ET.parse(file)
  except Exception as e:
    print(f'Exception parsing file {file}', e)
    exit(1)
  root = tree.getroot()
  samples = root.findall('.//instrumentComponent//filename')
  for sample in samples:
    path = sample.text
    if path is None:
      continue
    paths.append(path)
  return paths


def get_md5(fname):
  hash_md5 = hashlib.md5()
  with open(fname, 'rb') as f:
    for chunk in iter(lambda: f.read(4096), b''):
      hash_md5.update(chunk)
  return hash_md5.hexdigest()


def main():
  if len(sys.argv) != 2:
    print('ERROR')
    print(f'Usage: {sys.argv[0]} DIR')
    exit(1)
  path_dir = sys.argv[1]

  samplepaths = {}

  # Get h2songs and their samples
  h2song_files = glob.glob(f'{path_dir}/**/*.h2song', recursive=True)
  for h2_file in h2song_files:
    song_samples = get_h2_samples(h2_file)
    # Reorganise
    for sample_path in song_samples:
      if not exists(sample_path):
        continue
      md5 = get_md5(sample_path)
      if md5 not in samplepaths:
        samplepaths[md5] = {}
      if sample_path not in samplepaths[md5]:
        samplepaths[md5][sample_path] = []
      if h2_file not in samplepaths[md5][sample_path]:
        samplepaths[md5][sample_path].append(h2_file)

  # Remove the ones which have no duplicates
  for md5 in list(samplepaths):
    if len(samplepaths[md5]) == 1:
      del samplepaths[md5]

  print(json.dumps(samplepaths, indent=2))


if __name__ == "__main__":
  main()
