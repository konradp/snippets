# For a .h2song, and find all the sample paths which do not exist
# Note: Run for all h2song files with:
# for F in $(f .h2song); do hydrogen_status $F; done
# Note: Does not support spaces in file paths
# TODO: other/hydrogen_songs/1.h2song uses different syntax: instrument/instrumentComponent/layer/
import json
import sys
from os.path import isfile
import xml.etree.ElementTree as ET


def raise_if_not_empty(item):
  if item is not None:
    raise Error('ERROR')


def analyse_file(file):
  # read file as xml, get instrument(sample) info and print errors
  # Gather data
  print('===', file)
  try:
    tree = ET.parse(file)
    root = tree.getroot()
  except Exception as e:
    print('ERROR: Could not read file:', file, ':', e)
    exit(0)
  # instruments = [ {id=5, name=Kick, drumkit=GMKit, filename=kick.flac}, {...}, ... ]
  instruments = []
  for instrument in root.findall("./instrumentList/"):
    i = {
      'id': None,
      'name': None,
      'drumkit': None,
      'fileName': None,
    }
    for f in instrument:
      if f.tag == 'id':
        raise_if_not_empty(i['id'])
        i['id'] = f.text
      if f.tag == 'name':
        raise_if_not_empty(i['name'])
        i['name'] = f.text
      if f.tag == 'drumkit':
        raise_if_not_empty(i['drumkit'])
        i['drumkit'] = f.text
      if f.tag == 'layer':
        for layer_field in f:
          if layer_field.tag == 'filename':
            raise_if_not_empty(i['fileName'])
            i['fileName'] = layer_field.text
    instruments.append(i)
  # Check validity of instruments
  for i in instruments:
    errors = []
    if i['drumkit'] != None:
      errors.append('ERROR: drumkit not empty')
    # check fileName exists
    if i['fileName'] is None:
      errors.append('ERROR: filename is empty')
    else:
      if not isfile(i['fileName']):
        errors.append('ERROR: filename does not exist')
    if len(errors) != 0:
      print(f"id ({i['id']}), name ({i['name']}), drumkit ({i['drumkit']}), file ({i['fileName']})")
      for err in errors:
        print(err)
      


def main():
  if len(sys.argv) < 2:
    print(f"Usage: hydrogen_status FILE.h2song")
    print('Example: for F in $(f .h2song); do hydrogen_status $F; done')
    exit(1)
  file = sys.argv[1]
  analyse_file(file)


if __name__ == "__main__":
  main()
