# Usage: $0 DIR
# It does:
# - find all .h2song files in the $DIR
# - for each of these:
#   - print the sample paths used
import glob
import sys
import xml.etree.ElementTree as ET

def main():
  path_dir = '/music'
  h2song_files = glob.glob(f'{path_dir}/**/*.h2song', recursive=True)
  for f in h2song_files:
    try:
      tree = ET.parse(f)
    except Exception as e:
      print(f'Exception parsing file {f}', e)
    root = tree.getroot()
    samples = root.findall('.//instrumentComponent//filename')
    paths = []
    for sample in samples:
      path = sample.text
      if path is None:
        continue
      paths.append(path)
    if len(paths) != 0:
      # If found samples, print
      print('=', f)
      for path in paths:
        print('-', path)


if __name__ == "__main__":
  main()
