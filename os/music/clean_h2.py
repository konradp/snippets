# Usage: hydrogen_clean FILE.h2song
# It does:
# - find sample paths which do not exist
# - move instrument/layer/ to  instrument/instrumentComponent/layer/
#
# Note: Run for all h2song files with:
#   for F in $(f .h2song); do hydrogen_clean $F; done
# Note: Does not support spaces in file paths
# TODO: New file uses one space indent, but ignore that for now

import json
import sys
from os.path import basename, dirname, isfile
# editing xml
import xml.etree.ElementTree as ET
# formatting xml
from xml.dom import minidom


def raise_if_not_empty(item):
  if item is not None:
    raise OSError('ERROR:', item)


def main():
  file = sys.argv[1]
  # Gather data: read file as xml, get instrument(sample) info and print errors
  try:
    tree = ET.parse(file)
    root = tree.getroot()
  except Exception as e:
    print('ERROR: Could not read file:', file, ':', e)
    exit(0)

  # Detect type: 
  # - old: instrument/layer/
  # - new: instrument/instrumentComponent/layer/
  print('Detect type')
  old = root.findall(f"./instrumentList/instrument/layer")
  new = root.findall(f"./instrumentList/instrument/instrumentComponent/layer")
  file_ver = None
  if len(old) > 0 and len(new) == 0:
    file_ver = 'old'
  elif len(old) == 0 and len(new) > 0:
    file_ver = 'new'
  else:
    raise OSError('Unknown type')
  print(f"Detected type: {file_ver}")

  if file_ver == 'old':
    # Move layers into instrumentComponent structure
    # Gather data: old filetype
    print('Gather data: old type')
    for instrument in root.findall("./instrumentList/"):
      idx = instrument.find('./id').text
      layers = instrument.findall("./layer")
      for layer in layers:
        # Remove old layers
        instrument.remove(layer)

      # Create instrumentComponent structure and readd the layers
      instrument_component = ET.Element('instrumentComponent')
      component_id = ET.SubElement(instrument_component, 'component_id')
      component_id.text = '0'
      for layer in layers:
        instrument_component.append(layer)
      instrument.append(instrument_component)


  # Fix layer filenames
  # e.g 00KICK maps to /music/samples/00KICK
  print('Calculate fixes')
  drumkit_map = {
    '00KICK': '/music/samples/00KICK',
    '01KICK': '/music/samples/01KICK',
    '00HAT':  '/music/samples/00HAT',
    '01HAT':  '/music/samples/01HAT',
    '3355606kit': '/music/samples/3355606kit',
    '9': '/music/samples/9',
    '12':     '/music/samples/12',
    '18sac': '/music/samples/18sac',
    'GMkit':  '/music/samples/00kit_GMkit',
    'HAT': '/music/samples/HAT',
    'KICK': '/music/samples/KICK',
    'konlad': '/music/samples/konlad',
    'SNARE': '/music/samples/SNARE',
    '01SNARE': '/music/samples/01SNARE',
    'GITARA1.1': '/music/samples/GITARA1.1',
    'TR808EmulationKit': '/music/samples/00kit_TR808EmulationKit',
    'hawaii1': '/music/samples/hawaii1',
    'rose_lipstick': '/music/songs/00_ALBUMY/Gitarowe_raw/ROSE_LIPSTICK1',
    'sam1': '/music/samples/sam1',
    'sinatra2': '/music/samples/sinatra2',
    'vsample': '/music/samples/vsample',
    'vsample2': '/music/samples/vsample2',
    'piano6nowesample2': '/music/samples/piano6nowesample2',
    'piano': '/music/samples/piano',
    'piano2': '/music/samples/piano2',
    'piano4': '/music/samples/piano4',
    'piano5': '/music/samples/piano5',
    'radio1': '/music/samples/radio1',
    '8_gitara': '/music/samples/8_gitara',
    'Synthie-1': '/music/samples/Synthie-1',
  }
  path_map = {
    '/home/konradjr/NAGRANIA/NAGRANIA/23red_lipstick/': '/music/songs/00_ALBUMY/Gitarowe_raw/ROSE_LIPSTICK/',
    '/home/konradjr/NAGRANIA/SONGS/get_lucky/': '/music/songs/00_ALBUMY/Gitarowe_raw/Covers/get_lucky/',
    '/home/konradjr/NAGRANIA/SONGS/Albumy/Gitarowe_raw/nowe25/': '/music/songs/00_ALBUMY/Gitarowe_raw/horsies/',
    '/home/konradjr/NAGRANIA/SONGS/od_tylu/': '/music/samples/od_tylu/',
    '/home/DATA/HOME/NAGRANIA/SONGS/nowe14/': '/music/samples/nowe14/',
    '/home/konradjr/NAGRANIA/SONGS/Lorene/': '/music/samples/Lorene/',
    '/home/DATA/HOME/NAGRANIA/SONGS/nowe7/': '/music/samples/nowe7/',
    '/home/DATA/HOME/NAGRANIA/SAMPLES/HATS/': '/music/samples/HATS/',
    '/home/DATA/HOME/NAGRANIA/SAMPLES/SNARES/': '/music/samples/SNARES/',
    '/home/DATA/HOME/NAGRANIA/SAMPLES/KICKS/': '/music/samples/KICKS/',
    '/home/konradjr/NAGRANIA/VINYL_SAMPLE/HAT/': '/music/samples/HAT/',
    '/home/DATA/HOME/NAGRANIA/SAMPLES/APARAT/': '/music/samples/APARAT/',
    '/home/konradjr/NAGRANIA/SONGS/Pioro/': '/music/samples/pioro/',
    '/home/konradjr/NAGRANIA/SONGS/nowe1/': '/music/samples/nowe1/',
    '/home/konradjr/NAGRANIA/ROSEGARDEN/8/akord/': '/music/songs/00_ALBUMY/Although/Shuffles/akord/',
    '/home/konradjr/NAGRANIA/NAGRANIA/18loopsac1/': '/music/samples/18sac/',
    '/home/konradjr/NAGRANIA/SONGS/nowe3/': '/music/samples/nowe3/',
    '/home/konradjr/NAGRANIA/SONGS/wakacje1/SAMPLE/': '/music/samples/wakacje1/',
    '/home/konradjr/NAGRANIA/KONLAD/': '/music/samples/konlad/',
    '/home/konradjr/Desktop/mango/': '/music/samples/mango/',
    '/home/konradjr/NAGRANIA/SONGS/spanish/': '/music/samples/spanish/',
    #'/home/konradjr/NAGRANIA/VINYL_SAMPLE/MELODY/': '/music/samples/MELODY/',
    '/home/konradjr/NAGRANIA/VINYL_SAMPLE/MELODY/sinatra1/': '/music/samples/MELODY/sinatra1/',
    '/home/konradjr/NAGRANIA/VINYL_SAMPLE/CHORDS/sinatra/': '/music/samples/CHORDS/sinatra/',
    '/music/samples/DWA/': '/music/samples/vsample/',
    '/home/konradjr/NAGRANIA/SONGS/hot_bun/': '/music/samples/hot_bun/',#
    '/home/konradjr/NAGRANIA/SONGS/bjork/': '/music/samples/BJORK/',
    '/home/tato/bity/AMSTRONGSAMPLE/': '/music/songs/00_BEZ_ALBUMOW/00BITY/AMSTRONGSAMPLE/',
    '/home/tato/bity/samplegitara/': '/music/songs/00_BEZ_ALBUMOW/00BITY/samplegitara/',
    '/home/tato/bity/Vivaldisample/': '/music/songs/00_BEZ_ALBUMOW/00BITY/Vivaldisample/',
    '/home/konradjr/NAGRANIA/ROSEGARDEN/13waterworld/sample/': '/music/samples/13/',
    '/home/konradjr/NAGRANIA/LOOPER/': '/music/samples/looper1/',
    '/home/konradjr/NAGRANIA/SAMPLE/DWA/': '/music/samples/true_nowe/',
    '/music/samples/K-27_Trash_Kit/': '/music/samples/accumulation/',
    '/home/konrad.pisarczyk/OneDrive/music/Konrad/sample/00KICK/': '/music/samples/00KICK/',
    '/music/samples/GITARA/': '/music/samples/SLOW/',
    '/music/samples/SLOW/': '/music/songs/00_BEZ_ALBUMOW/SLOW/SLOW_samples/',
    '/music/songs/00_BEZ_ALBUMOW/SLOW/SLOW_samples/': '/music/samples/SLOW/',
    '/home/konradjr/NAGRANIA/VINYL_SAMPLE/00SNARE/': '/music/samples/00SNARE/',
    #'/music/samples/radio1/': '/music/samples/00kit_TR808EmulationKit/',
    '/home/DATA/NAGRANIA/SAMPLE/SAMPLE2/SAMPLE/SQ2/': '/music/samples/SAMPLE2/SAMPLE/SQ2/',
    '/home/konradjr/NAGRANIA/BITY/sam1/': '/music/samples/sam1/',
    '/home/konradjr/Desktop/DA/': '/music/samples/DA/',
    '/home/konradjr/NAGRANIA/VINYL_SAMPLE/CHORDS/': '/music/samples/CHORDS/',
    #'/home/konradjr/NAGRANIA/VINYL_SAMPLE/MELODY/': '/music/samples/MELODY/',
    '/home/konradjr/NAGRANIA/VINYL_SAMPLE/MELODY/diana_ross/': '/music/samples/ross1/',
    '/home/konradjr/NAGRANIA/SONGS/nowe001/': '/music/samples/nowe001/',
    '/home/konradjr/NAGRANIA/SONGS/uo/': '/music/samples/uo/',
    '/home/konradjr/NAGRANIA/SONGS/Zmulka/': '/music/samples/aZmulka/',
    '/home/konradjr/NAGRANIA/SONGS/tull/': '/music/samples/tull/',
    '/home/konradjr/NAGRANIA/NAGRANIA/19tomorrowyoullbehere/': '/music/songs/00_BEZ_ALBUMOW/TOMORROW_YOULL_BE_HERE/',
    '/home/tato/bity/nowesampleszkola/': '/music/songs/00_BEZ_ALBUMOW/nowesampleszkola/',
  }
  for instrument in root.findall("./instrumentList/"):
    idx = instrument.find('./id').text
    drumkit = instrument.find('./drumkit')
    layers = instrument.findall("./instrumentComponent/layer")
    for layer in layers:
      filename = layer.find('./filename')
      if drumkit.text is not None \
        and drumkit.text in drumkit_map \
        and (not isfile(filename.text) \
        or '/' not in filename.text):
        # Change e.g. k21h.wav from drumkit 01KICK to /music/samples/01KICK/k21h.wav
        filename_new = f"{drumkit_map[drumkit.text]}/{filename.text}"
        if not isfile(filename_new):
          raise OSError(f"ERROR: fname doesn't exist: {filename_new}")
        print(f"UPDATE: instrument {idx}: filename from {filename.text} to {filename_new}")
        filename.text = filename_new

      # Fix paths, e.g. /home/konradjr/.../ to /music/songs/...
      for old_path in path_map:
        if old_path in filename.text:
          filename.text = path_map[old_path] + basename(filename.text)
          print(filename.text)
          #print('FOUND', old_path, filename.text, basename(filename.text))

      if not isfile(filename.text):
        print(f"ERROR: Not a file: {filename.text} for drumkit {drumkit.text}")


  print('Remove old drumkits')
  for instrument in root.findall("./instrumentList/"):
    idx = instrument.find('./id').text
    drumkit = instrument.find(f'./drumkit')
    if drumkit is not None:
      parent = root.find(f"./instrumentList/instrument/[id='{idx}']/drumkit/..")
      parent.remove(drumkit)


  ## Save the XML to a file
  file_out = file + '.cleaned.h2song'
  print('Write file:', file_out)
  xml_string = ET.tostring(root, encoding='utf-8').decode('utf-8')
  dom = minidom.parseString(xml_string)
  pretty_xml = dom.toprettyxml(indent=' ')
  pretty_xml = "\n".join(line for line in pretty_xml.split("\n") if line.strip())
  with open(file_out, 'w') as f:
    f.write(pretty_xml)


if __name__ == "__main__":
  main()
