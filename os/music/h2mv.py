# Move a sample from TARGET to DEST
# update all the h2song files which use this sample to use the new path
#
import hashlib
import os
import sys
import xml.etree.ElementTree as ET
from glob import glob
from os.path import exists
from xml.dom import minidom

# Globals
HOME = '/music'


def usage():
  print(f'Usage: {sys.argv[0]} SRC DEST')
  print(f'Example: {sys.argv[0]} /music/sample1.wav /music/sample2.wav')


#def get_md5(fname):
#  hash_md5 = hashlib.md5()
#  with open(fname, 'rb') as f:
#    for chunk in iter(lambda: f.read(4096), b''):
#      hash_md5.update(chunk)
#  return hash_md5.hexdigest()


def update_song(f_song, src, dst):
  # Update the song file by replacing all instances of SRC with DST
  try:
    tree = ET.parse(f_song)
    root = tree.getroot()
  except Exception as e:
    print(f'ERROR: Could not parse {f_song}')
    print(e)
    exit(1)
  samples = root.findall('.//instrumentComponent//filename')
  for sample in samples:
    sample_path = sample.text
    if sample_path == src:
      sample.text = dst
  xml_string = ET.tostring(root, encoding='utf-8').decode('utf-8')
  dom = minidom.parseString(xml_string)
  pretty_xml = dom.toprettyxml(indent=' ')
  pretty_xml = "\n".join(line for line in pretty_xml.split("\n") if line.strip())
  with open(f_song, 'w') as f:
    f.write(pretty_xml)
  # Delete source
  #if exists(src):
  #  os.remove(src)


def main():
  # Args
  if len(sys.argv) != 3:
    usage()
    print('ERROR: Must provide SRC and DEST')
    exit(1)
  src = sys.argv[1]
  dst = sys.argv[2]
  # Checks
  if not src.startswith('/') or not dst.startswith('/'):
    usage()
    print('ERROR: Must use full paths (start paths with /)')
    exit(1)
  if not exists(src):
    print(f'ERROR: Source file {src} does not exist')
    exit(1)
  if exists(dst):
    print(f'ERROR: Destination file {dst} already exists')
    exit(1)
  print(f'SRC: {src}')
  print(f'DST: {dst}')
  #if get_md5(src) != get_md5(dst):
  #  print(f'ERROR: MD5 sums do not match')
  #  exit(1)

  songs_to_update = []
  songs = glob(f'{HOME}/**/*.h2song', recursive=True)
  for file_song in songs:
    bad_samples = []
    tree = ET.parse(file_song)
    root = tree.getroot()
    samples = root.findall('.//instrumentComponent//filename')
    for sample in samples:
      sample_path = sample.text
      if sample_path == src:
        if file_song not in songs_to_update:
          songs_to_update.append(file_song)
  print('Songs to update:')
  for song in songs_to_update:
    print('-', song)


  # Update
  for song in songs_to_update:
    update_song(song, src, dst)

  # Move the file
  os.rename(src, dst)

if __name__ == '__main__':
  main()
