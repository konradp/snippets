#include <iostream>
#include "App.h"

using std::cout;
using std::endl;

int main() {
  cout << "Hello world" << endl;
  App *app = new App();
  app->Run();
  return 0;
}
