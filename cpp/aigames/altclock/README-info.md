# altclock
Alternating light clock

# Spec
Receive signal on touch A or B to start.
Then, on the output, one second gap:
- A on, B off
- B on, A off
- A on, B off
Note: First response has to be on opposite of initial sig.
E.g. if started with A, respond with B.
Receive signal on touch A or B to stop.

# Senses
There are no bidirectional senses, only one way.  
Senses can be related, e.g. motor related to sense of touch.  
Same here.
IN:
- reward (food/happiness?)
- clock

IN/OUT (related senses):
- inA (touch)
- outA (light bulb)
- inB (touch)
- outB (light bulb)

# Training
State: stopped -> running
- Send signal on A or B (random)
- If received back single sig on A or B within a second, reward
- Note: opposite of start sig


# Brain

# Notes
Notes:
- time sense relative to outside env
- touch both in and out
