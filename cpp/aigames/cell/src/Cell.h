#ifndef CELL_H
#define CELL_H
#include <vector>

using std::make_pair;
using std::pair;
using std::vector;

class Cell {
public:
  Cell();
  ~Cell() {};
  // Methods
  void AddIn(Cell* cell, int weight);
  void Run();

  /* Properties */
  // In: Other cells connecting to ours
  vector<pair<Cell*,int>> ins;
  // Out: The signal
  bool out;

  // Trigger: threshold for firing signal (0-100)
  int trigger;

private:



}; //class Cell
#endif
