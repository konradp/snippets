#include <iostream>
#include <stdexcept>
#include "ConfigRead.hpp"

int main()
{
  std::cout << "Config reader" << std::endl;
  ConfigRead c;
  try {
    std::cout << c.GetValue("key2") << std::endl;
  } catch(const std::exception& e) {
    std::cerr << "Failed reading config: " << std::endl
    << e.what() << std::endl;
  }
}
