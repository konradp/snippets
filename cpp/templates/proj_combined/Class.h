#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

// A simple config file reader

class ConfigRead {
public:
  ConfigRead() : configPath("./config.cfg") {};

  std::string GetValue(std::string _key) {
    try {
      std::ifstream ifs;
      ifs.open(configPath);
      std::string line, key, value;

      if(ifs.is_open()) {
        while(getline(ifs, line)) {
          std::istringstream s(line);
          s >> key >> value;
          // Ignore comments
          if(key == "#") continue;
          if(key == _key) return value;
        }
      }
      throw std::runtime_error("Value not found for key: " + _key);
    } catch(std::exception &e) {
      throw std::runtime_error(e.what());
    }
  };

private:
  std::string configPath;
}; // class ConfigRead

