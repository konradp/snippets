#include <iostream>
#include <alsa/asoundlib.h>

using std::cerr;
using std::cout;
using std::endl;
using std::string;

void die(string s) {
  cerr << s << endl;
  exit(1);
}

int main() {
  // Declare and set vars
  snd_pcm_hw_params_t* hwParams;
  snd_pcm_t*           pcmHandle;
  char*                pcmName;
  snd_pcm_stream_t     str = SND_PCM_STREAM_PLAYBACK;
  pcmName = strdup("hw:0,0");
  snd_pcm_hw_params_alloca(&hwParams);

  if(snd_pcm_open(&pcmHandle, pcmName, str, 0) < 0 )
    die(string("Error opening PCM device ") + pcmName);
  if(snd_pcm_hw_params_any(pcmHandle, hwParams) < 0)
    die("Could not configure PCM device");

  int rate = 44100;
  unsigned int exactRange;
  exactRange = rate;
  int dir;
  int periods = 2;
  snd_pcm_uframes_t periodsize = 8192;

  // Access type
  if(snd_pcm_hw_params_set_access(pcmHandle, hwParams,
      SND_PCM_ACCESS_RW_INTERLEAVED) < 0)
    die("Error setting access");
  if(snd_pcm_hw_params_set_format(pcmHandle, hwParams,
      SND_PCM_FORMAT_S16_LE) < 0)
    die("Error setting format");
  if(snd_pcm_hw_params_set_rate_near(pcmHandle, hwParams,
      &exactRange, 0) < 0)
    die("Error setting rate");
  if(rate != exactRange) die("The rate is not supported");
  if (snd_pcm_hw_params_set_channels(pcmHandle, hwParams, 2) < 0)
    die("Error setting channels.");
  if (snd_pcm_hw_params_set_periods(pcmHandle, hwParams, periods, 0) < 0)
    die("Error setting periods.");
  /* Set buffer size (in frames). The resulting latency is given by */
  /* latency = periodsize * periods / (rate * bytes_per_frame)     */
  if (snd_pcm_hw_params_set_buffer_size(pcmHandle, hwParams,
      (periodsize * periods)>>2) < 0)
    die("Error setting buffersize");
  if (snd_pcm_hw_params(pcmHandle, hwParams) < 0)
    die("Error setting HW params");

  /* Write num_frames frames from buffer data to    */ 
  /* the PCM device pointed to by pcm_handle.       */
  /* Returns the number of frames actually written. */
  unsigned char *data;
  int pcmreturn, l1, l2;
  short s1, s2;
  int frames;
  int num_frames = 3;

  data = (unsigned char *)malloc(periodsize);
  frames = periodsize >> 2;
  for(l1 = 0; l1 < 100; l1++) {
    for(l2 = 0; l2 < num_frames; l2++) {
      s1 = (l2 % 128) * 100 - 5000;  
      s2 = (l2 % 256) * 100 - 5000;  
      data[4*l2] = (unsigned char)s1;
      data[4*l2+1] = s1 >> 8;
      data[4*l2+2] = (unsigned char)s2;
      data[4*l2+3] = s2 >> 8;
    }
    while ((pcmreturn = snd_pcm_writei(pcmHandle, data, frames)) < 0) {
      snd_pcm_prepare(pcmHandle);
      cerr << "Buffer underrun" << endl;
    }
  }


}
