#include "MainWindow.h"
#include <gtkmm.h>
#include <iostream>

int main(int argc, char *argv[])
{
    auto app = Gtk::Application::create(argc, argv, "org.sawtooth");

    MainWindow window;
    return app->run(window);
}
