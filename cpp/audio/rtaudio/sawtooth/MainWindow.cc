#include "MainWindow.h"

#include "rtaudio/RtAudio.h"
#include <gtkmm.h>
#include <iostream>

using std::cout;
using std::endl;

MainWindow::MainWindow()
: m_Box(Gtk::ORIENTATION_VERTICAL, 10),
  m_value(Gtk::Adjustment::create(0.0, 0.0, 101.0, 0.1, 1.0, 1.0)),
  m_Scale(m_value, Gtk::ORIENTATION_VERTICAL),
  m_ButtonPlay("||"),
  k_isPlaying(false),
  // Audio
  bufferFrames(256),
  sampleRate(44100)
{
  // Main box
  m_Scale.set_inverted();

  // Slider
  m_Box.pack_start(m_Scale);
  m_value->signal_value_changed().connect(sigc::mem_fun(*this,
    &MainWindow::on_value_changed));

  // Play button
  m_Box.pack_start(m_ButtonPlay);
  m_ButtonPlay.signal_clicked().connect(sigc::mem_fun(*this,
              &MainWindow::on_play_clicked));

  // Add main box
  add(m_Box);
  show_all_children();
  Init();

}


MainWindow::~MainWindow()
{
}


void
MainWindow::Init() {
  // Set up Audio
  if(dac.getDeviceCount() < 1) {
    std::cout << "\nNo audio devices found!\n";
    exit( 0 );
  }
  parameters.deviceId = dac.getDefaultOutputDevice();
  parameters.nChannels = 2;
  parameters.firstChannel = 0;
  data[0] = 0;
}


void
MainWindow::on_value_changed() {
  const double val = m_value->get_value();
  data[0] = val;
  cout << "V: " << val << endl;
}


void
MainWindow::on_play_clicked() {
  const double val = m_value->get_value();
  k_isPlaying = !k_isPlaying;
  if(k_isPlaying) {
    // Start
    m_ButtonPlay.set_label(">");
    try {
      dac.openStream(&parameters, NULL, RTAUDIO_FLOAT64, sampleRate,
          &bufferFrames, &MainWindow::sawtooth, (void *)&data);
      dac.startStream();
    }
    catch(RtAudioError& e) {
      e.printMessage();
      exit(0);
    }
  } else {
    m_ButtonPlay.set_label("||");
    try {
      // Stop
      dac.stopStream();
    } catch (RtAudioError& e) {
      e.printMessage();
    }
    if(dac.isStreamOpen()) dac.closeStream();


  }
}

// Util
int
MainWindow::sawtooth(void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *userData)
{
  unsigned int i, j;
  double *buffer = (double*) outputBuffer;
  //double (*data)[2] = static_cast<double(*)[2]>(userData);
  double *last = (double *) userData;
  cout << "A: " << last << endl;

  if (status)
    std::cout << "Stream underflow detected!" << std::endl;
  // Write interleaved audio data.
  for ( i=0; i < nBufferFrames; i++ ) {
    *buffer++ = *last;
    *buffer++ = *last;
    *last += 0.015;
    if(*last >= 1.0) *last -= 2.0;
  }
  return 0;
}
