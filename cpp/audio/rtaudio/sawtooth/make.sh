#!/bin/sh
gcc -o main main.cc MainWindow.cc \
  -lrtaudio \
  -lstdc++ \
  `pkg-config gtkmm-3.0 --cflags --libs`
