#ifndef GTKMM_EXAMPLEWINDOW_H
#define GTKMM_EXAMPLEWINDOW_H

//include "backend.h"
#include "rtaudio/RtAudio.h"
#include <gtkmm.h>

class MainWindow : public Gtk::Window {
public:
  MainWindow();
  virtual ~MainWindow();
  void Init();
  static int sawtooth(void *outputBuffer, void *inputBuffer,
      unsigned int nBufferFrames, double streamTime,
      RtAudioStreamStatus status, void *userData);

protected:
  // Signal handlers
  void on_value_changed();
  void on_play_clicked();

  // Child widgets
  Glib::RefPtr<Gtk::Adjustment> m_value;
  Gtk::Scale m_Scale;
  Gtk::Button m_ButtonPlay;

private:
  Gtk::Box m_Box;
  bool k_isPlaying;
  // Stream params
  RtAudio dac;
  unsigned int sampleRate;
  RtAudio::StreamParameters parameters;
  unsigned int bufferFrames;
  double data[2];

};

#endif //GTKMM_EXAMPLEWINDOW_H
