#include <iostream>
#include <stdexcept>
#include <unistd.h>
#include <rtmidi/RtMidi.h>

void cleanup(RtMidiIn *midiin, RtMidiOut *midiout) {
  delete midiin;
  delete midiout;
}

std::vector<unsigned char> colorMessage(int _button, int color) {
  // color:
  // 0x00 - black (off)
  // 0x01 - red
  // 0x04 - green
  // 0x05 - yellow
  // 0x10 - blue
  // 0x11 - magenta
  // 0x14 - cyan
  // 0x7F - white
  std::vector<unsigned char> message;
  int button = 0x70 + _button;
  message.clear();
  message.push_back(0xF0);
  message.push_back(0x00);
  message.push_back(0x20);
  message.push_back(0x6B);
  message.push_back(0x7F);
  message.push_back(0x42);
  message.push_back(0x02);
  message.push_back(0x00);
  message.push_back(0x10);
  message.push_back(button); // button
  message.push_back(color); // color
  message.push_back(0xF7);
  return message;
}

int main() {
  RtMidiIn *midiin = 0;
  RtMidiOut *midiout = 0;
  std::vector<unsigned char> message;

  // Input
  try {
    midiin = new RtMidiIn();
  } catch (RtMidiError &error) {
    error.printMessage();
    exit(EXIT_FAILURE);
  }
  unsigned int nPorts = midiin->getPortCount();
  std::cout << "\nThere are " << nPorts << " MIDI input sources available.\n";
  std::string portName;
  for (unsigned int i=0; i<nPorts; i++) {
    try {
      portName = midiin->getPortName(i);
    }
    catch (RtMidiError &error) {
      error.printMessage();
      cleanup(midiin, midiout);
    }
    std::cout << "  Input Port #" << i+1 << ": " << portName << '\n';
  }

  // Output
  try {
    midiout = new RtMidiOut();
  }
  catch ( RtMidiError &error ) {
    error.printMessage();
    exit( EXIT_FAILURE );
  }
  nPorts = midiout->getPortCount();
  if ( nPorts == 0 ) {
    std::cout << "No ports available!\n";
    cleanup(midiin, midiout);
  }
  std::cout << "\nThere are " << nPorts << " MIDI output ports available.\n";
  for ( unsigned int i=0; i<nPorts; i++ ) {
    try {
      portName = midiout->getPortName(i);
    }
    catch (RtMidiError &error) {
      error.printMessage();
      cleanup(midiin, midiout);
    }
    std::cout << "  Output Port #" << i+1 << ": " << portName << '\n';
  }
  std::cout << '\n';

  // Send
  midiout->openPort(1);

  // SysEx
  // color:
  // 00 - black (off)
  // 01 - red
  // 04 - green
  // 05 - yellow
  // 10 - blue
  // 11 - magenta
  // 14 - cyan
  // 7F - white
  std::vector<unsigned char> msg;
  std::vector<unsigned char> colors = {
    0x00, 0x01, 0x04, 0x05, 0x10, 0x11, 0x14, 0x7F, 0x00
  };
  for (int i : colors) {
    msg = colorMessage(1, i);
    std::cout << "a: " << std::hex << "0x" << i << std::endl;
    midiout->sendMessage(&msg);
    usleep(1000000/2);
  }
  //msg = colorMessage(1, 0x04);
  //
  //midiout->sendMessage(&msg);
  //sleep(2);


  cleanup(midiin, midiout);
  return 0;
}
