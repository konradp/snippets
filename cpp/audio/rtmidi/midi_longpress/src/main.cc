#include <iostream>
#include <unistd.h>
#include <uv.h>
#include <stdlib.h> // uv
#include <stdio.h> // uv
#include <rtmidi/RtMidi.h>
#include <thread>

using std::cout;
using std::endl;

// Global
RtMidiIn *midiin = 0;
RtMidiOut *midiout = 0;
std::vector<unsigned char> message;
bool is_pad_on = false;
uv_loop_t *loop;
uv_timer_t timer_req;
bool stop = false;


void run() {
  while (!stop) {
    uv_run(loop, UV_RUN_DEFAULT);
  }
}

int usage() {
  cout << "Usage: ./main PORT" << endl;
  cout << "Example ./main 1" << endl;
  return 1;
}


void timer_callback(uv_timer_t* handle) {
  cout << "Callback: " << endl;
  if (is_pad_on) {
    cout << "Long press" << endl;
  } else {
    cout << "--------------" << endl;
  }
  //uv_timer_stop(&timer_req);
  //uv_handle_t* h = (uv_handle_t*) &timer_req;
  //uv_close(h, NULL);
}


void cleanup() {
  delete midiin;
  delete midiout;
}

void mycallback(double deltatime, std::vector<unsigned char> *message,
  void *userData) {
  if ((int)message->at(0) == 153) {
    // Pad on
    cout << "PAD ON" << endl;
    //uv_handle_t* handle = (uv_handle_t*) &timer_req;
    //uv_close(handle, NULL);
    //uv_timer_init(loop, &timer_req);
    uv_timer_start(&timer_req, timer_callback, 1000, 0);
    is_pad_on = true;
    //uv_print_active_handles(loop, stdout);
    uv_handle_t* handle = (uv_handle_t*) &timer_req;
    cout << "active: " << uv_is_active(handle) << endl;
    //uv_handle_t* handle = (uv_handle_t*) &timer_req;
    //cout << "active: " << uv_is_active(handle) << endl;
  } else if ((int)message->at(0) == 137) {
    // Pad off
    cout << "PAD OFF" << endl;
    uv_timer_stop(&timer_req);
    is_pad_on = false;
    //uv_print_all_handles(loop, stdout);
    //uv_handle_t* handle = (uv_handle_t*) &timer_req;
    //uv_close(handle, NULL);
    //if (uv_is_active(handle) && !uv_is_closing(handle)) {
    //if (uv_is_active(handle)) {
    //  cout << "Is active" << endl;
    //}
    //if (!uv_is_closing(handle)) {
    //  cout << "close" << endl;
    //  uv_close(handle, NULL);
    //}
    //uv_timer_init(loop, &timer_req);
    //if (uv_is_active(handle)) {
    //  cout << "active" << endl;
    //}
    //uv_print_active_handles(loop, stdout);
    //uv_handle_t* handle = (uv_handle_t*) &timer_req;
    //cout << "active: " << uv_is_active(handle) << endl;
  } else {
    return;
  }
}

void printPorts() {
  unsigned int nPorts = midiin->getPortCount();
  std::cout << "\nThere are " << nPorts << " MIDI input sources available.\n";
  std::string portName;
  for (unsigned int i=0; i<nPorts; i++) {
    try {
      portName = midiin->getPortName(i);
    } catch (RtMidiError &error) {
      error.printMessage();
      cleanup();
    }
    std::cout << "  Input Port #" << i+1 << ": " << portName << '\n';
  }

  // Output
  nPorts = midiout->getPortCount();
  if ( nPorts == 0 ) {
    std::cout << "No ports available!\n";
    cleanup();
  }
  std::cout << "\nThere are " << nPorts << " MIDI output ports available.\n";
  for ( unsigned int i=0; i<nPorts; i++ ) {
    try {
      portName = midiout->getPortName(i);
    }
    catch (RtMidiError &error) {
      error.printMessage();
      cleanup();
    }
    std::cout << "  Output Port #" << i+1 << ": " << portName << '\n';
  }
  std::cout << '\n';
  return;
}


int main(int argc, char **argv) {
  // Input
  try {
    midiin = new RtMidiIn();
  } catch (RtMidiError &error) {
    error.printMessage();
    exit(EXIT_FAILURE);
  }

  // Output
  try {
    midiout = new RtMidiOut();
  } catch ( RtMidiError &error ) {
    error.printMessage();
    exit( EXIT_FAILURE );
  }

  if (argc == 1) {
    printPorts();
    return usage();
  } else if (argc != 2) {
    return usage();
  }

  int port = atoi(argv[1]) - 1;

  // Receive
  midiin->openPort(port);
  midiin->setCallback(&mycallback);
  midiin->ignoreTypes(false, false, false);

  // EVENTS
  cout << "Init loop" << endl;
  loop = uv_default_loop();
  uv_timer_init(loop, &timer_req);
  std::thread t(run);
  //uv_run(loop, UV_RUN_DEFAULT);
  //while (true) {
  //  //cout << "Running loop" << endl;
  //  uv_run(loop, UV_RUN_DEFAULT);
  //}
  while (1) {
    char c = getchar();
    if (c == 'q' || c == 'Q')
     break;
  };
  stop = true;
  cout << "Quitting" << endl;
  uv_stop(loop);
  t.join();
  uv_handle_t* h = (uv_handle_t*) &timer_req;
  uv_close(h, NULL);
  uv_loop_close(loop);

  //delete timer_req;
  //&timer_req = NULL;
  //delete loop;
  //loop = NULL;

  cout << "Cleanup" << endl;
  cleanup();
  return 0;
}
