// Send MIDI program change to a specified port, e.g.
// ./main 1 2
// sends MIDI msg to port 1, instructing to change the program
// to program 2
#include <iostream>
#include <stdexcept>
#include <unistd.h>
#include <rtmidi/RtMidi.h>

using std::cout;
using std::endl;

RtMidiIn *midiin = 0;
RtMidiOut *midiout = 0;
std::vector<unsigned char> message;

int usage() {
  cout << "Usage: ./main PORT PROGRAM" << endl;
  cout << "Example: ./main 1 0" << endl;
  return 1;
}

void cleanup() {
  delete midiin;
  delete midiout;
}

std::vector<unsigned char> programMsg(int program) {
  std::vector<unsigned char> message;
  message.clear();
  message.push_back(0x2);
  message.push_back(192);
  message.push_back(program);
  return message;
}

void printPorts() {
  unsigned int nPorts = midiin->getPortCount();
  std::cout << "\nThere are " << nPorts << " MIDI input sources available.\n";
  std::string portName;
  for (unsigned int i=0; i<nPorts; i++) {
    try {
      portName = midiin->getPortName(i);
    }
    catch (RtMidiError &error) {
      error.printMessage();
      cleanup();
    }
    std::cout << "  Input Port #" << i+1 << ": " << portName << '\n';
  }

  // Output
  nPorts = midiout->getPortCount();
  if ( nPorts == 0 ) {
    std::cout << "No ports available!\n";
    cleanup();
  }
  std::cout << "\nThere are " << nPorts << " MIDI output ports available.\n";
  for ( unsigned int i=0; i<nPorts; i++ ) {
    try {
      portName = midiout->getPortName(i);
    } catch (RtMidiError &error) {
      error.printMessage();
      cleanup();
    }
    std::cout << "  Output Port #" << i+1 << ": " << portName << '\n';
  }
  std::cout << '\n';
}

int main(int argc, char **argv) {
  // Input
  try {
    midiin = new RtMidiIn();
  } catch (RtMidiError &error) {
    error.printMessage();
    exit(EXIT_FAILURE);
  }
  // Output
  try {
    midiout = new RtMidiOut();
  } catch (RtMidiError &error) {
    error.printMessage();
    exit(EXIT_FAILURE);
  }

  // Arguments
  if (argc == 1) {
    printPorts();
    return usage();
  } else if (argc != 3) {
    return usage();
  }

  int port = atoi(argv[1]) - 1;
  int program = atoi(argv[2]);

  // Send
  midiout->openPort(port);

  cout << "Sending msg" << endl;

  std::vector<unsigned char> msg;
  msg = programMsg(program);
  midiout->sendMessage(&msg);

  cleanup();
  return 0;
}
