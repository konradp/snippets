#include <iostream>
#include <stdexcept>
#include <unistd.h>
#include <rtmidi/RtMidi.h>

using std::cout;
using std::endl;

// Global
RtMidiIn *midiin = 0;
RtMidiOut *midiout = 0;
std::vector<unsigned char> message;

int usage() {
  cout << "Usage: ./main PORT" << endl;
  cout << "Example ./main 1" << endl;
  return 1;
}

void cleanup() {
  delete midiin;
  delete midiout;
}

void mycallback(double deltatime, std::vector<unsigned char> *message, void *userData)
{
  unsigned int nBytes = message->size();
  for (unsigned int i=0; i<nBytes; i++) {
    std::cout << "Byte " << i << " = " << (int)message->at(i) << ", ";
  }
  if (nBytes > 0) {
    std::cout << "stamp = " << deltatime << std::endl;
  }
}

void printPorts() {
  unsigned int nPorts = midiin->getPortCount();
  std::cout << "\nThere are " << nPorts << " MIDI input sources available.\n";
  std::string portName;
  for (unsigned int i=0; i<nPorts; i++) {
    try {
      portName = midiin->getPortName(i);
    } catch (RtMidiError &error) {
      error.printMessage();
      cleanup();
    }
    std::cout << "  Input Port #" << i+1 << ": " << portName << '\n';
  }

  // Output
  nPorts = midiout->getPortCount();
  if ( nPorts == 0 ) {
    std::cout << "No ports available!\n";
    cleanup();
  }
  std::cout << "\nThere are " << nPorts << " MIDI output ports available.\n";
  for ( unsigned int i=0; i<nPorts; i++ ) {
    try {
      portName = midiout->getPortName(i);
    }
    catch (RtMidiError &error) {
      error.printMessage();
      cleanup();
    }
    std::cout << "  Output Port #" << i+1 << ": " << portName << '\n';
  }
  std::cout << '\n';
  return;
}


int main(int argc, char **argv) {
  // Input
  try {
    midiin = new RtMidiIn();
  } catch (RtMidiError &error) {
    error.printMessage();
    exit(EXIT_FAILURE);
  }

  // Output
  try {
    midiout = new RtMidiOut();
  } catch ( RtMidiError &error ) {
    error.printMessage();
    exit( EXIT_FAILURE );
  }

  if (argc == 1) {
    printPorts();
    return usage();
  } else if (argc != 2) {
    return usage();
  }

  int port = atoi(argv[1]) - 1;

  // Receive
  midiin->openPort(port);
  midiin->setCallback(&mycallback);
  midiin->ignoreTypes(false, false, false);
  cout << "Reading at port " << port << endl;
  cout << "Press enter to quit." << endl;
  char input;
  std::cin.get(input);

  std::vector<unsigned char> msg;

  cleanup();
  return 0;
}
