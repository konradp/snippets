// This is my reworking of this file for study purposes
// See LICENSE for where I copied the file from
#include <iostream>
#include <stdio.h>
#include <math.h>
#include "portaudio.h"

#define NUM_SECONDS   (5)
#define SAMPLE_RATE   (44100)
#define FRAMES_PER_BUFFER  (64)
#ifndef M_PI
#define M_PI  (3.14159265)
#endif
#define TABLE_SIZE   (200)

using std::cout;
using std::endl;

class Sine {
public:
  Sine() : stream(0), left_phase(0), right_phase(0) {
    /* initialise sinusoidal wavetable */
    for(int i=0; i<TABLE_SIZE; i++) {
        sine[i] = (float) sin( ((double)i/(double)TABLE_SIZE) * M_PI * 2. );
    }
    sprintf(message, "No Message");
  }


  bool open(PaDeviceIndex index) {
    PaStreamParameters outputParameters;
    outputParameters.device = index;
    if (outputParameters.device == paNoDevice) {
      return false;
    }
    const PaDeviceInfo* pInfo = Pa_GetDeviceInfo(index);
    if (pInfo != 0) {
      printf("Output device name: '%s'\r", pInfo->name);
    }
    outputParameters.channelCount = 2;       /* stereo output */
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;
    PaError err = Pa_OpenStream(
      &stream,
      NULL,       // no input
      &outputParameters,
      SAMPLE_RATE,
      paFramesPerBufferUnspecified,
      paClipOff,  // no out of range samples no clipping
      &Sine::paCallback,
      this        // using 'this' in userData to cast to Sine* in paCallback method
    );
    if (err != paNoError) {
      // Failed to open stream to device
      return false;
    }
    err = Pa_SetStreamFinishedCallback( stream, &Sine::paStreamFinished );
    if (err != paNoError) {
      Pa_CloseStream(stream);
      stream = 0;
      return false;
    }
    return true;
  }


  bool close() {
    if (stream == 0) return false;
    PaError err = Pa_CloseStream(stream);
    stream = 0;
    return (err == paNoError);
  }


  bool start() {
    if (stream == 0) return false;
    PaError err = Pa_StartStream(stream);
    return (err == paNoError);
  }


  bool stop() {
    if (stream == 0) return false;
    PaError err = Pa_StopStream(stream);
    return (err == paNoError);
  }

private:
  // Instance callback: has access to every method/variable in object of class Sine
  int paCallbackMethod(const void *inputBuffer, void *outputBuffer,
      unsigned long framesPerBuffer,
      const PaStreamCallbackTimeInfo* timeInfo,
      PaStreamCallbackFlags statusFlags)
  {
    float *out = (float*)outputBuffer;
    unsigned long i;
    (void) timeInfo; /* Prevent unused variable warnings. */
    (void) statusFlags;
    (void) inputBuffer;
    for (i=0; i<framesPerBuffer; i++) {
      *out++ = sine[left_phase];  /* left */
      *out++ = sine[right_phase];  /* right */
      left_phase += 1;
      if (left_phase >= TABLE_SIZE) left_phase -= TABLE_SIZE;
      right_phase += 3; /* higher pitch so we can distinguish left and right. */
      if (right_phase >= TABLE_SIZE) right_phase -= TABLE_SIZE;
    }
    return paContinue;
  }


  /* This routine will be called by the PortAudio engine when audio is needed.
     It may called at interrupt level on some machines so don't do anything
     that could mess up the system like calling malloc() or free()
  */
  static int paCallback( const void *inputBuffer, void *outputBuffer,
      unsigned long framesPerBuffer,
      const PaStreamCallbackTimeInfo* timeInfo,
      PaStreamCallbackFlags statusFlags,
      void *userData )
  {
    /* Cast userData to Sine* type so can call the instance method
       paCallbackMethod; we can do that since we called Pa_OpenStream with
       'this' for userData
    */
    return ((Sine*)userData)->paCallbackMethod(inputBuffer, outputBuffer,
        framesPerBuffer,
        timeInfo,
        statusFlags);
  }


  void paStreamFinishedMethod() {
    printf("Stream Completed: %s\n", message);
  }


  // Called by portaudio when playback is done
  static void paStreamFinished(void* userData) {
    return ((Sine*)userData)->paStreamFinishedMethod();
  }


  PaStream *stream;
  float sine[TABLE_SIZE];
  int left_phase;
  int right_phase;
  char message[20];
}; // class Sine


class ScopedPaHandler {
public:
  ScopedPaHandler()
      : _result(Pa_Initialize()) {}
  ~ScopedPaHandler() {
    if (_result == paNoError) {
      Pa_Terminate();
    }
  }

  PaError result() const { return _result; }

private:
    PaError _result;
}; // class ScopedPaHandler


/*******************************************************************/
//int main(void);
int main(void)
{
  Sine sine;
  cout << "PortAudio Test: output sine wave. SR = " << SAMPLE_RATE
       << ", BufSize = " << FRAMES_PER_BUFFER << endl;
  ScopedPaHandler paInit;
  if (paInit.result() != paNoError) goto error;
  if (sine.open(Pa_GetDefaultOutputDevice())) {
    if (sine.start()) {
      printf("Play for %d seconds.\n", NUM_SECONDS );
      Pa_Sleep( NUM_SECONDS * 1000 );
      sine.stop();
    }
    sine.close();
  }
  printf("Test finished.\n");
  return paNoError;

error:
  fprintf( stderr, "An error occured while using the portaudio stream\n" );
  fprintf( stderr, "Error number: %d\n", paInit.result() );
  fprintf( stderr, "Error message: %s\n", Pa_GetErrorText( paInit.result() ) );
  return 1;
}
