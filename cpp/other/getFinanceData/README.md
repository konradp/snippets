# Yahoo data download
These are some old snippets from when I was studying the Yahoo finance API

Example:

<https://query1.finance.yahoo.com/v7/finance/download/ANTO.L?period1=1519675442&period2=1522094642&interval=1d&events=history&crumb=6EtRv7548bk>


Split:
```
https://query1.finance.yahoo.com
/v7
/finance
/download
/ANTO.L
?period1=1519675494
&period2=1522094694
&interval=1d
&events=history
&crumb=cookie
```

## Analysis

```
crumb: ldsqhurcGnt

NEEDED
Name: B
site: yahoo.com
Content: b490qp5dc1tlm&b=3&s=6d
Domain: .yahoo.com
Path: /
Send For: Any type of connection
Expires: April 2, 2019, 11:10:53 AM UTC

NOT NEEDED
Name: PRF
site: finance.yahoo.com
Content: t%3DANTO.L
Domain: .finance.yahoo.com
Path: /
Send For: Any type of connection
Expires: April 1, 2020, 10:42:33 PM UTC
```

```
crumb: LoT3J4QWBjB

Name: B
site: yahoo.com
Content: bbtkn8ldc4ut2&b=3&s=74
Domain: .yahoo.com
Path: /
Send For: Any type of connection
Expires: April 2, 2019, 6:56:19 PM UTC

Name: PRF
site: finance.yahoo.com
Content: t%3DANTO.L
Domain: .finance.yahoo.com
Path: /
Send For: Any type of connection
Expires: April 2, 2020, 6:28:56 AM UTC
```
---

### Comparison:
```
crumb: ldsqhurcGnt
Content: b490qp5dc1tlm&b=3&s=6d
Expires: April 2, 2019, 11:10:53 AM UTC
Name: PRF
Content: t%3DANTO.L
Expires: April 1, 2020, 10:42:33 PM UTC

crumb: LoT3J4QWBjB
Content: bbtkn8ldc4ut2&b=3&s=74
Expires: April 2, 2019, 6:56:19 PM UTC
Name: PRF
Content: t%3DANTO.L
Expires: April 2, 2020, 6:28:56 AM UTC
```

t%3DRIO.L%252BANTO.L

**Old crumb**: 6EtRv7548bk
