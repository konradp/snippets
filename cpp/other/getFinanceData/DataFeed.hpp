#ifndef DATA_FEED_HPP
#define DATA_FEED_HPP

#include "lib/net.hpp"
#include "lib/tinydate.hpp"
#include <iostream>

using Tinydate::date;

class DataFeed {
public:
    DataFeed() {};

    // Getters
    virtual std::string Get() = 0;
    // Setters
    virtual void SetTicker(std::string t) = 0;
    virtual void SetDateFrom(Tinydate::date d) = 0;
    virtual void SetDateTo(Tinydate::date d) = 0;
private:
    std::string mUrl;
    virtual void PrepareUrl() = 0;

}; // class YhooFeed

#endif // DATA_FEED_HPP

