#include "lib/net.hpp"
#include "lib/tinydate.hpp"
#include "YhooFeed.hpp"
#include <exception>
#include <iostream>

using Tinydate::date;

int main()
{
    try {
        YhooFeed f;
        f.SetTicker("ANTO.L");
        f.SetDateFrom(date("2018-03-10"));
        f.SetDateTo(date("2018-03-20"));
        f.SetCookie("ldsqhurcGnt");

        std::cout << "Url: " << f.GetUrl() << std::endl;
        std::cout << "Got: " << std::endl << f.Get() << std::endl;
    } catch (const std::exception& e) {
        std::cout << "Exception: " << e.what() << std::endl;
    }
}
