#ifndef YHOO_FEED_HPP
#define YHOO_FEED_HPP

#include "lib/net.hpp"
#include "lib/tinydate.hpp"
#include "DataFeed.hpp"
#include <exception>
#include <iostream>
#include <string>

using Tinydate::date;

class YhooFeed : public DataFeed {
public:
    YhooFeed() {
    };

    // Getters
    std::string Get() {
        Net net;
        //return "bla";
        return net.get(mUrl);
    };
    std::string GetUrl() {
        Validate();
        PrepareUrl();
        return mUrl;
    };

    // Setters
    void SetTicker(std::string t) { mTicker = t; };
    void SetDateFrom(Tinydate::date d) { mDateFrom = d.GetTimestamp(); };
    void SetDateTo(Tinydate::date d) { mDateTo = d.GetTimestamp(); };
    void SetCookie(std::string c) { mCookie = c; };

    // Other
    void Validate() {
        if(mCookie.empty())
            throw std::runtime_error("Cookie is not set");
        if(mDateFrom == 0)
            throw std::runtime_error("DateFrom is not set");
        if(mDateTo == 0)
            throw std::runtime_error("DateTo is not set");
        if(mTicker.empty())
            throw std::runtime_error("Ticker is not set");
    };

private:
    std::string mCookie;
    std::string mUrl;
    time_t mDateFrom = 0;
    time_t mDateTo = 0;
    std::string mTicker;
    void PrepareUrl() {
        std::ostringstream os;
        os << "https://query1.finance.yahoo.com"
           << "/v7"
           << "/finance"
           << "/download"
           << "/" << mTicker
           << "?period1=" << mDateFrom
           << "&period2=" << mDateTo
           << "&interval=1d"
           << "&events=history"
           << "&crumb=" << mCookie;
        mUrl = os.str();
    };

}; // class YhooFeed

#endif // YHOO_FEED_HPP

