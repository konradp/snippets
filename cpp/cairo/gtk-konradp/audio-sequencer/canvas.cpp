#include "canvas.h"
#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::pair;

bool CCanvas::on_motion_notify_event(GdkEventMotion *event)
{
  m_tMousePos = SPoint{*event};
  queue_draw();
  return true;
}

bool CCanvas::on_draw(Cairo::RefPtr<Cairo::Context> const & cr)
{
  struct pos {
    int x;
    int y;
  };
  vector <pos> v = {
    { 1, 2 },
    { 2, 3 },
    { 3, 4 }
  };
  pos cursor = { 5, 2 };

  Gtk::Allocation allocation { get_allocation() };
  auto const width { (double)allocation.get_width() };
  auto const height { (double)allocation.get_height() };

  cr->set_source_rgb(0,0,0); // black
  cr->set_line_width(3);

  // Grid
  int n_x = 10;
  int n_y = 5;
  // horiz lines
  for (int i = 0; i < n_y; i++) {
      cr->move_to(0, i*height/n_y);
      cr->line_to(width, i*height/n_y);
      cr->stroke();
  }
  // vert lines
  for (int i = 0; i < n_x; i++) {
      cr->move_to(i*width/n_x, 0);
      cr->line_to(i*width/n_x, height);
      cr->stroke();
  }

  // Draw dots
  for (auto i: v) {
    cout << i.x << endl;
    cr->arc(i.x*width/n_x, i.y*height/n_y, 10, 0, 2*M_PI);
    cr->fill();
  }

  // Draw cursor
  cr->arc(cursor.x*width/n_x, cursor.y*height/n_y, 15, 0, 2*M_PI);
  cr->stroke();

  // draw a blue circle at last mouse position
  cr->arc(m_tMousePos.x, m_tMousePos.y, 10, 0, 2*M_PI);
  cr->fill();

  return true;
}
