#include "canvas.h"
#include <gtkmm/application.h>
#include <gtkmm/window.h>

// DEBUG
#include <iostream>
using std::endl;
using std::cout;

static gboolean
clicked(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
  cout << "aa" << endl;
  if (event->keyval == GDK_KEY_space)
  {
    cout << "SPACE KEY PRESSED!" << endl;
    return TRUE;
  }
  return FALSE;
}



int
main(int argc, char** argv)
{
  auto app = Gtk::Application::create(argc, argv, "org.gtkmm.cairo.tut");

  Gtk::Window win;
  win.resize(800,600);
  win.set_title("Cairo tutorial C++");

  // Events
  //gtk_widget_add_events(&c, GDK_BUTTON_PRESS_MASK);
  //g_signal_connect(G_OBJECT(win), "key-press-event",
  //  G_CALLBACK(clicked), NULL);

  CCanvas c;
  win.add(c);
  c.show();

  return app->run(win);
}
