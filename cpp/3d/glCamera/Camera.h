#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp> // rotate()
#include <glm/gtc/matrix_transform.hpp> // matrices, lookAt()

/* Based on Nghia Ho's Camera */
/* Based on hmazhar's moderngl_camera */

class Camera {
public:
    Camera();
    ~Camera() {}
    void      DebugVec();
    void      Refresh();

    // Getters
    glm::mat4 GetMvpMatrix();

    // Setters
    void      SetClipping(double near_clip, double far_clip);
    void      SetFoV(double fov);
    void      SetLookAt(glm::vec3 pos);
    void      SetPos(glm::vec3 pos);
    void      SetUp(glm::vec3 pos);
    void      SetViewport(int loc_x, int loc_y, int width, int height);

    // Movement
    void Move(float incr);
    void Strafe(float incr);
    void Fly(float incr);

    // Rotations
    void RotatePitch(float delta);
    void RotateRoll(float delta);
    void RotateYaw(float delta);

private:
    float m_x, m_y, m_z; // Position
    float m_lx, m_ly, m_lz; // LookAt direction

    // Rotation angles: up/down, tilt, left/right
    float m_RotYaw;
    float m_RotPitch;
    float m_RotRoll;

    // Vectors: position
    glm::vec3 m_CamVecPos;      // Position of camera (coords)
    glm::vec3 m_CamVecPosDelta; // Delta/change of camera position
    glm::vec3 m_CamVecLookAt;   // Point we are looking at (coords)
    // Unit vector
    glm::vec3 m_CamVecDir;
    glm::vec3 m_CamVecUp;

    // Matrices
    glm::mat4 m_MatModel;
    glm::mat4 m_MatView;
    glm::mat4 m_MatProjection;
    glm::mat4 m_MatMvp;

    // Constants
    double k_Aspect;
    double k_FieldOfView;
    double k_NearClip;
    double k_FarClip;

    // Other constants
    int k_ViewportX;
    int k_ViewportY;
    int k_WinWidth;
    int k_WinHeight;

};
#endif
