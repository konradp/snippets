#include "Camera.h"

#include <GL/glut.h>
#include <iostream>
#include <math.h>

Camera::Camera()
{
    // Set angles and initial position
    m_RotYaw = 0.0;
    m_RotPitch = 0.0;
    m_RotRoll = 0.0;

    // Camera settings
    SetFoV(45);
    SetLookAt(glm::vec3(0, 0, 0));
    SetPos(glm::vec3(0, 0, -1));
    SetUp(glm::vec3(0, 1, 0));
    SetClipping(.1, 1000); // Extremely important!!!! Won't render otherwise
}

void
Camera::DebugVec()
{
    std::cout << "m_CamVecPos: "
        << m_CamVecPos.x << " : "
        << m_CamVecPos.y << " : "
        << m_CamVecPos.z << std::endl;

    std::cout << "m_CamVecDir: "
        << m_CamVecDir.x << " : "
        << m_CamVecDir.y << " : "
        << m_CamVecDir.z << std::endl;

    std::cout << "m_CamVecUp: "
        << m_CamVecUp.x << " : "
        << m_CamVecUp.y << " : "
        << m_CamVecUp.z << std::endl;

    std::cout << "m_CamVecLookAt: "
        << m_CamVecLookAt.x << " : "
        << m_CamVecLookAt.y << " : "
        << m_CamVecLookAt.z << std::endl;

    std::cout << "k_FieldOfView: " << k_FieldOfView << std::endl;

    std::cout << "------" << std::endl;
}


void
Camera::Refresh()
{
    // Set matrix state. Otherwise lighting doesn't work
    glViewport(k_ViewportX, k_ViewportY, k_WinWidth, k_WinHeight);

    // Camera direction unit vector
    m_CamVecDir = glm::normalize(m_CamVecLookAt - m_CamVecPos);

    // Quaternions: roll, pitch, yaw
    glm::quat rRoll = glm::angleAxis(m_RotRoll, m_CamVecDir);
    glm::vec3 pitchAxis = glm::cross(m_CamVecDir, m_CamVecUp);
    glm::quat rPitch = glm::angleAxis(m_RotPitch, pitchAxis);
    glm::quat rYaw = glm::angleAxis(m_RotYaw, m_CamVecUp);

    /* AXIS VECTOR ROTATIONS */
    // Direction unit vector rotation: affected by PITCH + YAW rotations
    glm::quat rTmp = glm::normalize(glm::cross(rPitch, rYaw));
    m_CamVecDir = glm::rotate(rTmp, m_CamVecDir);
    
    // Up vector rotation: affected by PITCH + ROLL rotations
    glm::quat rTmp2  = glm::cross(rPitch, rRoll);
    m_CamVecUp = glm::rotate(rTmp2, m_CamVecUp);

    // Increment camera position, lookAt is in 'front' of cam
    m_CamVecPos += m_CamVecPosDelta;
    m_CamVecLookAt = m_CamVecPos + ( m_CamVecDir * 1.0f );
}


glm::mat4
Camera::GetMvpMatrix()
{
    // Compute the MVP matrix
    m_MatModel = glm::mat4(1.0f);
    m_MatView = glm::lookAt(m_CamVecPos, m_CamVecLookAt, m_CamVecUp);
    m_MatProjection = glm::perspective(
        k_FieldOfView,
        k_Aspect,
        k_NearClip,
        k_FarClip
    );
    m_MatMvp = m_MatProjection * m_MatView * m_MatModel;
    return m_MatMvp;
}


/* SETTERS */
void Camera::SetClipping(double near_clip, double far_clip) {
    k_NearClip = near_clip;
    k_FarClip = far_clip;
}
void Camera::SetFoV(double fov)     { k_FieldOfView = fov; }
void Camera::SetLookAt(glm::vec3 v) { m_CamVecLookAt = v; }
void Camera::SetPos(glm::vec3 v)    { m_CamVecPos = v; }
void Camera::SetUp(glm::vec3 v)     { m_CamVecUp = v; }
void Camera::SetViewport(int x, int y, int w, int h) {
    k_ViewportX = x;
    k_ViewportY = y;
    k_WinWidth = w;
    k_WinHeight = h;
    k_Aspect = double(w) / double(h);
}


/* Rotations */
// TODO: Use a class for these to avoid repetition?
// The 'rotation' functions do the same thing, only the axes change
void
Camera::RotatePitch(float delta)
{
    m_RotPitch = delta;
    Refresh();
    m_RotPitch = 0;
}

void
Camera::RotateRoll(float delta)
{
    m_RotRoll = delta;
    Refresh();
    m_RotRoll = 0;
}

void
Camera::RotateYaw(float delta)
{
    m_RotYaw = delta;
    Refresh();
    m_RotYaw = 0;
}

/* Movement */
// TODO: Use a class for this to avoid repetition?
// The 'movement' functions to the same thing, only the direction vector changes
void
Camera::Move(float incr)
{
    m_CamVecPosDelta = m_CamVecDir * incr;
    Refresh();
    m_CamVecPosDelta = glm::vec3(0, 0, 0);
}

void
Camera::Strafe(float incr)
{
    m_CamVecPosDelta = glm::cross(m_CamVecUp, m_CamVecDir) * incr;
    Refresh();
    m_CamVecPosDelta = glm::vec3(0, 0, 0);
}

void
Camera::Fly(float incr)
{
    m_CamVecPosDelta = m_CamVecUp * incr;
    Refresh();
    m_CamVecPosDelta = glm::vec3(0, 0, 0);
}
