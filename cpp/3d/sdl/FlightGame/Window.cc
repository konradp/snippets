#include "Window.h"

std::unique_ptr<SDL_Window, void (*)(SDL_Window*)> Window::mWin
    = std::unique_ptr<SDL_Window, void (*)(SDL_Window*)>(nullptr, SDL_DestroyWindow);

void
Window::Init()
{

    mWin.reset(SDL_CreateWindow(
        "test",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        640, 480,
        SDL_WINDOW_SHOWN
    ));

}

