// Standard
#include <iostream>
//#include <GL/glew.h> // No need to include GL
// added
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Local
#include "App.h"
#include "Camera.h"
#include "Window.h"
#include "common/shader.hpp"

int main()
{
    try {
        Window::Init();
    } catch(const std::exception &e) {
        std::cerr << e.what() << std::endl;
        // Window::Quit();
        return -1;
    }

    App app;
    // Update camera and render to screen
    // Implement FPS=frame per second timer

    // VAO: Vertex array object
    GLuint VertexArrayId;
    glGenVertexArrays(1, &VertexArrayId);
    glBindVertexArray(VertexArrayId);

    // Shaders
    GLuint shaderProgram = LoadShaders(
        "shaders/shader.vert",
        "shaders/shader.frag"
    );

    // VBO: Vertex buffer object
    static const GLfloat g_vertex_buffer_data[] = {
        -1.0f, -1.0f, 0.0f,
         1.0f, -1.0f, 0.0f,
         0.0f,  1.0f, 0.0f,
    };
    GLuint vertBuf;
    glGenBuffers(1, &vertBuf);
    glBindBuffer(GL_ARRAY_BUFFER, vertBuf);
    glBufferData(
        GL_ARRAY_BUFFER,
        sizeof(g_vertex_buffer_data),
        g_vertex_buffer_data,
        GL_STATIC_DRAW
    );

    // MAIN LOOP
    SDL_Event event;
    while(app.mQuit == false) {
        // Draw
        app.Render();
        glUseProgram(shaderProgram);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertBuf);
        glVertexAttribPointer(
            0,          // attribute 0. Must match layout in v.shader
                        // TODO: Why?
            3,          // size
            GL_FLOAT,   // TODO: type
            GL_FALSE,   // TODO: normalised?
            0,          // TODO: stride?
            (void*) 0   // array buffer offset?
        );
        glDisableVertexAttribArray(0);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 5);
        //SDL_GL_SwapWindow(window);
        ////////////////////
    glClearColor(1.0, 1.0, 1.0, 1.0); // set background colour
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

    // MVP
    Camera cam;
    cam.Refresh();
    glm::mat4 mvp = cam.GetMvpMatrix();
    
    


	// pass MVP as uniform into shader
	//int mvpIndex = glGetUniformLocation(shaderProgram, "MVP");
	//glUniformMatrix4fv(mvpIndex, 1, GL_FALSE, glm::value_ptr(MVP)); 

    glDrawArrays(GL_TRIANGLE_FAN, 0, 5); // draw the pyramid
	//r+=0.5;
    //SDL_GL_SwapWindkw(window); // swap buffers
    ///////
        // Should all the above live in App::Render?

        //glewSwapBuffers(Window::mWin.get());

        // Event loop: handle events (keyboard)
        while(SDL_PollEvent(&event)) {
            app.OnInput(&event);
        }
    } //MAIN LOOP

    // Clean up
    glDeleteBuffers(1, &vertBuf);
    glDeleteVertexArrays(1, &VertexArrayId);
    //glDeleteProgram

    //Window::Quit();

    return 0;
}



