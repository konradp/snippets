#ifndef APP_H
#define APP_H

#include <SDL2/SDL.h>

class App {
public:
    App();
    ~App();

    void OnInput(SDL_Event* ev);
    void Render();

    bool mQuit;
};

#endif //APP_H

