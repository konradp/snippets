#include <GL/glew.h> // No need to include GL
#include <iostream>
#include <stdexcept>

#include "Window.h"

// Deleters
std::unique_ptr      <SDL_Window, void (*)(SDL_Window*)> Window::mWin
    = std::unique_ptr<SDL_Window, void (*)(SDL_Window*)> (nullptr, SDL_DestroyWindow);
std::unique_ptr      <SDL_Renderer, void (*)(SDL_Renderer*)> Window::mRenderer
    = std::unique_ptr<SDL_Renderer, void (*)(SDL_Renderer*)> (nullptr, SDL_DestroyRenderer);

void
Window::Init()
{
    // Init SDL
    if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
        throw std::runtime_error("Failed to init SDL.");

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    // Create window
    mWin.reset(SDL_CreateWindow(
        "test",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        640, 480,
        SDL_WINDOW_SHOWN
    ));
    if(mWin == nullptr)
        throw std::runtime_error("Failed to create window.");

    // Create renderer
    mRenderer.reset(SDL_CreateRenderer(
        mWin.get(),
        -1,
        SDL_RENDERER_ACCELERATED
        | SDL_RENDERER_PRESENTVSYNC
    ));
    if(mRenderer == nullptr)
        throw std::runtime_error("Failed to create renderer");

    //glewExperimental = true; // for glGenVertexArrays()
    // Init GLEW
    GLenum res = glewInit();
    if(res != GLEW_OK) {
        std::string err("Failed to initialise GLEW: ");
        err.append(reinterpret_cast<const char*>(glewGetErrorString(res)));
        throw std::runtime_error(err);
    }

    //SDL_GLContext _gl_context;
    //_gl_context = SDL_GL_CreateContext(mWin);

}

void
Window::Clear()
{
    SDL_RenderClear(mRenderer.get());
}

void
Window::Update()
{
    SDL_RenderPresent(mRenderer.get());
    SDL_GL_SwapWindow(mWin.get());
}


