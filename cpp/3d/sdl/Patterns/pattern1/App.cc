#include <iostream>

#include "App.h"
#include "Window.h"

App::App()
: mQuit(false)
{
}

App::~App()
{
}

void
App::OnInput(SDL_Event* e)
{
    if(e->type == SDL_QUIT) mQuit = true;
    if(e->type == SDL_KEYDOWN) {
        switch(e->key.keysym.sym) {
            case SDLK_ESCAPE:
                mQuit = true;
                break;

            case SDLK_UP:
                std::cout << "Arrow up" << std::endl;
                break;
            case SDLK_DOWN:
                std::cout << "Arrow down" << std::endl;
                break;
            case SDLK_LEFT:
                std::cout << "Arrow left" << std::endl;
                break;
            case SDLK_RIGHT:
                std::cout << "Arrow right" << std::endl;
                break;
            default:
                break;
        }
    }
}

void
App::Render()
{
    Window::Clear();

    Window::Update();
}

