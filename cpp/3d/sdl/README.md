# SDL
Add here examples which use Simple DirectMedia Layer (SDL).

best: https://github.com/TannerRogalsky/sdl_asteroids/blob/master/src/main.cpp

https://gist.github.com/gkbrk/3c852eb27a8aa7108d74
https://github.com/BennyQBD/3DGameEngineCpp_60/blob/master/3DEngineCpp/window.cpp


https://github.com/Laremere/unity-vr/blob/master/SDL2window/vs2010_project/sdl2window/Window.h
https://github.com/redblobgames/helloworld-sdl2-opengl-emscripten/blob/master/main.cpp
https://github.com/Telion/Game/blob/master/Client/src/Renderer.cpp

