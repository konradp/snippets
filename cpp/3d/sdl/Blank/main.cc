#include <iostream>

#include "App.h"
#include "Camera.h"
#include "Window.h"

int main()
{
    try {
        Window::Init();
    } catch(const std::runtime_error &e) {
        std::cout << e.what() << std::endl;
        // Window::Quit();
        return -1;
    }

    App app;
    // Start event loop
    // and pass events to app's input handler
    // Update camera and render to screen
    // Implement FPS=frame per second timer

    return 0;
}

