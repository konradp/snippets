#ifndef WINDOW_H
#define WINDOW_H

#include <memory> //unique_ptr
#include <SDL2/SDL.h>

// This class consists only of static functions,
// so the constructor/destructor do not do anything
class Window {
public:
    Window(){};
    ~Window(){};

    static void Init();

private:
    static std::unique_ptr<SDL_Window, void (*)(SDL_Window*)> mWin;
    static std::unique_ptr<SDL_Renderer, void (*)(SDL_Renderer)> mRender;
};

#endif //WINDOW_H

