# First person 'aircraft' camera

This OpenGL glut (GL Utility) camera is a camera which can be used for aircraft-like first-person view.  
That is, it allows for these rotations:

```
Name    Direction       Axis of rotation                    Keyboard
-----------------------------------------------------------------------
yaw     left/right      camera up                           'q' and 'e'
pitch   up/down         horizontal                          'w' and 's'
roll    tilt left/right camera direction (lookAt vector)    'a' and 'd'
-----------------------------------------------------------------------
move    forward/back    -                                   'i' and 'k'
strafe  left/right      -                                   'j' and 'l'
flyy    up/down         -                                   'v' and 'c'
```

