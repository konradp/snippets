#include "Speed.h"
#include <cstdlib>
#include <iostream>

Speed::Speed()
: kSpeed(0),
  kSpeedLevel(0)
{
    std::cout << "Speed constructor" << std::endl;
    mSpeeds = { 0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06 };
}

// Get
double Speed::Value() {
    double speed = mSpeeds[abs(kSpeedLevel)];
    return (kSpeedLevel >= 0)? speed : -speed;
}

int Speed::Level()
{ return kSpeedLevel; }

// Set
void Speed::Up()
{
    if(kSpeedLevel < (int) mSpeeds.size() - 1)
        kSpeedLevel++;
}
void Speed::Down()
{
    if(kSpeedLevel > -((int) mSpeeds.size() - 1) )
        kSpeedLevel--;
}

