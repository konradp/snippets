#include <GL/glut.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr
#include <iostream>
#include <map>
#include <math.h>
#include <vector>
#include "Camera.h"
#include "Speed.h"

// Forward-declare main callbacks
void Display();
void Resize(int win_w, int win_h);
void OnKey(unsigned char key, int x, int y);
void OnKeyUp(unsigned char key, int x, int y);
void Timer(int delay);
// Other
void DrawGrid();
void DrawTeapot();

// Global variables
Camera g_cam;
int    g_viewport_width = 0;
int    g_viewport_height = 0;
int    g_interval = 1000 / 60; // fps: 60 frames per second

// Movement Settings
const float g_mv_velocity = 0.02;
Speed g_speed;

// Movement states
enum MoveStates { MOVE, RYAW, ROLL, PTCH, STRF, FLYY };
// movestate: -1, 0, 1
std::map<MoveStates, int> g_movestate {
    { MOVE, 0 },
    { RYAW, 0 },
    { ROLL, 0 },
    { PTCH, 0 },
    { STRF, 0 },
    { FLYY, 0 }
};
bool g_key[256];

//// MAIN
int main(int argc, char *argv[])
{
    // GLUT init
    glutInit(&argc, argv);
    glutInitWindowSize(300, 300);
    glutInitWindowPosition(0, 0);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH);
    glutCreateWindow("Scene example");

    // Callback functions
    glutDisplayFunc(Display);
    glutReshapeFunc(Resize);
    glutKeyboardFunc(OnKey);
    glutKeyboardUpFunc(OnKeyUp);
    glutTimerFunc(g_interval, Timer, 0);

    // Settings
    glutIgnoreKeyRepeat(1);
    glutSetCursor(GLUT_CURSOR_NONE);

    // GL
    glClearColor(0, 0, 0, 0); // black bg
    glEnable(GL_DEPTH_TEST);

    // Camera settings
    // Call here methods such as SetPos, SetLookAt, SetClipping, SetFov etc
    // Otherwise, defaults are used
    g_cam.SetPos(glm::vec3(1, 1, -2));

    glutMainLoop();
    return 1; // Only reached on error
}

/* Display functions */
void Display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Refresh camera
    g_cam.Refresh();
    glm::mat4 mvp = g_cam.GetMvpMatrix();
    glLoadMatrixf(glm::value_ptr(mvp));

    DrawGrid();
    DrawTeapot();

    glutSwapBuffers();
}

/* Keep x/y ratio on window resize */
void Resize(int win_w, int win_h)
{
    if(win_w == 0 || win_h == 0) return;
    g_cam.SetViewport(0, 0, win_w, win_h);
}

/* Keyboard/mouse functions */
// w/s: UP/DOWN
// a/d: ROLL left/right
// [ and ]: YAW left/right (pedal left/right)
// Exit on 'q' or 'Escape'
void OnKey(unsigned char key, int x, int y) {
    if(key == 'x' || key == 27) exit(0);
    g_key[key] = true;

    switch(key) {
        case 's': g_movestate[PTCH] =  1; break;
        case 'w': g_movestate[PTCH] = -1; break;
        case 'a': g_movestate[ROLL] =  1; break;
        case 'd': g_movestate[ROLL] = -1; break;
        case 'q': g_movestate[RYAW] =  1; break;
        case 'e': g_movestate[RYAW] = -1; break;

        case 'i': g_movestate[MOVE] =  1; break;
        case 'k': g_movestate[MOVE] = -1; break;
        case 'j': g_movestate[STRF] =  1; break;
        case 'l': g_movestate[STRF] = -1; break;
        case 'v': g_movestate[FLYY] =  1; break;
        case 'c': g_movestate[FLYY] = -1; break;
    }
    //g_cam.DebugVec();
}

void OnKeyUp(unsigned char key, int x, int y)
{
    // If a key is released, stop the movement, but
    // check in case another 'movement' key is albo already pressed
    g_key[key] = false;
    // TODO: This is not ideal, and is a repetition of the OnKey() function
    // TODO: Use a class for this
    switch(key) {
        // Rotations
        case 's':
            if(g_key['w']) g_movestate[PTCH] = 1;
            else g_movestate[PTCH] = 0;
            break;
        case 'w':
            if(g_key['s']) g_movestate[PTCH] = -1;
            else g_movestate[PTCH] = 0;
            break;
        case 'a':
            if(g_key['d']) g_movestate[ROLL] = -1;
            else g_movestate[ROLL] = 0;
            break;
        case 'd':
            if(g_key['a']) g_movestate[ROLL] = 1;
            else g_movestate[ROLL] = 0;
            break;
        case 'q':
            if(g_key['e']) g_movestate[RYAW] = -1;
            else g_movestate[RYAW] = 0;
            break;
        case 'e':
            if(g_key['q']) g_movestate[RYAW] = 1;
            else g_movestate[RYAW] = 0;
            break;
        // Movement
        case 'i':
            if(g_key['k']) g_movestate[MOVE] = 1;
            else g_movestate[MOVE] = 0;
            break;
        case 'k':
            if(g_key['i']) g_movestate[MOVE] = -1;
            else g_movestate[MOVE] = 0;
            break;
        case 'j':
            if(g_key['l']) g_movestate[STRF] = -1;
            else g_movestate[STRF] = 0;
            break;
        case 'l':
            if(g_key['j']) g_movestate[STRF] = 1;
            else g_movestate[STRF] = 0;
            break;
        case 'v':
            if(g_key['c']) g_movestate[FLYY] = -1;
            else g_movestate[FLYY] = 0;
            break;
        case 'c':
            if(g_key['v']) g_movestate[FLYY] = 1;
            else g_movestate[FLYY] = 0;
            break;
        // Speed
        case '=':
            g_speed.Up();
            std::cout << "Speed: " << g_speed.Level() << ": " << g_speed.Value() << std::endl;
            break;
        case '-':
            g_speed.Down();
            std::cout << "Speed: " << g_speed.Level() << ": " << g_speed.Value() << std::endl;
            break;
    }
}

void DrawGrid()
{
    glPushMatrix();
    glColor3f(1, 1, 1); // white

    for(int i = -50; i < 50; i++) {
        glBegin(GL_LINES);
            glVertex3f(i, 0, -50);
            glVertex3f(i, 0,  50);
        glEnd();
        glBegin(GL_LINES);
            glVertex3f(-50, 0, i);
            glVertex3f( 50, 0, i);
        glEnd();
    }
    
    glPopMatrix();
}

void DrawTeapot()
{
    glPushMatrix();
        glColor3f(0, 0.8, 0);
        glTranslatef(0, 0.8, 0);
        glutWireTeapot(1);
    glPopMatrix();
}

void Timer(int delay)
{
    // Rotations
    if(g_movestate[RYAW] > 0) g_cam.RotateYaw( g_mv_velocity);
    if(g_movestate[RYAW] < 0) g_cam.RotateYaw(-g_mv_velocity);

    if(g_movestate[PTCH] > 0) g_cam.RotatePitch(g_mv_velocity);
    if(g_movestate[PTCH] < 0) g_cam.RotatePitch(-g_mv_velocity);

    if(g_movestate[ROLL] > 0) g_cam.RotateRoll(-g_mv_velocity);
    if(g_movestate[ROLL] < 0) g_cam.RotateRoll( g_mv_velocity);

    // Movement
    if(g_movestate[MOVE] > 0) g_cam.Move( g_mv_velocity);
    if(g_movestate[MOVE] < 0) g_cam.Move(-g_mv_velocity);

    if(g_movestate[STRF] > 0) g_cam.Strafe( g_mv_velocity);
    if(g_movestate[STRF] < 0) g_cam.Strafe(-g_mv_velocity);

    if(g_movestate[FLYY] > 0) g_cam.Fly( g_mv_velocity);
    if(g_movestate[FLYY] < 0) g_cam.Fly(-g_mv_velocity);

    if(g_speed.Value() != 0) g_cam.Move(g_speed.Value());

    glutTimerFunc(g_interval, Timer, delay);
    glutPostRedisplay(); // TODO: Unclear what this does
}

