#ifndef SPEED_H
#define SPEED_H
#include <vector>

using std::vector;

class Speed {
public:
    Speed();
    ~Speed(){};

    // Get
    int Level();
    double Value();

    // Set
    void Up();
    void Down();
private:
    int kSpeedLevel;
    double kSpeed;
    vector<double> mSpeeds;

};

#endif // SPEED_H

