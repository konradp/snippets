#include <GL/glut.h>
#include <iostream>

void Display();
void Resize(int win_w, int win_h);
void OnKey(unsigned char key, int x, int y);

// Variables
int g_viewport_width  = 0;
int g_viewport_height = 0;

int main(int argc, char *argv[])
{
    std::cout << "Test" << std::endl;

    // GLUT init
    glutInit(&argc, argv);
    glutInitWindowSize(300, 300);
    glutCreateWindow("Scene example");

    // Callback functions
    glutDisplayFunc(Display);
    glutIdleFunc(Display);
    glutReshapeFunc(Resize);
    glutKeyboardFunc(OnKey);

    // Settings
    glutIgnoreKeyRepeat(1);
    glutSetCursor(GLUT_CURSOR_NONE);

    // GL
    glClearColor(0, 0, 0, 0); // black bg

    glutMainLoop();
    return 1; // Only reached on error
}

/* Display functions */
void Display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    // Camera
    // Draw other things

    glutSwapBuffers();
}

/* Keep x/y ratio on window resize */
void Resize(int win_w, int win_h)
{
    if(win_w == 0 || win_h == 0) return;
    g_viewport_width = win_w;
    g_viewport_height = win_h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // gluPerspective here?
    // TODO: Read about this
    gluPerspective(60, (GLfloat) win_w / (GLfloat) win_h, 0.1, 100);
    
    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, win_w, win_h);
}

/* Keyboard/mouse functions */
// Exit on 'q' or 'Escape'
void OnKey(unsigned char key, int x, int y) { if(key == 'q' || key == 27) exit(0); }
