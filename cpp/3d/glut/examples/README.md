# 3d examples

## Tutorials
http://www.lighthouse3d.com/tutorials/glut-tutorial/  
http://www.codecolony.de/opengl.htm#introduction  
https://www.ntu.edu.sg/home/ehchua/programming/opengl/CG_Introduction.html  
http://nghiaho.com/?p=1613  

These require:
- mesa
- freeglut
- glu

# Examples
Square:  
- 1: white square 
- 2: colour square 
- 3: + keep proportions 
- 4: + rotate
- 5: + keys change colours

Camera:
- 1: Snowmen, moving camera
- 2: Animated object: spinning toruses
- 3: + moving first person camera
- 4: + no key repeat
- 5: Removed toruses, added strafing and mouse left/right
- 5: + Mouse up/down

