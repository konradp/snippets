/* Draw a white 2d triangle
 * See www2.cs.arizona.edu/classes/cs433/spring02/opengl/triangle.html
 */

#include <iostream>
#include <GL/glut.h>

float angle = 0.0f;
float red = 0.0;
float green = 1.0;
float blue = 0.0;

void Display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    // Set camera
    gluLookAt(
        0.0f, 0.0f, 10.0f,
        0.0f, 0.0f,  0.0f,
        0.0f, 1.0f,  0.0f);

    // Rotate object
    glRotatef(angle, 0.0f, 1.0f, 0.0f);
    angle+=0.1f;

    glColor3f(red, green, blue);
    glBegin(GL_QUADS); // Square: RGB
        glVertex3f(-0.5, -0.5, -3.0);
        glVertex3f( 0.5, -0.5, -3.0);
        glVertex3f( 0.5,  0.5, -3.0);
        glVertex3f(-0.5,  0.5, -3.0);
    glEnd();
    glutSwapBuffers();
}

void Resize(int x, int y) {
    // Crucial for keeping the square 'square' after window is resized
    if(x == 0 || y == 0) return;
    glMatrixMode(GL_PROJECTION);    // switch to editing the projection matrix
    glLoadIdentity();

    // FieldOfView angle, aspect ratio, far planes
    gluPerspective(45, (GLfloat)x/(GLfloat)y, 1, 100);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, x, y);         // viewport spans entire window
}

void OnKey(unsigned char key, int x, int y)
{
    if(key == 'q' || key == 27) exit(0);
}

void OnKeySpecial(int key, int x, int y)
{
switch(key) {
    case GLUT_KEY_F1:
        red = 1.0;
        green = 0.0;
        blue = 0.0;
        break;
    case GLUT_KEY_F2:
        red = 0.0;
        green = 1.0;
        blue = 0.0;
        break;
    case GLUT_KEY_F3:
        red = 0.0;
        green = 0.0;
        blue = 1.0;
        break;


}

}

int main(int argc, char *argv[])
{
    // Init window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA);
    glutInitWindowSize(300,300); // pixel size
    glutCreateWindow("Square example");
    glClearColor(0.0, 0.0, 0.0, 0.0); // black bg
    // Display
    glutDisplayFunc(Display);
    glutIdleFunc(Display);
    glutReshapeFunc(Resize);
    // Keyboard
    glutKeyboardFunc(OnKey);
    glutSpecialFunc(OnKeySpecial);

    glutMainLoop();
    return 0; // We never get here
}

