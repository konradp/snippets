/* Draw a snowman scene, first person camera
 * See
 * http://www.lighthouse3d.com/tutorials/glut-tutorial/keyboard-example-moving-around-the-world/
 */

#include <iostream>
#include <GL/glut.h>
#include <math.h>

float angle = 0.0f;
float x = 0.0f;
float y = 0.0f;
float z = 5.0f;
float lx = 0.0f;
float lz = -1.0f;

void DrawObject()
{
    glColor3f(1.0f, 1.0f, 1.0f);

    // Body
    glTranslatef(0.0f, 0.75f, 0.0f);
    glutSolidSphere(0.75f, 20, 20);
    // Head
    glTranslatef(0.0f, 1.0f, 0.0f);
    glutSolidSphere(0.25f, 20, 20);
    // Eyes
    glPushMatrix();
    glColor3f(0.0f, 0.0f, 0.0f);
    glTranslatef(0.05f, 0.10f, 0.18f);
    glutSolidSphere(0.05f, 10, 10);
    glTranslatef(-0.1f, 0.0f, 0.0f);
    glutSolidSphere(0.05f, 10, 10);
    glPopMatrix();
    // Nose
    glColor3f(1.0f, 0.5f, 0.5f);
    glutSolidCone(0.08f, 0.5f, 10, 2);
}

void Display()
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    // Set camera
    gluLookAt(
        x,    1.0f, z,
        x+lx, 1.0f, z+lz,
        0.0f, 1.0f, 0.0f);

    // Draw ground
    glColor3f(0.9f, 0.9f, 0.9f);
    glBegin(GL_QUADS); // Square: RGB
        glVertex3f(-100.0f, 0.0f, -100.0f);
        glVertex3f(-100.0f, 0.0f,  100.0f);
        glVertex3f( 100.0f, 0.0f,  100.0f);
        glVertex3f( 100.0f, 0.0f, -100.0f);
    glEnd();

    // Draw 36 objects
    for(int i = -3; i < 3; i++) {
        for(int j = -3; j < 3; j++) {
            glPushMatrix();
            glTranslatef(i*10.0, 0, j*10.0);
            DrawObject();
            glPopMatrix();
        } // for
    } // for

    glutSwapBuffers();
}

void Resize(int x, int y) {
    // Crucial for keeping the square 'square' after window is resized
    if(x == 0 || y == 0) return;
    glMatrixMode(GL_PROJECTION);    // switch to editing the projection matrix
    glLoadIdentity();

    // FieldOfView angle, aspect ratio, far planes
    gluPerspective(45, (GLfloat)x/(GLfloat)y, 1, 100);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, x, y);         // viewport spans entire window
}

void OnKey(unsigned char key, int x, int y)
{
    if(key == 'q' || key == 27) exit(0);
}

void OnKeySpecial(int key, int mouse_x, int mouse_y)
{
    float fraction = 0.1f;

    switch(key) {
        case GLUT_KEY_LEFT:
            angle -= 0.01f;
            lx = sin(angle);
            lz = -cos(angle);
            break;
        case GLUT_KEY_RIGHT:
            angle += 0.01f;
            lx = sin(angle);
            lz = -cos(angle);
            break;
        case GLUT_KEY_UP:
            x += lx * fraction;
            z += lz * fraction;
            break;
        case GLUT_KEY_DOWN:
            x -= lx * fraction;
            z -= lz * fraction;
            break;
    }

}

int main(int argc, char *argv[])
{
    // Init window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(300,300); // pixel size
    glutCreateWindow("Scene example");
    glClearColor(0.0, 0.0, 0.0, 0.0); // black bg
    // Callbacks: display and keyboard
    glutDisplayFunc(Display);
    glutIdleFunc(Display);
    glutReshapeFunc(Resize);
    glutKeyboardFunc(OnKey);
    glutSpecialFunc(OnKeySpecial);

    // OpenGL init: ?
    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 1; // We never get here, so return 1
}

