/* Draw a snowman scene, first person camera
 * See
 * http://www.lighthouse3d.com/tutorials/glut-tutorial/keyboard-example-moving-around-the-world/
 */

#include <iostream>
#include <GL/glut.h>
#include <math.h>

float x = 0.0f;
float y = 0.0f;
float z = 5.0f;
float lx = 0.0f;
float lz = -1.0f;
float xRot = 0.0f;
float yRot = 0.0f;
float zRot = 0.0f;

void DrawObject()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    // Base
    glTranslatef(0.0f, 0.0f, 0.0f);
    glutSolidSphere(0.25f, 20, 20);

    // Rings
    glTranslatef(0.0f, 1.10f, 0.0f);

    glRotatef(xRot, 4.0, 1.0, 0.0);
    glutSolidTorus(0.05f, 0.85f, 20, 20);

    glRotatef(yRot, 0.0, 4.0, 0.0);
    glutSolidTorus(0.05f, 0.65f, 20, 20);

    glRotatef(zRot, 2.0, 0.0, 4.0);
    glutSolidTorus(0.05f, 0.45f, 20, 20);

    glRotatef(yRot, 4.0, 1.0, 2.0);
    glutSolidTorus(0.05f, 0.25f, 20, 20);

    // Center
    glutSolidSphere(0.10f, 20, 20);
}

void Display()
{
    xRot += 0.2;
    yRot += 0.5;
    zRot += -0.6;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    // Set camera
    gluLookAt(
        x,    1.0f, z,
        x+lx, 1.0f, z+lz,
        0.0f, 1.0f, 0.0f);

    // Draw ground
    glColor3f(0.5f, 0.5f, 0.5f);
    glBegin(GL_QUADS); // Square: RGB
        glVertex3f(-100.0f, 0.0f, -100.0f);
        glVertex3f(-100.0f, 0.0f,  100.0f);
        glVertex3f( 100.0f, 0.0f,  100.0f);
        glVertex3f( 100.0f, 0.0f, -100.0f);
    glEnd();

    // Draw object
    glPushMatrix();
    glTranslatef(0, 0, 0);
    DrawObject();
    glPopMatrix();

    glutSwapBuffers();
}

void Resize(int x, int y) {
    // Crucial for keeping the square 'square' after window is resized
    if(x == 0 || y == 0) return;
    glMatrixMode(GL_PROJECTION);    // switch to editing the projection matrix
    glLoadIdentity();

    // FieldOfView angle, aspect ratio, far planes
    gluPerspective(45, (GLfloat)x/(GLfloat)y, 1, 100);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, x, y);         // viewport spans entire window
}

void OnKey(unsigned char key, int x, int y)
{ if(key == 'q' || key == 27) exit(0); }

int main(int argc, char *argv[])
{
    // Init window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(300,300); // pixel size
    glutCreateWindow("Spinning toruses");
    glClearColor(0.0, 0.0, 0.0, 0.0); // black bg
    // Callbacks: display and keyboard
    glutDisplayFunc(Display);
    glutIdleFunc(Display);
    glutReshapeFunc(Resize);
    glutKeyboardFunc(OnKey);

    // OpenGL init
    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 1; // We never get here, so return 1
}

