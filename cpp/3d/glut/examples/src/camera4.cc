/* Draw a snowman scene, first person camera
 * See
 * http://www.lighthouse3d.com/tutorials/glut-tutorial/keyboard-example-moving-around-the-world/
 */

#include <iostream>
#include <GL/glut.h>
#include <math.h>

float angle = 0.0f;
float x = 0.0f;
float y = 0.0f;
float z = 5.0f;
float lx = 0.0f;
float lz = -1.0f;
// Rotation
float xRot = 0.0f;
float yRot = 0.0f;
float zRot = 0.0f;
// Key states
float dAngle = 0.0f;
float dMove = 0.0f;

void DrawObject()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    // Base
    glTranslatef(0.0f, 0.0f, 0.0f);
    glutSolidSphere(0.25f, 20, 20);

    // Rings
    glTranslatef(0.0f, 1.10f, 0.0f);

    glRotatef(xRot, 4.0, 1.0, 0.0);
    glutSolidTorus(0.05f, 0.85f, 20, 20);

    glRotatef(yRot, 0.0, 4.0, 0.0);
    glutSolidTorus(0.05f, 0.65f, 20, 20);

    glRotatef(zRot, 2.0, 0.0, 4.0);
    glutSolidTorus(0.05f, 0.45f, 20, 20);

    glRotatef(yRot, 4.0, 1.0, 2.0);
    glutSolidTorus(0.05f, 0.25f, 20, 20);

    // Center
    glutSolidSphere(0.10f, 20, 20);
}

void computePos(float dMove)
{
    x += dMove * lx * 0.1f;
    z += dMove * lz * 0.1f;
}

void computeDir(float dAngle)
{
    angle += dAngle;
    lx = sin(angle);
    lz = -cos(angle);
}

void Display()
{
    // Rotate all objects
    xRot += 0.2;
    yRot += 0.5;
    zRot += -0.6;

    // Compute position
    if(dMove) computePos(dMove);
    if(dAngle) computeDir(dAngle);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    // Set camera
    gluLookAt(
        x,    1.0f, z,
        x+lx, 1.0f, z+lz,
        0.0f, 1.0f, 0.0f);

    // Draw ground
    glColor3f(0.4f, 0.4f, 0.4f);
    glBegin(GL_QUADS); // Square: RGB
        glVertex3f(-100.0f, 0.0f, -100.0f);
        glVertex3f(-100.0f, 0.0f,  100.0f);
        glVertex3f( 100.0f, 0.0f,  100.0f);
        glVertex3f( 100.0f, 0.0f, -100.0f);
    glEnd();

    // Draw 36 objects
    for(int i = -3; i < 3; i++) {
        for(int j = -3; j < 3; j++) {
            glPushMatrix();
            glTranslatef(i*10.0, 0, j*10.0);
            DrawObject();
            glPopMatrix();
        } // for
    } // for

    glutSwapBuffers();
}

void Resize(int x, int y) {
    // Crucial for keeping the square 'square' after window is resized
    if(x == 0 || y == 0) return;
    glMatrixMode(GL_PROJECTION);    // switch to editing the projection matrix
    glLoadIdentity();

    // FieldOfView angle, aspect ratio, far planes
    gluPerspective(45, (GLfloat)x/(GLfloat)y, 1, 100);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, x, y);         // viewport spans entire window
}

void OnKey(unsigned char key, int x, int y)
{
    if(key == 'q' || key == 27) exit(0);
}

void OnKeySpecial(int key, int mouse_x, int mouse_y)
{
    switch(key) {
        case GLUT_KEY_LEFT: dAngle -= 0.03f; break;
        case GLUT_KEY_RIGHT: dAngle += 0.03f; break;
        case GLUT_KEY_UP: dMove = 2.5f; break;
        case GLUT_KEY_DOWN: dMove = -2.5f; break;
    }
}

void OnKeyRelease(int key, int mouse_x, int mouse_y)
{
    switch(key) {
        case GLUT_KEY_LEFT:
        case GLUT_KEY_RIGHT: dAngle = 0.0f; break;
        case GLUT_KEY_UP:
        case GLUT_KEY_DOWN: dMove = 0; break;
    }
}

int main(int argc, char *argv[])
{
    // Init window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(300,300); // pixel size
    glutCreateWindow("Scene example");
    glClearColor(0.0, 0.0, 0.0, 0.0); // black bg
    // Callbacks: display and keyboard
    glutDisplayFunc(Display);
    glutIdleFunc(Display);
    glutReshapeFunc(Resize);
    // Keyboard
    glutKeyboardFunc(OnKey);
    glutSpecialFunc(OnKeySpecial);
    glutIgnoreKeyRepeat(1);
    glutSpecialUpFunc(OnKeyRelease);

    // OpenGL init: ?
    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 1; // We never get here, so return 1
}

