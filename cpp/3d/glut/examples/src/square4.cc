/* Draw a white 2d triangle
 * See www2.cs.arizona.edu/classes/cs433/spring02/opengl/triangle.html
 */

#include <iostream>
#include <GL/glut.h>

float angle = 0.0f;

void Display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    // Set camera
    gluLookAt(
        0.0f, 0.0f, 10.0f,
        0.0f, 0.0f,  0.0f,
        0.0f, 1.0f,  0.0f);

    // Rotate object
    glRotatef(angle, 0.0f, 1.0f, 0.0f);
    angle+=0.1f;

    glBegin(GL_QUADS); // Square: RGB
        glColor3f(1.0, 0.0, 0.0); glVertex3f(-0.5, -0.5, -3.0);
        glColor3f(0.0, 1.0, 0.0); glVertex3f( 0.5, -0.5, -3.0);
        glColor3f(0.0, 0.0, 1.0); glVertex3f( 0.5,  0.5, -3.0);
                                  glVertex3f(-0.5,  0.5, -3.0);
    glEnd();
    glFlush();  // complete any pending operations
}

void Resize(int x, int y) {
    // Crucial for keeping the square 'square' after window is resized
    if(x == 0 || y == 0) return;
    glMatrixMode(GL_PROJECTION);    // switch to editing the projection matrix
    glLoadIdentity();

    // FieldOfView angle, aspect ratio, far planes
    gluPerspective(45, (GLfloat)x/(GLfloat)y, 1, 100);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, x, y);         // viewport spans entire window
}

void OnKey(unsigned char key, int x, int y)
{
    if(key == 'q') exit(0);
}

int main(int argc, char *argv[])
{
    // Init window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA);
    glutInitWindowSize(300,300); // pixel size
    glutCreateWindow("Square example");
    glClearColor(0.0, 0.0, 0.0, 0.0); // black bg
    glutDisplayFunc(Display);
    glutIdleFunc(Display);
    glutReshapeFunc(Resize);
    glutKeyboardFunc(OnKey);

    glutMainLoop();
    return 0; // We never get here
}

