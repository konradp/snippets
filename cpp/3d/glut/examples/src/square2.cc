/* Draw a white 2d triangle
 * See www2.cs.arizona.edu/classes/cs433/spring02/opengl/triangle.html
 */

#include <iostream>
#include <GL/glut.h>

void Display()
{
    // Background
    glClearColor(0.0, 0.0, 0.0, 0.0); // black bg
    glClear(GL_COLOR_BUFFER_BIT);

    // Foreground
    glColor3f(1.0, 1.0, 1.0); // white

    // Square
    glBegin(GL_QUADS);
        glColor3f(1.0, 0.0, 0.0); // red
        glVertex2i(2, 2);

        glColor3f(0.0, 1.0, 0.0); // green
        glVertex2i(8, 2);

        glColor3f(0.0, 0.0, 1.0); // blue
        glVertex2i(8, 8);

        glVertex2i(2, 8);
    glEnd();

    glFlush();  // complete any pending operations
}

void OnKey(unsigned char key, int x, int y)
{
    if(key == 'q') exit(0);
}

int main(int argc, char *argv[])
{
    // Init window
    int win;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA);
    glutInitWindowSize(400,500); // pixel size
    win = glutCreateWindow("Square example");
    glutDisplayFunc(Display);
    glutKeyboardFunc(OnKey);

    // Important: set viewing region
    // Note: It's wider than 400 here
    gluOrtho2D(0, 10, 0, 10);

    glutMainLoop();
    return 0; // We never get here
}

