/* Draw a white 2d triangle
 * See www2.cs.arizona.edu/classes/cs433/spring02/opengl/triangle.html
 */

#include <iostream>
#include <GL/glut.h>

void OnDisplay()
{
    // Background
    glClearColor(0.0, 0.0, 0.0, 0.0); // black bg
    glClear(GL_COLOR_BUFFER_BIT);

    // Foreground
    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_POLYGON);
        glVertex2i(200, 125);
        glVertex2i(100, 375);
        glVertex2i(300, 375);
    glEnd();

    glFlush();  // complete any pending operations
}

void OnKey(unsigned char key, int x, int y)
{
    if(key == 'q') exit(0);
}

int main(int argc, char *argv[])
{
    // Init window
    int win;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA);
    glutInitWindowSize(400,500); // pixel size
    win = glutCreateWindow("Triangle example");
    glutDisplayFunc(OnDisplay);
    glutKeyboardFunc(OnKey);

    // Important: set viewing region
    // Note: It's wider than 400 here
    gluOrtho2D(0, 800, 0, 500);

    glutMainLoop();
    return 0; // We never get here
}

