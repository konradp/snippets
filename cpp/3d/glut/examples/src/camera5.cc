/* A first person camera example
 * See
 * http://www.lighthouse3d.com/tutorials/glut-tutorial/keyboard-example-moving-around-the-world/
 */

#include <iostream>
#include <GL/glut.h>
#include <math.h>

int g_viewport_width = 0;
int g_viewport_height = 0;

// Camera position
float xCam = 0;
float yCam = 1;
float zCam = 7;
float angle = 0.0f;

// Key states
float dAngle  = 10;
float hAngle  = M_PI/2;
float dX      = 0;
float dZ      = 0;

// Move (forwards/backwards) or strafe (left/right)
void computePos(float move, float strafe)
{
    xCam += move * cos(angle+dAngle);
    zCam += move * sin(angle+dAngle);

    xCam += strafe * sin(angle+dAngle);
    zCam += strafe * (-cos(angle+dAngle));
}

void MouseMove(int x, int y)
{
    // Avoid a loop due to glutWarpPointer
    // TODO: Understand this further
    static bool just_warped = false;
    if(just_warped) {
        just_warped = false;
        return;
    }

    int dx = x - g_viewport_width/2;
    int dy = y - g_viewport_height/2;

    dAngle += dx*0.003f;
    hAngle += dy*0.003f;

    // Avoid camera reverting when looking up/down
    // TODO: These should be in radians
    if(hAngle < 1) hAngle = 1;
    if(hAngle > 2) hAngle = 2;

    glutWarpPointer(g_viewport_width/2, g_viewport_height/2);
    just_warped = true;
}

void Display()
{
    // Compute position
    if(dX!= 0 || dZ != 0) computePos(dX, dZ);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    // Set camera
    gluLookAt(
        xCam,                   yCam,                zCam,
        xCam+cos(angle+dAngle), 1.0f + cos(hAngle),  zCam+sin(angle+dAngle),
        0.0f,                   1.0f,                0.0f);

    // Draw ground grid
    glColor3f(0.4f, 0.4f, 0.4f);
    for(int i = -20; i < 20; i++) {
        glBegin(GL_LINES);
            glVertex3f(i, 0, -20);
            glVertex3f(i, 0,  20);
        glEnd();
        glBegin(GL_LINES);
            glVertex3f(-20, 0, i);
            glVertex3f( 20, 0, i);
        glEnd();
    }

    // Draw teapots
    glColor3f(0.8f, 0.8f, 0.8f);
    for(int i = -2; i < 2; i++) {
        for(int j = -2; j < 2; j++) {
            glPushMatrix();
            glTranslatef(i*10.0, 0.8f, j*10.0);
            glutWireTeapot(1);
            glPopMatrix();
        } // for
    } // for

    glutSwapBuffers();
}

void Resize(int w, int h) {
    // Crucial for keeping the square 'square' after window is resized
    if(w == 0 || h == 0) return;
    g_viewport_width = w;
    g_viewport_height = h;
    glMatrixMode(GL_PROJECTION);    // switch to editing the projection matrix
    glLoadIdentity();

    // FieldOfView angle, aspect ratio, far planes
    gluPerspective(45, (GLfloat) w / (GLfloat) h, 1, 100);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, w, h);         // viewport spans entire window
}

void OnKey(unsigned char key, int x, int y)
{
    if(key == 'q' || key == 27) exit(0);
    if(key == 'a') {
        angle -= 0.1f;
    }
    if(key == 'd') {
        angle += 0.1f;
    }
}

void OnKeySpecial(int key, int mouse_x, int mouse_y)
{
    switch(key) {
        case GLUT_KEY_UP:    dX  =  0.3f; break;
        case GLUT_KEY_DOWN:  dX  = -0.3f; break;
        case GLUT_KEY_LEFT:  dZ  =  0.3f; break;
        case GLUT_KEY_RIGHT: dZ  = -0.3f; break;
    }
}

void OnKeyRelease(int key, int mouse_x, int mouse_y)
{
    switch(key) {
        case GLUT_KEY_UP:    if(dX > 0) dX = 0; break;
        case GLUT_KEY_DOWN:  if(dX < 0) dX = 0; break;
        case GLUT_KEY_LEFT:  if(dZ > 0) dZ = 0; break;
        case GLUT_KEY_RIGHT: if(dZ < 0) dZ  = 0; break;
    }
}

int main(int argc, char *argv[])
{
    // Init window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(300,300); // pixel size
    glutCreateWindow("Scene example");
    glClearColor(0.0, 0.0, 0.0, 0.0); // black bg
    // Callbacks: display and keyboard
    glutDisplayFunc(Display);
    glutIdleFunc(Display);
    glutReshapeFunc(Resize);
    // Keyboard
    glutKeyboardFunc(OnKey);
    glutSpecialFunc(OnKeySpecial);
    glutIgnoreKeyRepeat(1);
    glutSpecialUpFunc(OnKeyRelease);
    // Mouse
    glutPassiveMotionFunc(MouseMove);
    glutSetCursor(GLUT_CURSOR_NONE);

    // Allow objects to cover other objects
    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 1; // We never get here, so return 1
}

