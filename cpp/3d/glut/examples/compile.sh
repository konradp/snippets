#!/bin/bash
for f in $(find ./ -type f -name "*.cc" -printf "%f\n"); do
    echo "Compiling ${f}"
    g++ -lGL -lGLU -lglut -o "bin/${f%.*}" "src/$f"
done

