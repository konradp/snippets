# First person 'aircraft' camera

This OpenGL glut (GL Utility) camera is a camera which can be used for aircraft-like first-person view.  
That is, it allows for these rotations:

```
Name    Direction       Axis of rotation
-------------------------------------------------------
yaw     left/right      camera up
pitch   up/down         horizontal
roll    tilt left/right camera direction (lookAt vector)
```
Note: This is in-progress, it will not work at all, until the below TODO is empty.
TODO:
- Migrate what was in `main.cc` into `Game.cc` class.
  - This will not be easy, see C callbacks in C++ references below.
    The issue is that GLUT is a C library and uses C-style callback functions.
    I was trying to migrate GLUT init and callbacks into a standalone class,
    and then tried to make the GLUT callback functions (such as `Display()` into
    static members. That then didn't work. Investigate the below to handle callbacks properly.
    http://tedfelix.com/software/c++-callbacks.html
    https://stackoverflow.com/questions/18550818/glut-code-in-c-with-classes
    Due to this, will switch to SDL instead of GLUT going forward.
    Abandoning for now, but keeping this here as an exercise for later..
- Update the `Display\Update` callback to be a quaternion camera

DONE:
- Fix 'invalid use of non-static member function' for GLUT callback functions

