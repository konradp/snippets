#include "Game.h"

#include <GL/glut.h>
#include <iostream>
#include <map>
#include <math.h>

Game::Game(int *argc, char *argv[])
{
    std::cout << "Constructor" << std::endl;

    std::cout << "Running" << std::endl;
    Init();

    // GLUT init
    glutInit(argc, argv);
    glutInitWindowSize(300, 300);
    glutCreateWindow("Scene example");

    // Set GLUT callback functions
    glutDisplayFunc(Display);
 
    glutMainLoop();
    return; // Only reached on error
}

void
Game::Run(int *argc, char *argv[])
{
}

void
Game::Display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    // Camera here
    mCam.Refresh();

    // Grid and teapot
    DrawGrid();
    glPushMatrix();
    glTranslatef(0, 0.5, 0);
    glColor3f(0, 0.8, 0);
    glutWireTeapot(0.6);
    glPopMatrix();

    glutSwapBuffers();
}

void
Game::DrawGrid()
{
    glPushMatrix();
    glColor3f(1, 1, 1); // white

    for(int i = -50; i < 50; i++) {
        glBegin(GL_LINES);
            glVertex3f(i, 0, -50);
            glVertex3f(i, 0,  50);
        glEnd();
        glBegin(GL_LINES);
            glVertex3f(-50, 0, i);
            glVertex3f( 50, 0, i);
        glEnd();
    }
    
    glPopMatrix();
}

void
Game::Init()
{
    // Not used
}

/*
// Forward-declare main callbacks
void Display();
void Resize(int win_w, int win_h);
void OnKey(unsigned char key, int x, int y);
void OnKeyUp(unsigned char key, int x, int y);
void OnMouseMove(int x, int y);
void Timer(int delay);
// Other
void DrawGrid();

// VARIABLES
// Global
Camera g_cam;
int g_viewport_width = 0;
int g_viewport_height = 0;

// Movement Settings
const float g_mv_velocity       = 0.05;
const float g_rotation_velocity = (M_PI/180)*0.2;

// Movement states
enum MoveStates { MOVE, RYAW, ROLL, PTCH };
// movestate: -1, 0, 1
std::map<MoveStates, int> g_movestate {
    { MOVE, 0 },
    { PTCH, 0 },
    { ROLL, 0 },
    { RYAW, 0 }
};
bool g_key[256];

// MAIN
int main(int argc, char *argv[])
{

    // Callback functions
    glutDisplayFunc(Display); // DELETED
    glutIdleFunc(Display);
    glutReshapeFunc(Resize);
    glutKeyboardFunc(OnKey);
    glutKeyboardUpFunc(OnKeyUp);
    glutPassiveMotionFunc(OnMouseMove);

    // Settings
    glutIgnoreKeyRepeat(1);
    glutSetCursor(GLUT_CURSOR_NONE);

    // GL
    glClearColor(0, 0, 0, 0); // black bg
    glEnable(GL_DEPTH_TEST);
    g_cam.SetPos(-2.0, 0.3, 0);

    glutTimerFunc(1, Timer, 0);
}


// Keep x/y ratio on window resize
void Resize(int win_w, int win_h)
{
    if(win_w == 0 || win_h == 0) return;
    g_viewport_width = win_w;
    g_viewport_height = win_h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // gluPerspective here: Crucial
    // TODO: Learn more about this
    // Angle of sight, width, height, depth
    gluPerspective(60, (GLfloat) win_w / (GLfloat) win_h, 0.1, 100);
    
    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, win_w, win_h);
}

// Keyboard/mouse functions
// Exit on 'q' or 'Escape'
void OnKey(unsigned char key, int x, int y) {
    if(key == 'q' || key == 27) exit(0);
    g_key[key] = true;

    switch(key) {
        case 'w': g_movestate[PTCH] =  1; break;
        case 's': g_movestate[PTCH] = -1; break;
        //case 'w': g_movestate[MOVE] =  1; break;
        //case 's': g_movestate[MOVE] = -1; break;
        case '[': g_movestate[ROLL] =  1; break;
        case ']': g_movestate[ROLL] = -1; break;
        case 'a': g_movestate[RYAW] =  1; break;
        case 'd': g_movestate[RYAW] = -1; break;
    }
}

void OnKeyUp(unsigned char key, int x, int y)
{
    // If a key is released, stop the movement, but
    // check in case another 'movement' key is albo already pressed
    g_key[key] = false;
    switch(key) {
        case 'w':
            if(g_key['s']) g_movestate[PTCH] = -1;
            else g_movestate[PTCH] = 0;
            break;
        case 's':
            if(g_key['w']) g_movestate[PTCH] = 1;
            else g_movestate[PTCH] = 0;
            break;
        case 'a':
            if(g_key['d']) g_movestate[RYAW] = -1;
            else g_movestate[RYAW] = 0;
            break;
        case 'd':
            if(g_key['a']) g_movestate[RYAW] = 1;
            else g_movestate[RYAW] = 0;
            break;
        case '[':
            if(g_key[']']) g_movestate[ROLL] = -1;
            else g_movestate[ROLL] = 0;
            break;
        case ']':
            if(g_key['[']) g_movestate[ROLL] = 1;
            else g_movestate[ROLL] = 0;
            break;
        case 'k': g_cam.Fly(0); break;
        case 'm': g_cam.Fly(0); break;
    }
}


void OnMouseMove(int x, int y)
{
    // Avoid a loop due to glutWarpPointer
    // TODO: ?
    static bool just_warped = false;
    if(just_warped) {
        just_warped = false;
        return;
    }

    int dx = x - g_viewport_width/2;
    int dy = y - g_viewport_height/2;

    if(dx) g_cam.RotateYaw  (g_rotation_velocity*dx);
    if(dy) g_cam.RotatePitch(g_rotation_velocity*dy);

    // Lock mouse pointer to screen centre
    glutWarpPointer(g_viewport_width/2, g_viewport_height/2);
    just_warped = true;
}

void Timer(int delay)
{
    if(g_movestate[MOVE] > 0) g_cam.Move( g_mv_velocity);
    if(g_movestate[MOVE] < 0) g_cam.Move(-g_mv_velocity);

    if(g_movestate[PTCH] > 0) g_cam.RotatePitch(g_mv_velocity);
    if(g_movestate[PTCH] < 0) g_cam.RotatePitch(-g_mv_velocity);

    if(g_movestate[ROLL] > 0) g_cam.RotateRoll(-g_mv_velocity);
    if(g_movestate[ROLL] < 0) g_cam.RotateRoll( g_mv_velocity);

    if(g_movestate[RYAW] > 0) g_cam.RotateYaw(-g_mv_velocity);
    if(g_movestate[RYAW] < 0) g_cam.RotateYaw( g_mv_velocity);

    glutTimerFunc(1, Timer, 0);
}

*/
