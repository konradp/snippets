#ifndef GAME_H
#define GAME_H

#include "Camera.h"

class Game {
public:
    Game(int *argc, char *argv[]);

    void Run(int *argc, char *argv[]);
    void Init();

private:
    // GLUT callback functions
    static void Display();
    static void DrawGrid();

    Camera mCam;

};

#endif // GAME_H
