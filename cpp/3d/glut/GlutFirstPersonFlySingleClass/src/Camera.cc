#include "Camera.h"
#include <GL/glut.h>
#include <iostream>
#include <math.h>

void
Camera::Init()
{
    // Set angles and initial position
    m_pitch = 0.0;
    m_roll = 0.0;
    m_yaw = 0.0;
    SetPos(0, 0, 0);
}

void
Camera::Refresh()
{
    // x,y = plane, z = up/down
    // LookAt angles
    m_lx = cos(m_yaw) * cos(m_pitch);
    m_ly = sin(m_pitch);
    m_lz = sin(m_yaw) * cos(m_pitch);

    // Strafe angles: always perpendicular to the LookAt direction
    m_strafe_lx = cos(m_yaw - M_PI_2);
    m_strafe_lz = sin(m_yaw - M_PI_2);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(
        m_x,        m_y,        m_z,
        m_x + m_lx, m_y + m_ly, m_z + m_lz,
        0.0,        1.0,        0.0
    );

//    std::cout
//        << "Camera: " << m_x << " : " << m_y << " : " << m_z << std::endl
//        << "LookAt: " << m_lx << " : " << m_ly << " : " << m_lz << std::endl << std::endl;
}

void
Camera::SetPos(float x, float y, float z)
{
    m_x = x;
    m_y = y;
    m_z = z;
    Refresh();
}

/* Movement */
void
Camera::Move(float incr)
{
    // Note: This allows for flying
    m_x += incr*m_lx;
    m_z += incr*m_lz;
    // Note: Below commented out to disallow flying
    // m_y += incr*m_ly;

    Refresh();
}

void
Camera::Strafe(float incr)
{
    m_x += incr*m_strafe_lx;
    m_z += incr*m_strafe_lz;
    Refresh();
}

void
Camera::Fly(float incr)
{
    m_y += incr;
    Refresh();
}

/* Angles */
void
Camera::RotatePitch(float angle)
{
    // Set angle limit (otherwise camera flips upside down)
    const float limit = (89.0 * M_PI) / 180.0;

    // Note: Set thit to positive for reverse up/down
    m_pitch -= angle;
    
    if(m_pitch < -limit) m_pitch = -limit;
    if(m_pitch >  limit) m_pitch =  limit;
    Refresh();
}

void
Camera::RotateRoll(float angle)
{}

void
Camera::RotateYaw(float angle)
{
    m_yaw += angle;
    Refresh();
}

