#ifndef CAMERA_H
#define CAMERA_H

/* Based on Nghia Ho's Camera */

class Camera {
public:
    Camera() { Init(); }
    ~Camera() {}
    void Init();
    void Refresh();
    void SetPos(float x, float y, float z);

    // Movement
    void Move(float incr);
    void Strafe(float incr);
    void Fly(float incr);

    // Angles
    void RotatePitch(float angle);
    void RotateRoll(float angle);
    void RotateYaw(float angle);
private:
    float m_x, m_y, m_z; // Position
    float m_lx, m_ly, m_lz; // LookAt direction
    float m_pitch, m_roll, m_yaw; // Up/down, tilt, left/right angles
    float m_strafe_lx, m_strafe_lz; // Helper vector: Always perpendicular to current dir
};
#endif
