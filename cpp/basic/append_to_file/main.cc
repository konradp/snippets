#include <iostream>
#include <fstream>

using std::ofstream;
using std::endl;

int main() {
  ofstream file;
  // Open for writing, appending
  file.open("main.log", ofstream::out | ofstream::app);
  file << "Test" << endl;
  file.close();
  return 0;
}
