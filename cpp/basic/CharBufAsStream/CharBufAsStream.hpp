#include <iostream>
#include <vector>

// This is so we can write
// cout << http_header << std::endl;

class HttpHeader {
public:
    HttpHeader() {};
    
    // Getters
    std::string GetStr() const {
        std::string str;
        for(auto el: _fields) {
            str += el.first + ": " + el.second + "\n";
        }
        return str;
    };

    // Setters
    void AddField(std::string name, std::string value) {
        _fields.push_back(std::make_pair(name, value));
    };

    // Operator overload
    friend std::ostream& operator<< (std::ostream &out, const HttpHeader& h);
private:
    std::vector<std::pair<std::string, std::string>> _fields;

};

// Operator overloads
std::ostream& operator<< (std::ostream &out, const HttpHeader& h) {
    return out << h.GetStr();
};

class HttpMessage {
// HttpMessage:
// - header
// - body
public:
    HttpMessage()
    : _type(1)
    {
        std::cout << "Initialised" << std::endl;
    };
    
    // Getters
    std::string GetRequest()   { return _request_status; };
    std::string GetStatus()    { return _request_status; };
    HttpHeader  GetHeader()    { return _header; };
    std::string GetBody()      { return _body; };
    int         GetType()      { return _type; };

    // Setters
    bool SetHeader(HttpHeader h) { _header = h; };
    bool SetBody() {};

private:
    std::string _request_status;
    HttpHeader _header;
    std::string _body;
    int _type; // 1: request, 2: response


};


