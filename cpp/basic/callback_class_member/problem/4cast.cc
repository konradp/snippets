#include <iostream>
#include <unistd.h> // for usleep
using std::cout;
using std::endl;

// Library
typedef void (*lib_timer_cb)(int number);
void start_timer(lib_timer_cb cb, void* data) {
  int ms = 1000000; // second in microseconds
  usleep(2*ms);
  cb(2);
};

// My program
class MyClass {
  public:
  MyClass() {
    start_timer(_MyCallback, (void*)this);
  };
  int member;

  private:
  void MyCallback(int number) {
    cout << "My number is: " << number << endl;
    member = number;
  };

  static void _MyCallback(int number, void *data) {
    MyClass *c = (MyClass*)data;
    c->MyCallback(number);
  };
}; // end MyClass

int main() {
  MyClass c = MyClass();
}
