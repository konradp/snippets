#include <iostream>
#include <unistd.h> // for usleep
using std::cout;
using std::endl;

// Library
typedef void (*lib_timer_cb)(int number);
void start_timer(lib_timer_cb cb, int seconds) {
  int ms = 1000000; // second in microseconds
  usleep(seconds*ms);
  cb(seconds);
};

// My program
class MyClass {
  public:
  MyClass() {
    start_timer(MyCallback, 2);
  };

  private:
  void MyCallback(int number) {
    cout << "My number is: " << number << endl;
  };
}; // end MyClass

int main() {
  MyClass c = MyClass();
}
