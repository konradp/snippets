#include <iostream>
#include <unistd.h> // for usleep
#include <uv.h>
using std::cout;
using std::endl;
uv_loop_t *loop;
uv_timer_t timer;


// My program
class MyClass {
  public:
  struct TimerData {
    int number;
    MyClass* instance;
  };
  MyClass() {
    loop = uv_default_loop();
    uv_timer_init(loop, &timer);
    uv_handle_t* h = (uv_handle_t*) &timer;
    uv_handle_set_data(h, new TimerData { 1, this });
    uv_timer_start(&timer, _MyCallback, 1000, 2000);

    uv_run(loop, UV_RUN_DEFAULT);
    uv_loop_close(loop);
  };
  int member = 4;

  private:
  static void _MyCallback(uv_timer_t* timer) {
    TimerData *td = (TimerData*)timer->data;
    MyClass *c = (MyClass*)td->instance;
    c->MyCallback(td->number);
  };
  void MyCallback(int timer_number) {
    cout << "User data number: " << timer_number << endl;
    cout << "Class member data: " << member << endl;
    member = 5;
  };
}; // end MyClass

int main() {
  MyClass c = MyClass();
}
