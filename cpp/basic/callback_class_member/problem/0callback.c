#include <iostream>
#include <unistd.h> // for usleep
using std::cout;
using std::endl;

// Library
typedef void (*lib_timer_cb)(int number);
void start_timer(lib_timer_cb cb, int seconds) {
  int ms = 1000000; // second in microseconds
  usleep(seconds*ms);
  cb(seconds);
};


// Main program
void my_callback(int number) {
  cout << "Passed to callback: " << number << endl;
}

int main() {
  start_timer(my_callback, 1);
  start_timer(*my_callback, 2);
  start_timer(&my_callback, 2);
}
