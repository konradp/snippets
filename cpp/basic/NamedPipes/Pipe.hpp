#pragma once
#ifndef PIPE_HPP
#define PIPE_HPP
#include <iostream>
#include <string>
#include <unistd.h> // unlink

using std::cout;
using std::endl;
using std::string;

class Pipe {
public:
Pipe(std::string s)
{
    kName = "/tmp/" + s;
    int ret;
    ret = mkfifo(kName.c_str(), 0666);
    if(ret == 0) {
        cout << "Good" << endl;
    } else {
        cout << "Bad" << endl;
    }
};
~Pipe()
{
    unlink(kName.c_str());
}

private:
    string kName;



// Remove pipe
//ret = unlink("pip");
}; // class Pipe
#endif // PIPE_HPP
