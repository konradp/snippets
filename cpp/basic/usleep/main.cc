#include <iostream>
#include <unistd.h> // for usleep

using std::cout;
using std::endl;

int main() {
  int second = 1000000; // 1000 * 1000
  usleep(2*second);
  cout << "Hello World" << endl;
  return 0;
}
