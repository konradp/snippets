#include <iostream>
#include <asio.hpp>

using std::cout;
using std::endl;

void print(const asio::error_code& /*e*/) {
  cout << "Test" << endl;
}

int main() {
  cout << "Start" << endl;

  asio::io_context io;
  cout << "Context started" << endl;

  asio::steady_timer t(io, asio::chrono::seconds(5));
  cout << "After steady_timer" << endl;

  t.async_wait(&print);
  cout << "After async_wait" << endl;
  // Note: The async_wait does not block

  io.run();
  // WAIT HERE, show 'Test'
  cout << "After io.run(), end" << endl;
  return 0;
}
