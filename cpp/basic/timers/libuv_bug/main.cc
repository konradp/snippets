#include <iostream>
#include <thread>
#include <unistd.h> // usleep
#include <uv.h>
uv_loop_t *loop;
uv_timer_t timer_req;
float timerSeconds = 2.0f;
float sleepSeconds = 1.0f;

void timerCallback(uv_timer_t* handle) {
  std::cout << "Callback" << std::endl;
  uv_timer_stop(&timer_req);
}

int main() {
  loop = uv_default_loop();
  uv_run(loop, UV_RUN_DEFAULT);
  uv_timer_init(loop, &timer_req);
  while (true) {
    std::cout << "sleep" << std::endl;
    uv_timer_start(&timer_req, timerCallback, 1000*timerSeconds, 0);
    usleep(1000000*sleepSeconds);
    uv_timer_stop(&timer_req);
  };
  return 0;
}
