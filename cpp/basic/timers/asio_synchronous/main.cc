#include <iostream>
#include <asio.hpp>

using std::cout;
using std::endl;

int main() {
  cout << "Start" << endl;

  asio::io_context io;
  cout << "Context started" << endl;

  asio::steady_timer t(io, asio::chrono::seconds(5));
  cout << "After steady_timer" << endl;

  t.wait();
  // WAIT HERE
  cout << "After t.wait(), end" << endl;

  return 0;
}
