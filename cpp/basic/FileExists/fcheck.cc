#include <iostream>
#include <fstream>
#include <sys/stat.h>

void usage()
{
    std::cout << "./fcheck filename" << std::endl;
}

bool IsFileAndExists(std::string s)
{
    struct stat path_stat;
    stat(s.c_str(), &path_stat);
    return S_ISREG(path_stat.st_mode);
}

int main(int argc, char **argv)
{
    if(argc != 2) {
        usage();
        return 1;
    }

    if( IsFileAndExists(argv[1]) ) {
        std::cout << "exists" << std::endl;
        return 0;
    } else {
        std::cout << "does not exist" << std::endl;
        return 1;
    }
}

