#include <chrono> // clock
#include <cstdlib>
#include <curses.h>
#include <iostream>
#include <string>
#include <vector>
#include <unistd.h> // sleep

void Usage()
{
    std::cout << "./rhex length, e.g." << std::endl
              << "./rhex 4" << std::endl
              << "gives: AB3C H" << std::endl;
    return;
}

std::string GetRandomHex(int length)
{

    std::vector<char> v = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'B', 'C', 'D', 'E', 'F'
    };
    //char* hexDigits = "0123456789ABCDEF";
    char randHex[length+1];
    for(int i = 0; i < length; i++) {
        srand(time(0)+i);
        randHex[i] = v[ rand() % 16 ];
    }
    randHex[length] = '\0'; // end C-string
    std::string s = std::string(randHex);
    return s;

}

int main(int argc, char *argv[])
{
    // Check input
    if(argc != 2) {
        Usage();
        return 1;
    }

    // ncurses init and settings
    initscr();
    cbreak();               // process char immediately
    curs_set(0);            // no cursor
    noecho();               // don't echo keys
    keypad(stdscr, TRUE);
    
    // Print random HEX number
    mvprintw(0, 0, "Press 'q' when done.");
    mvprintw(
        1, 0,
        "Number: %sH",
        GetRandomHex( std::stoi(argv[1]) ).c_str()
    );

    // Start timer
    auto tStart = std::chrono::high_resolution_clock::now();

    // Wait until quit pressed
    while(getch() != 'q') { sleep(1); }
    endwin();

    // Print time elapsed
    auto tEnd = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = tEnd - tStart;
    std::cout.precision(1);
    std::cout << "Elapsed: " << (int) elapsed.count() << "s" << std::endl;
}

