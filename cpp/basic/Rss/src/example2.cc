// Parse an RSS XML file and print the selected data
// as a tab-separated table

#include <iostream>
#include <tinynet/tinynet.hpp>
#include <tinyxml2.h>

using std::cerr;
using std::cout;
using std::endl;
using std::string;
using tinyxml2::XMLDocument;
using tinyxml2::XMLElement;

void die(string msg) {
  cerr << msg << endl;
  exit(1);
}

// Utility method
string
getElementText(XMLElement* e) {
  if(e != NULL) return e->GetText();
  else throw std::runtime_error("Could not get text for element");
};

int main()
{
  XMLDocument doc;
  int err = doc.LoadFile("./example2.xml");
  if(err != tinyxml2::XML_SUCCESS) die("Could not load file");

  XMLElement* r = doc.FirstChildElement("feed");
  if(r == NULL) die("Error parsing");

  XMLElement* entry = r->FirstChildElement("entry");
  XMLElement* c;
  while(entry != NULL) {
    c = entry->FirstChildElement("content");
    cout
      << getElementText(c->FirstChildElement("filing-date"))
      << '\t'
      << getElementText(c->FirstChildElement("filing-type"))
      << '\t'
      << getElementText(c->FirstChildElement("form-name"))
      << endl;
    // TODO: create a helper method for GetText, checking null
    entry = entry->NextSiblingElement("entry");
  }
}
