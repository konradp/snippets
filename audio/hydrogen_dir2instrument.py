#!/usr/bin/env python3
import os
import sys
import xml.etree.ElementTree as ET


def usage():
  print(f'Usage: {sys.argv[0]} PATH_SONG DIR_SAMPLES')

def get_instrument(idx, name, midi_out_note, sample_path):
  return f'''\
  <instrument>
   <id>{idx}</id>
   <name>{name}</name>
   <drumkit>GMkit</drumkit>
   <volume>0.8</volume>
   <isMuted>false</isMuted>
   <pan_L>0.58</pan_L>
   <pan_R>1</pan_R>
   <gain>1</gain>
   <applyVelocity>true</applyVelocity>
   <filterActive>false</filterActive>
   <filterCutoff>1</filterCutoff>
   <filterResonance>0</filterResonance>
   <FX1Level>0</FX1Level>
   <FX2Level>0</FX2Level>
   <FX3Level>0</FX3Level>
   <FX4Level>0</FX4Level>
   <Attack>0</Attack>
   <Decay>0</Decay>
   <Sustain>1</Sustain>
   <Release>1000</Release>
   <randomPitchFactor>0</randomPitchFactor>
   <muteGroup>-1</muteGroup>
   <isStopNote>false</isStopNote>
   <sampleSelectionAlgo>VELOCITY</sampleSelectionAlgo>
   <midiOutChannel>-1</midiOutChannel>
   <midiOutNote>{midi_out_note}</midiOutNote>
   <isHihat>-1</isHihat>
   <lower_cc>0</lower_cc>
   <higher_cc>127</higher_cc>
   <instrumentComponent>
    <component_id>0</component_id>
    <gain>1</gain>
    <layer>
     <filename>{sample_path}</filename>
     <ismodified>false</ismodified>
     <smode>forward</smode>
     <startframe>0</startframe>
     <loopframe>0</loopframe>
     <loops>0</loops>
     <endframe>0</endframe>
     <userubber>0</userubber>
     <rubberdivider>1</rubberdivider>
     <rubberCsettings>4</rubberCsettings>
     <rubberPitch>1</rubberPitch>
     <min>0</min>
     <max>1</max>
     <gain>1</gain>
     <pitch>0</pitch>
    </layer>
   </instrumentComponent>
  </instrument>\
'''

def main():
  if len(sys.argv) != 3:
    usage()
    exit(1)

  # Args
  path_song = sys.argv[1]
  dir_samples = sys.argv[2]

  # Read song file
  # Clear instruments
  tree = ET.parse(path_song)
  root = tree.getroot()

  instrument_list = root.find('instrumentList');
  instrument_list.clear()

  # Append instruments
  #print(get_instrument(2, 'B01.wav', 3))
  # Get sample paths
  samples = os.listdir(dir_samples)
  midi = 60
  idx = 14
  for sample in sorted(samples):
    sample_path = f'{dir_samples}/{sample}'
    a = get_instrument(idx, sample.replace('.wav', ''), midi, sample_path)
    b = ET.fromstring(a)
    instrument_list.append(b)
    midi += 1
    idx += 1

  # Save file
  tree.write(path_song)

if __name__ == '__main__':
  main()
