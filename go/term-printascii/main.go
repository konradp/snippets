// Print a box of chars
package main

import (
  "fmt"
  "os"
  kb "github.com/eiannone/keyboard"
)

func main() {
  var s string = "> "
  fmt.Println("To exit, press 'q' or ESC")
  fmt.Printf("%#s", s);
  // Get char
  char, key, err := kb.GetSingleKey()
  if (err != nil) {
      panic(err)
  }
  if key == kb.KeyBackspace2 {
    // Backspace
  }
  if key == kb.KeyEsc || char == 'q' {
    fmt.Println("Exitting")
    os.Exit(0)
  } else {
    // Add char
    s := "\033[10B"
    fmt.Printf("F: %s", s[:0])
    fmt.Printf("%#s : %#s : %#s\n", string(s), char, key)
    fmt.Printf("%#q : %#q : %#q\n", string(s), char, key)
  }
}
