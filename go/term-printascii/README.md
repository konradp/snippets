# term
An snippet using [goterm library](https://github.com/buger/goterm) to diplay a terminal like input.

# Compile and run
Compile and run the snippet.
```
make
./bin/main
```
Press various keys, including backspace.
