// Print a box of chars
package main

import (
  //"fmt"
  kb "github.com/eiannone/keyboard"
  tm "github.com/buger/goterm"
)

func main() {
  var err error
  var char rune
  for {
    tm.Clear()
    w := tm.Width()
    h := tm.Height()
    tm.MoveCursor(0,0)
    tm.Printf("width: %#d\n", w)
    tm.Printf("height: %#d\n", h)
    tm.Println("Press a key, or 'q' to exit")
    tm.Flush()
    // Get char
    char, _, err = kb.GetSingleKey()
    if (err != nil) {
        panic(err)
    }
    // Exit
    if char == 'q' {
      tm.MoveCursor(0, h)
      tm.Println()
      tm.Flush()
      break
    }
    // Draw box of chars (runes)
    for i := h/2; i < h+1; i++ {
      tm.MoveCursor(30, i)
      for j := 30; j < w+1; j++ {
        tm.Printf("%#s", string(char))
      }
    }
  } //for
}
