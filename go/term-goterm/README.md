# term
An snippet using [goterm library](https://github.com/buger/goterm) to display characters in a terminal.

![term](term.gif?raw=true" "term example")

# Compile and run
Compile and run the snippet.
```
make
./bin/main
```
Press various keys to affect the bottom-right box of characters.
