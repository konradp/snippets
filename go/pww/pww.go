package main
import (
  "crypto/aes"
  "crypto/cipher"
  "crypto/rand"
  "errors"
  "fmt"
  "io"
  "io/ioutil"
)

func main() {
  fmt.Println("Start")

  text := []byte("Secret text")
  key := []byte("passphrasewhichneedstobe32bytes!")

  ciphertext, err := encrypt(text, key)
  if err != nil {
    fmt.Println(err)
  }

  err = ioutil.WriteFile("myfile.data", ciphertext, 0777)
  // handle this error
  if err != nil {
    // print it out
    fmt.Println(err)
  }


  plaintext, err := decrypt(ciphertext, key)
  if err != nil {
    fmt.Println(err)
  }
  fmt.Printf("%x => %s\n", ciphertext, plaintext)
}

func encrypt(plaintext []byte, key []byte) ([]byte, error) {
  c, err := aes.NewCipher(key)
  if err != nil {
    return nil, err
  }

  gcm, err := cipher.NewGCM(c)
  if err != nil {
    return nil, err
  }

  nonce := make([]byte, gcm.NonceSize())
  if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
    return nil, err
  }

  return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

func decrypt(ciphertext []byte, key []byte) ([]byte, error) {
  c, err := aes.NewCipher(key)
  if err != nil {
    return nil, err
  }

  gcm, err := cipher.NewGCM(c)
  if err != nil {
    return nil, err
  }

  nonceSize := gcm.NonceSize()
  if len(ciphertext) < nonceSize {
    return nil, errors.New("ciphertext too short")
  }

  nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
  return gcm.Open(nil, nonce, ciphertext, nil)
}
