// Print a box of chars
package main

import (
  kb "github.com/eiannone/keyboard"
  tm "github.com/buger/goterm"
)

func main() {
  prompt := "> "
  var s string = prompt
  var l int
  for {
    h := tm.Height()
    l = len(s)
    // Prompt
    tm.Clear()
    tm.MoveCursor(0,h)
    tm.Printf("To exit, press 'q' or ESC")
    tm.MoveCursor(0,0)
    tm.Printf("%#s", s);
    tm.Flush()
    // Get char
    char, key, err := kb.GetSingleKey()
    if (err != nil) {
        panic(err)
    }
    if key == kb.KeyBackspace2 {
      // Backspace
      if l > 2 {
        s = s[:l-1]
      }
    } else if key == kb.KeyEsc || char == 'q' {
      // Exit
      tm.MoveCursor(0, h)
      tm.Clear()
      tm.Println("Exitting")
      tm.Flush()
      break
    } else {
      // Add char
      s += string(char)
    }
    tm.Flush()
  } //for
}
