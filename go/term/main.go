// Print terminal width and height
package main

import (
  "fmt"
  "os"
  "os/exec"
  "strconv"
  "strings"
  "github.com/eiannone/keyboard"
)

//var Screen *bytes.Buffer = new(bytes.Buffer)
var Screen = os.Stdout

func getsize() (string, error) {
  cmd := exec.Command("stty", "size")
  cmd.Stdin = os.Stdin
  s, err := cmd.Output()
  if err != nil {
    return string(s), err
  }
  return strings.TrimSuffix(string(s), "\n"), nil
}

func getheight() (int, error) {
  size, err := getsize()
  if err != nil {
    panic(err)
  }
  i, err := strconv.Atoi(strings.Split(size, " ")[0])
  if err != nil {
    panic(err)
  }
  return i, nil
}

func getwidth() (int, error) {
  size, err := getsize()
  if err != nil {
    panic(err)
  }
  i, err := strconv.Atoi(strings.Split(size, " ")[1])
  if err != nil {
    panic(err)
  }
  return i, nil
}

// TERMINAL escape sequences
func Clear() {
  // Clear screen
  fmt.Fprintf(Screen,  "\033[H\033[2J")
}

func MvCursor(x int, y int) {
  // Move cursor to given position
  fmt.Fprintf(Screen, "\033[%d;%dH", y, x)
}

/////////////////////// MAIN /////////////////////
func main() {
  var char = '0'
  for {
    Clear()
    // Get width/height
    w, err := getwidth()
    if err != nil {
      panic(err)
    }
    h, err := getheight()
    if err != nil {
      panic(err)
    }
    // Print instructions
    Clear()
    MvCursor(0, 0)
    fmt.Printf("width: %#d\n", w)
    fmt.Printf("height: %#d\n", h)
    fmt.Println("Press a key, or 'q' to exit")
    // Draw box of char
    for i := h/2; i < h+1; i++ {
      MvCursor(30, i)
      for j := 30; j < w+1; j++ {
        fmt.Printf("%#s", string(char))
      }
    }

    // Get char
    char, _, err = keyboard.GetSingleKey()
    if (err != nil) {
        panic(err)
    }
    // Exit
    if char == 'q' {
      MvCursor(0, h)
      fmt.Printf("\n")
      fmt.Println("Exitting")
      break
    }
  }
}
