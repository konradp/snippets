# term
An example using bash ANSI escape sequences to clear screen and draw characters to screen similar to ncurses.  
See ANSI escape sequences http://tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html

![term](term.gif?raw=true" "term example")

**Note:** This snippet was me studying the [goterm](https://github.com/buger/goterm) library from first principles. Use the goterm library for snippets such as this. See [../term/goterm snippet](../term-goterm).

# Compile and run
Compile and run the snippet.
```
make
./bin/main
```
Press various keys to affect the bottom-right box of characters.
