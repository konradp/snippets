// Print terminal width and height
package main

import (
  //"fmt"
)

/////////////////////// MAIN /////////////////////
func main() {
  var char = "0"
  //_ = char
  tm.Clear()
  for {
    w := tm.Width()
    h := tm.Height()
    tm.MoveCursor(0,0)
    tm.Printf("width: %#d\n", w)
    tm.Printf("height: %#d\n", h)
    tm.Println("Press a key, or 'q' to exit")


    // Get char
    char, _, err := keyboard.GetSingleKey()
    if (err != nil) {
        panic(err)
    }
    // Exit
    if char == 'q' {
      tm.MoveCursor(0, h)
      //fmt.Printf("\n")
      //fmt.Println("Exitting")
      break
    }

    // Draw box of char
    tm.Clear()
    for i := h/2; i < h+1; i++ {
      tm.MoveCursor(30, i)
      for j := 30; j < w+1; j++ {
        tm.Printf("%#s", string(char))
      }
    }
    tm.Flush()






  }
}
