// This gets a key
package main

import (
  "fmt"
  "github.com/eiannone/keyboard"
)

func main() {
  fmt.Println("Press q to exit")
  for {
    char, _, err := keyboard.GetSingleKey()
    if (err != nil) {
        panic(err)
    }
    if char == 'q' {
      fmt.Println("Exitting")
      break
    } else {
      fmt.Printf("You pressed: %q. Press 'q' to exit.\n", char)
    }
  }
}
