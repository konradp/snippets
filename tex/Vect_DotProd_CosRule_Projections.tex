\documentclass[11pt]{article}
\usepackage{amssymb, amsthm, amsmath, bm, caption, hyperref, tikz}
\usepackage[a4paper, margin=0.3cm]{geometry}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{float}
\usetikzlibrary{arrows, angles, quotes}
\theoremstyle{definition}
\newenvironment{Figure}
  {\par\medskip\noindent\minipage{\linewidth}}
  {\endminipage\par\medskip}

\newtheorem{defn}{Definition}[section]
\newtheorem{theorem}{Theorem}[section]
\newtheorem*{thm}{Theorem}

\title{\textbf{Vector dot product: cosine formula and projections}}
\begin{document}
\date{}
\maketitle
\begin{multicols}{2}

\section*{Dot product: Cosine formula definition}
%* COSINE FORMULA
\noindent
\begin{thm}
Dot product algebraic definition is equivalent to geometric definition,
i.e. \\
\begin{align*}
    \bm{a} \bullet \bm{b} &= |\bm{a}|  | \bm{b} | cos(\theta) \\
                          &= \sum_i^n a_i b_i
                          \quad .
\end{align*}
\end{thm}

%* COSINE FORMULA: proof
\begin{proof} (sketch) \\
To prove, consider a dot product of a vector with a unit vector (a projection), e.g. in $\mathbb{R}^2$ when $e_i = (1,0)$
	\[
		\bm{a} \bullet \bm{e_i}
		= \begin{bmatrix} 4 \\ 2 \end{bmatrix}
		\bullet \begin{bmatrix} 1 \\ 0 \end{bmatrix}
		= 4 + 0
		= 4
		\quad ,
	\]
and remember that vectors can be expressed as a linear combination of the basis vectors (a sum of the scaled basis vectors), e.g:
\[ \bm{a} = \sum_i^n a_i \bm{e_i} \quad ,\]
where $\bm{e_i}$ are the basis vectors i.e. in $\mathbb{R}^3$ these are $e_1 = (1, 0, 0)$, $e_2 = (0, 1, 0)$, $e_3 = (0, 0, 1)$.

%* COSINE FORMULA: diagram
\begin{minipage}{\linewidth}
    \begin{center}
	\begin{tikzpicture}
		\coordinate (orig) at (0, 0);
		\coordinate (a_vec) at (4, 2);
		\coordinate (i_vec) at (1, 0);
		\draw[help lines, color=lightgray, dashed]
			(orig) grid (5.9, 3.9);
		\draw[->] (orig)--(6,0)
			node[right]{$x$};
		\draw[->] (orig)--(0,4)
			node[above]{$y$};
			
		\draw[->, very thick] (orig)--(4,2)
			node[midway, above, sloped] {$\bm{a}$};
		\draw[->, very thick] (orig)--(i_vec)
			node[midway, below, sloped] {$\bm{e_i}$};
		\draw[dashed] (4,2)--(4,0)
			node[midway, above, sloped] { projection };
		\draw[dashed] (orig)--(6,3);
		\pic [draw, ->, "$\theta$", angle eccentricity=1.5]
			{ angle = i_vec--orig--a_vec };
		\draw[->, -] (orig)--(4,0)
			node[midway, below] {$|\bm{a}| \cos(\theta)$};
	\end{tikzpicture}
	\captionof{figure}{Dot product with unit vector}
	\end{center}
\end{minipage}
See \url{https://www.geogebra.org/geometry/xK77hzCs}
\end{proof}
\end{multicols}
\rule{\textwidth}{1pt}
%*
%*
%*
%*
%* EXAMPLES
\section*{Examples: Dot product}
\noindent
\begin{tabular}{ l l }
%* LEFT
\begin{minipage}[top]{0.5\linewidth}
	\begin{center}	
    \begin{tikzpicture}
		\draw[help lines, color=lightgray, dashed]
			(0, 0) grid (7.9, 3.9);
		%* axes
		\draw[->, thick] (0,0)--(8,0) node[right]{$x$};
		\draw[->, thick] (0,0)--(0,4) node[above]{$y$};
		%* ticks
        \foreach \x / \xtext in {1,...,7}
            \draw[shift={(\x,0)}]
            (0pt,2pt) -- (0pt,-2pt) node[below] {$\xtext$};
		\draw[->, very thick]
			(0,0)--(2,3)
			node[midway, above, sloped] {$\bm{a}$};
		\draw[->, very thick]
			(0,0)--(2,1)
			node[midway, below, sloped] {$\bm{b}$};
		\draw[->, very thick]
			(0,0)--(7,0)
			node[pos=0.6, above, sloped] {$\bm{a} \bullet \bm{b}$};
		\draw[dashed, very thick]
			(0,0)--(6,3);
        \draw[dashed, very thick]
            (2,3)--(3.5,0)
            node[pos=0.2, above, sloped] { projection };
	\end{tikzpicture}
	\[
        \bm{a} \bullet \bm{b}
        =       \begin{bmatrix} 2 \\ 3 \end{bmatrix}
        \bullet \begin{bmatrix} 2 \\ 1 \end{bmatrix}
        = 4 + 3 = 7
	\]
	\end{center}
\end{minipage} &
%* RIGHT
\begin{minipage}[top]{0.5\linewidth}
	\begin{center}
	\begin{tikzpicture}
		\draw[help lines, color=lightgray, dashed]
			(0, 0) grid (5.9, 3.9);
		%* axes
		\draw[->, thick] (0,0)--(6,0) node[right]{$x$};
		\draw[->, thick] (0,0)--(0,4) node[above]{$y$};
		%* ticks
        \foreach \x / \xtext in {1,...,5}
            \draw[shift={(\x,0)}]
            (0pt,2pt) -- (0pt,-2pt) node[below] {$\xtext$};
		\draw[->, very thick]
			(0,0)--(1,3)
			node[midway, above, sloped] {$\bm{a}$};
		\draw[->, very thick]
			(0,0)--(2,1)
			node[midway, below, sloped] {$\bm{b}$};
		\draw[->, very thick]
			(0,0)--(5,0)
			node[pos=0.6, above, sloped] {$\bm{a} \bullet \bm{b}$};
		\draw[dashed, very thick]
			(0,0)--(6,3);
        \draw[dashed, very thick]
            (1,3)--(2.5,0)
            node[pos=0.2, above, sloped] { projection };
	\end{tikzpicture}
    \[
        \bm{a} \bullet \bm{b}
        =       \begin{bmatrix} 1 \\ 3 \end{bmatrix}
        \bullet \begin{bmatrix} 2 \\ 1 \end{bmatrix}
        = 2 + 3 = 5
    \]
	\end{center}
\end{minipage} \\
%* LEFT2
\begin{minipage}[top]{0.4\linewidth}
	\begin{center}
    \begin{tikzpicture}
		\draw[help lines, color=lightgray, dashed]
			(0, 0) grid (5.9, 2.9);
		%* axes
		\draw[->, thick] (0,0)--(6,0) node[right]{$x$};
		\draw[->, thick] (0,0)--(0,3) node[above]{$y$};
		%* ticks
        \foreach \x / \xtext in {1,...,5}
            \draw[shift={(\x,0)}]
            (0pt,2pt) -- (0pt,-2pt) node[below] {$\xtext$};
		\draw[->, very thick]
			(0,0)--(5,2)
			node[midway, above, sloped] {$\bm{a}$};
		\draw[->, very thick]
			(0,0)--(1,0)
			node[midway, below, sloped] {$\bm{b}$};
		\draw[->, very thick]
			(0,0)--(5,0)
			node[midway, above, sloped] {$\bm{a} \bullet \bm{b}$};
        \draw[dashed, very thick]
            (5,2)--(5,0)
            node[midway, above, sloped] { projection };
	\end{tikzpicture}
	\[
        \bm{a} \bullet \bm{b}
        =       \begin{bmatrix} 5 \\ 2 \end{bmatrix}
        \bullet \begin{bmatrix} 1 \\ 0 \end{bmatrix}
        = 5 + 0 = 5
	\]
	\end{center}
\end{minipage} &
\begin{minipage}[l]{0.5\linewidth}
\textbf{Reference} \\
\url{http://math.oregonstate.edu/bridge/papers/dot+cross.pdf} \\
\url{https://en.wikipedia.org/wiki/Dot_product}
\end{minipage}
\end{tabular}

\end{document}