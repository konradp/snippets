# 1
Ordered pair
(x,y) := { {x,y}, {x} }

Ordered triple:
(x,y,z) := { {x,y,z}, {x,y}, x }
(x,y,z) := ((x,y),z)
Show the equivalence of the two definitions.

