#!/bin/bash
# This shows that a problem (exitcode 1) can occur if we
# have inside a for-loop only a one-line if-then statement
# which never executes
# Note: Watch out for it in Makefiles
# Solution: Add a dummy 'sleep 0' command

FILE=$0
ARRAY="0 1 2"


# Problem: Returns exitcode 1
for EL in $ARRAY; do
    [ ! -e $FILE ] && echo File exists
done
echo Exitcode: $?


# This works fine
[ -e $FILE ] && sleep 0
echo Exitcode: $?


# This also works fine
# i.e. full version of if/then used
for EL in $ARRAY; do
    if [ ! -e $FILE ]; then echo File exists; fi
done
echo Exitcode: $?


# Solution
# This returns exitcode 0
for EL in $ARRAY; do
    [ ! -e $FILE ] && echo File exists
    sleep 0
done
echo Exitcode: $?

