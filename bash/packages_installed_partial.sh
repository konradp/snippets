#!/bin/sh
dpkg-query --show \
  --showformat='${Package} ${Status}\n' \
  | grep -v 'ok installed'
