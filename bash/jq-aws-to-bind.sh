#!/bin/bash

read -r -d '' VAR <<-'EOF'
{
  "ResourceRecordSets": [
    {
      "ResourceRecords": [
        {
          "Value": "value1.example.com"
        }
      ],
      "Type": "CNAME",
      "Name": "smtp.mrsitemail.com.",
      "TTL": 300
    },
    {
      "ResourceRecords": [
        {
          "Value": "value1.example2.net"
        },
        {
          "Value": "value2.example2.net"
        }
      ],
      "Type": "CNAME",
      "Name": "www.example.com.",
      "TTL": 300
    }
  ]
}
EOF

#echo $VAR | jq -r '. | .Name, .ResourceRecords[].Value'
echo $VAR
echo
echo
TYPE="CNAME"
# Select only matching record type
JQRY1=".ResourceRecordSets[] | select(.Type==\"$TYPE\")"
VAR1=$(echo "$VAR" | jq -r "$JQRY1")
echo $VAR1
JQRY2='
    . as $I
    | .ResourceRecords[]
    | . as $H
    | [$I.Name, "IN", $I.Type] + [$H[]]
    '
AA=$(echo $VAR1 | jq -r "$JQRY2")
BB='[
  "one",
  "IN",
  "three",
  "four"
]'
echo
echo
echo "$AA $BB" | jq -r '. | @tsv' \
  | column -t -s $'\t'

