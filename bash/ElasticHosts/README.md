# Deploy
Create a vanilla OS instance (VM or container) on ElasticHosts account.  
Return JSON with instance info.

You need two bash environment variables exported:  
- EHURI
- EHAUTH

Print help to see the parameters accepted.
```
$ ./createinstance -h
Create a container or a VM.
Usage: ./createinstance [-v] NAME [DISTRO] [SIZE] [ [MEMSIZE] [CPUSIZE] [TYPE] ]
where
  DISTRO    (debian9 centos7)
  SIZE      disk size in GB
  MEMSIZE   memory size in MB
  CPUSIZE   CPU speed in MHz
  TYPE      (container vm)
  -v        verbose

Default:
  DISTRO    debian9
  SIZE      10
  MEMSIZE   512
  CPUSIZE   500
  TYPE      container
  -v        false
```

# Examples
Create a container, call it machine1. Return the container UUID and IP, and connect.
```
$ ./createinstance machine1 | jq '.'
[
  {
    "uuid": "67aebd4c-cfd8-4fa4-aae7-3ebe770820e8",
    "ip": "185.114.37.231"
  }
]
$ ssh root@185.114.37.231
Linux debian 4.14.65-elastic #1 SMP Tue Aug 21 06:18:28 UTC 2018 x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Thu Mar 14 23:28:03 2019 from 88.108.197.7
root@debian:~#
```

Other examples
```
./createinstance vmname
./createinstance vmname debian9
./createinstance vmname2 centos7
./createinstance vmname3 debian9 10
```

Additionally, if the `PUB_KEY` environment variable is set, it will be added to `authorized_keys` on the instance.

# Troubleshooting
Run in verbose mode.
```
./createinstance -v vmname
```


test23
