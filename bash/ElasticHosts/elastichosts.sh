#!/bin/bash

set +o posix
shopt -s extglob

usage() {
  cat >&2 <<EOF
Usage: $0 [ -c | -f FILENAME ] CMD [ARG]...
Options:
  -c            force reading input data for API call from stdin
  -f FILENAME   read input data for API call from FILENAME
  -j            input and output are in JSON format
  -v            show full headers sent and received during API call
  -g            force using the GET HTTP method for API call
  -p            force using the POST HTTP method for API call

If the EHAUTO=1 environment variable is set, the script detects the HTTP
method to use for making the API call and will also pass the input to a POST
request if there is data available at stdin, unless -c or -f arguments have
been passed to the script.

When the API call fails, both the error and headers are being displayed (i.e.
similar to when -v is passed).

This script requires the following mandatory environment variables:
  EHURI=<API endpoint URI>
  EHAUTH=<user uuid>:<secret API key>
EOF
  exit 1
}


if ! type -t curl >/dev/null; then
  echo "This tool requires curl, available from http://curl.haxx.se/" >&2
  exit 1
elif [ ! -n "$EHURI" ]; then
  echo "Please set EHURI=<API endpoint URI>" >&2
  exit 1
elif [ ! -n "$EHAUTH" ]; then
  echo "Please set EHAUTH=<user uuid>:<secret API key>" >&2
  exit 1
fi

DATA=""
TYPE=""
VERBOSE=""
METHOD=

while getopts cf:jvgp OPTION; do
  case "$OPTION" in
    c)
      DATA="-"
      ;;
    f)
      if [ -e "$OPTARG" ]; then
        case "$OPTARG" in
          /*)
            DATA="$OPTARG"
            ;;
          *)
            DATA="$PWD/$OPTARG"
            ;;
        esac
      else
        echo "$OPTARG not found" >&2
        exit 1
      fi
      ;;
    j)
      TYPE="application/json"
      ;;
    v)
      VERBOSE=-v
      ;;
    g)
      METHOD=GET
      ;;
    p)
      METHOD=POST
      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND - 1))
[ $# -gt 0 ] || usage

EHURI="${EHURI%/}/`tr \  / <<< "$*"`"
EHAUTH="user = \"$EHAUTH\""

HFILE=`mktemp /tmp/elastic-headers-XXXXX`
RFILE=`mktemp /tmp/elastic-response-XXXXX`
CURLARGS=(-L -s -S $VERBOSE -D $HFILE ${TYPE:+-H "Accept: $TYPE"})
if [ "$EHAUTO" == "1" ]; then
  read -t 0 && DATA="-"
  if [ -z "$METHOD" ]; then
    while read HEADER VALUE; do
      VALUE=${VALUE/%?/} # strip \r
      [[ $HEADER == Access-Control-Allow-Methods: && $VALUE == @(POST|GET) ]] \
        && METHOD=$VALUE && break
    done <<<"`curl -L -s -S -K <(echo \"$EHAUTH\") -i -X OPTIONS \"$EHURI\"`"
  fi
  if [ -z "$METHOD" ]; then
    echo Could not automatically figure out which HTTP method to use. >&2
    echo Please pass the -g or -p arguments to force either. >&2
    exit 1
  fi
elif [ -z "$METHOD" ]; then
  [[ $DATA ]] && METHOD=POST || METHOD=GET
fi
if [ "$METHOD" == "GET" ]; then
  CURLARGS+=(-H "Content-Type:text/plain" --get)
else
  CURLARGS+=(-H "Content-Type: ${TYPE:-application/octet-stream}"
             -H "Expect:" --data-binary @"$DATA")
fi
curl -K <(echo "$EHAUTH") "${CURLARGS[@]}" "$EHURI" >$RFILE
STATUS=$?
if (( STATUS == 0 )); then
  read _ STATUS _ <$HFILE
  if (( STATUS >= 400 )); then
    STATUS=1
  else
    STATUS=0
  fi 2>/dev/null
fi
if (( STATUS > 0 )); then
  cat $RFILE >&2
  [[ ! $VERBOSE ]] && cat $HFILE >&2
else
  cat $RFILE
fi
rm -f $HFILE $RFILE
exit $STATUS
