#!/bin/bash
# Calculate compound percentage sequences

die() {
  cat << EOF
Usage: $0 START PERC [ITERATIONS]
Options
  START: Initial amount
  PERC: Percent increase
  ITERATIONS: How many iterations to perform (default 10)
EOF
  exit 1
}

# Param check
[[ $1 ]] || die
START=$1
[[ $2 ]] || die
PERC=$2
[[ $3 ]] && ITER=$3 || ITER=10

# Main loop
echo I:COMP:DIFF
I=0
let 'PREV = START'
while [[ $I -lt $ITER+1 ]]; do
  OLDPREV=$PREV
  PREV=$(bc -l <<< "scale = 2; $PREV + ($PERC / 100 ) * $PREV")
  DIFF=$(bc -l <<< "scale = 2; $PREV - $OLDPREV")

  echo $I : $PREV : $DIFF
  let I++
done
