#!/bin/sh
dpkg-query --show \
  --showformat='${Status} ${Package} ${Description}\n' \
  | grep 'ok installed' \
  | cut -d' ' -f4-
