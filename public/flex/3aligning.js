function changeAlignItems(e) {
  console.log(e.value);
  document.styleSheets[1].rules[4].style.alignItems = e.value;
  document.getElementById('align-items').innerHTML = e.value;
}
function changeFlexDirection(e) {
  console.log(e.value);
  document.styleSheets[1].rules[6].style.flexDirection = e.value;
  document.getElementById('flex-direction').innerHTML = e.value;
}
function changeAlignContent(e) {
  console.log(e.value);
  document.styleSheets[1].rules[10].style.alignContent = e.value;
  document.getElementById('align-content').innerHTML = e.value;
}
function changeAlignContentFlexDirection(e) {
  console.log(e.value);
  document.styleSheets[1].rules[10].style.flexDirection = e.value;
  document.getElementById('align-content-flex-direction').innerHTML = e.value;
}
function changeJustifyContent(e) {
  console.log(e.value);
  document.styleSheets[1].rules[12].style.justifyContent = e.value;
  document.getElementById('justify-content').innerHTML = e.value;
}
function changeJustifyContentFlexDirection(e) {
  console.log(e.value);
  document.styleSheets[1].rules[12].style.flexDirection = e.value;
  document.getElementById('justify-content-flex-direction').innerHTML = e.value;
}
