var express = require('express');
var app = express();
var port = 3000;
app.use(express.static('public'))
app.get('/', (req, res) => res.sendFile(__dirname + '/public/index.html'))

app.listen(3000, () => console.log('Listening on http://localhost:'+port))

