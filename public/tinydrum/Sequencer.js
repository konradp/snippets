'use strict';

class Sequencer {
// Methods:
// - StartPause()
// - Stop()
// - SetBpm(85)
// - SetSequence('HL--H-L')
constructor() {
  // Private
  this.time_now = 0
  this.pDelay = 0
  this.pTimeout = null
  this.state = 0
  this.pSeqLength = 16
  this.hitsPerBeat = 4
  this.notes_list = []
  this.SetBpm(85)
  this.player = new Player();
  this.player.load_sounds([
    [ 'H', 'sounds/sound1.ogg' ],
    [ 'L', 'sounds/sound2.ogg' ]
  ]);
}

//////////////// PUBLIC ////////////////////
/* TRANSPORT: Start/Stop/Pause */
StartPause() {
  if(!this.pTimeout) {
    // Start
    this.pTimeout = setInterval(
      this.pTick,
      this.pDelay
    )
  } else {
    // Pause
    clearInterval(this.pTimeout)
    this.pTimeout = null
  }
}

Stop() {
  clearInterval(this.pTimeout)
  this.pTimeout = null;
  self.state = 0;
}

/* Setters */
SetBpm(value) {
  // TODO: Setting BPM pauses the sequencer but does not resume afterwards
  if(this.pTimeout) clearInterval(this.pTimeout)
  if(value < 1) value = 1
  if(value > 500) value = 500

  // Note duration: ( 60 sec ) / BPM / sequenceLength
  this.StartPause();
  this.pDelay = (60.0 * 1000.0) / value / this.hitsPerBeat
  this.StartPause();
}

SetSequence(str) {
  var notes = new Array();
  const n = str.length
  for(let i = 0; i < n; i ++) {
    let ch = str[i];
    notes.push(ch)
  }
  this.notes_list = notes
}

//////////////// PRIVATE ////////////////////
// Note: From within callbacks, 'this' may not be what you expect
// i.e. it will not necessarily be 'this class', but can be e.g. 'this window'
// We need to get an instance of our class
pTick() {
  self = GetSeqInstance()
  let pos = self.state
  if(pos >= self.notes_list.length) pos = 0;
  self.state = pos + 1
  // TODO: Fix variable naming here. Also, 'var' or 'let'?
  var note = self.notes_list[pos]
  if (this.debug) console.log('NOTE IS', note);
  if (note != '-') {
    self.player.play(note)
  }
}

}; //class Sequencer
