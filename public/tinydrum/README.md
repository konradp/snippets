# HTML5 drum machine
<https://konradp.gitlab.io/tinydrum>

## What it does
It plays drum sounds in your web browser

Tech spec:
- javascript: ES6
- NodeJS
- sampled ogg (mp3) drum sounds
- HTML5 audio

## Install
Clone this project, and serve the page from your laptop.
```BASH
git clone https://gitlab.com/konradp/drum.git
cd drum
npm install
npm start
```
Navigate in web browser to http://localhost:3000/
