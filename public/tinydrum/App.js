console.log("Tinydrum");
// TODO: Should load sounds here also
var seq = new Sequencer();
seq.SetSequence("H-L-L-L-H-L-L-L-");

function GetSeqInstance() {
  return seq
}

// Call these functions from console for a demo
// Also, these are accessible from HTML via onclick=""
// TODO: Could be used by the front-end
function StartPause() {
  var sequence = document.getElementById('pattern');
  seq.SetSequence(sequence.value);
  seq.StartPause();
}
function Stop() {
  seq.Stop();
}
