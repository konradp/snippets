console.log('test');
// Music utils

const NOTE_NAMES = [ 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];

function getNoteFromMidi(midiNote) {
  // getNoteNameFromMidiNumber(60) = 'C4'
  return NOTE_NAMES[midiNote%12] + String(Math.floor(midiNote/12)-1);
}

function getMidiFromName(midiNote) {
  let octave = parseInt(midiNote.slice(-1));
  return octave;
}

// ref: https://www.music.mcgill.ca/~gary/307/week1/node28.html
function getFreqFromMidi(midiNote) {
  // 440*2^((n-69)/12)
  let n = midiNote;
  return 440*Math.pow(2, (n-69)/12);
}
