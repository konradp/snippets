import time
import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=5000)

def getGitCount():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

# MAIN
@app.route('/')
def hello():
    count = getHitCount()
    return 'Hello world. count: {} times.\n'.format(count)

if  __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)

