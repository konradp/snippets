#!/usr/bin/env python3
# Test Flask server
import json
import os
import sys
from flask import Flask, request

app = Flask(__name__)

# Config
header = {'Content-Type': 'application/json'}

## ROUTES
@app.route('/')
def main():
  return {
    'msg': 'Hello world',
  }

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=80)
