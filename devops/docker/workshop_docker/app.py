#!/usr/bin/env python3
# Test Flask server
import json
import os
import sys
from flask import Flask, request

app = Flask(__name__)

# Config
header = {'Content-Type': 'application/json'}

## ROUTES
# CI
@app.route('/')
def main():
  return json.dumps({
    'msg': 'Hello world',
  }),
  200,
  header

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=80)
