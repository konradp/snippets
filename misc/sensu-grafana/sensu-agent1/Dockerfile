FROM debian:latest

# Install prerequisite packages
RUN apt-get update
RUN apt-get -y install bash curl sudo

# Set up user
ARG USRNAME=testusr
ARG UID=1000
ARG GID=1000
RUN groupadd -g $GID $USRNAME
RUN useradd -m \
  -u $UID \
  -g $GID \
  -s /bin/bash \
  -G sudo \
  $USRNAME
RUN echo "$USRNAME ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/$USRNAME
RUN chmod 0440 /etc/sudoers.d/$USRNAME

# Switch to user
USER $USRNAME

# Install Sensu agent
RUN curl -s https://packagecloud.io/install/repositories/sensu/beta/script.deb.sh | sudo bash
RUN sudo apt-get -y install sensu-agent sensu-cli
COPY agent.yml /etc/sensu/agent.yml

# Start Sensu agent
# Note: Outside a container, use 'sudo systemctl start sensu-backend'
CMD ["/bin/bash", "-c", "sudo sensu-agent start 2> >(sudo tee -a /var/log/sensu/sensu.log >&2)" ]
