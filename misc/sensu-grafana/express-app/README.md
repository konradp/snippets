
Simple HelloWorld server using  
http://expressjs.com/en/starter/hello-world.html  
https://medium.com/@adnanrahic/hello-world-app-with-node-js-and-express-c1eb7cfa8a30

Dockerised as per  
https://github.com/nodejs/docker-node  
https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

```
npm install
npm start
```

http://localhost:49160/

```
docker ps
```
Then
```
docker exec -it <container id> /bin/bash
```
