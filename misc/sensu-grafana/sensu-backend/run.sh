#!/bin/bash
NAME="sensu-backend"
docker build -t `whoami`/$NAME .
docker run -p 49160:8080 -p 49161:8081 -t -d `whoami`/$NAME
