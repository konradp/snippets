# DockerCompose project

DockerCompose:
- Simple HelloWorld server in ExpressJS

Prerequisite:

```
sudo apt-get install docker-compose
```

Run app
```
docker-compose up -d
```

http://localhost:3000/

Stop the app.
```
docker compose stop
```

# Sensu prerequisites
Redis  
https://docs.sensu.io/sensu-core/2.0/installation/install-redis/

RabbitMQ  
https://docs.sensu.io/sensu-core/2.0/installation/install-rabbitmq/

Sensu clients  
https://docs.sensu.io/sensu-core/2.0/quick-start/client-installation/#install-the-sensu-client
