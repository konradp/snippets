# Snippets
Snippets in c++, bash etc.

https://konradp.gitlab.io/snippets

## HTML/CSS examples
```
npm start
```
http://localhost:8080/

## Other useful things
- i3 mod keys:
  - i3 UserGuide: Keyboard bindings
  - xmodmap on Arch wiki
  get: xev
  display: xmodmap
  display: xmodmap -pke
- keyboard backlight: `/sys/class/leds/tpacpi::kbd_backlight`

## Other
places to run cron:
- google cloud shell
  - 5gb
- oracle cloud shell
- azure cloud shell
- aws cloud shell
- alibaba cloud shell
