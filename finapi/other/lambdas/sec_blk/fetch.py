# Fetch 
import json
import pandas as pd
import requests

CIK = {
  "vanguard": "0000102909",
  "blackrock": "0001364742",
}

def fetch_filings(cik):
  user_agent_header = {
    "User-Agent": "Konrad Pisarczyk kpisarczyk@gmail.com",
    "Accept-Encoding": "gzip, deflate",
  }
  cik = str(cik).zfill(10)
  url = f"https://data.sec.gov/submissions/CIK{cik}.json"
  #print(url)
  res = requests.get(url=url, headers=user_agent_header)
  ret = res.json()
  ret = ret['filings']['recent']
  df = pd.DataFrame(ret)
  filtered_df = df[(df['filingDate'].str.startswith('2024')) & (df['form'] == 'SC 13G/A')]
  filtered = filtered_df.to_dict(orient='list')
  print(json.dumps(filtered))
  #print(json.dumps(ret))



def main():
  #print(CIK['blackrock'])
  fetch_filings(CIK['blackrock'])


if __name__ == "__main__":
  main()
