import json
import requests
from bs4 import BeautifulSoup
from datetime import datetime

FILE_DATA = "snp500.json"
URL = 'https://en.wikipedia.org/wiki/List_of_S%26P_500_companies'
URL_NTFY = "https://ntfy.sh/snp500chgs"

def send_notification(text):
  log(f"Notify: {text}")
  requests.post(URL_NTFY, data=text.encode(encoding='utf-8'))


def log(msg):
  now = datetime.now()
  now = now.strftime("%Y-%m-%d %H:%M")
  print(f"{now} {msg}")


def main():
  try:
    # Fetch
    response = requests.get(URL)
  except Exception as e:
    log(f"Error fetching data from url: {URL}: {e}")
    exit(1)

  try:
    # Process
    soup = BeautifulSoup(response.content, 'html.parser')
    table = soup.find('table', {'id': 'constituents'})
    headers = [th.text.strip() for th in table.find('tr').find_all('th')]
    rows = []
    for row in table.find_all('tr')[1:]:
      cells = [td.text.strip() for td in row.find_all('td')]
      if cells:
        rows.append(cells)
    data_new = [ i[0] for i in rows ]
    log(f"Fetched {len(data_new)} stocks")
  except Exception as e:
    log(f"Failed to process: {e}")
    exit(1)

  try:
    # Get file: prev list
    is_newfile = False
    with open(FILE_DATA, 'r') as f:
      data_old = json.load(f)
  except Exception:
    log(f"File {FILE_DATA} not found or invalid. Will create a new file.")
    is_newfile = True

  chgs = []
  if not is_newfile:
    # Compare files: Get added and removed
    chgs = [f"+{i}" for i in data_new if i not in data_old] \
            + [f"-{i}" for i in data_old if i not in data_new]
    if chgs:
      log(f"Changes: {chgs}")
    else:
      log("No changes")
    for chg in chgs:
      send_notification(chg)

  if chgs or is_newfile:
    # Update file
    content_new = json.dumps(data_new, indent=2)
    with open(FILE_DATA, 'w') as f:
      f.write(content_new)

if __name__ == '__main__':
  main()
