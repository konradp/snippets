zip -r ndq100.zip .

Market activity/quotes for NASDAQ-100 (NDX)
https://www.nasdaq.com/market-activity/quotes/nasdaq-ndx-index
https://www.nasdaq.com/market-activity/index/ndx
https://api.nasdaq.com/api/quote/list-type/nasdaq100

Run as
```
0 23 * * * /usr/bin/python3 snippets/finapi/other/lambdas/ndq100/ndq100_local.py >> ~/ndq100_cron.json

```
