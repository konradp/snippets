import json
import boto3
import requests

s3_client = boto3.client('s3')
s3_bucket = "bucket-konradp"
s3_file = "ndq100.json"
s3_file_hist = "ndq100_hist.json"
ntfy_url = "https://ntfy.sh/ndq100chgs"
user_agent_header = {
  "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) \
    AppleWebKit/537.36 (KHTML, like Gecko) \
    Chrome/129.0.0.0 Safari/537.36",
  "accept-language": "en-US,en;q=0.9",
}


def send_notification(text):
  print(f"Notify: {text}")
  requests.post(ntfy_url, data=text.encode(encoding='utf-8'))


def lambda_handler(event, context):
  try:
    print('Fetch ndq100')
    url = "https://api.nasdaq.com/api/quote/list-type/nasdaq100"
    #url = "https://konradp.gitlab.io/snippets/ndq100.json"
    response = requests.get(url, headers=user_agent_header)
    response.raise_for_status()
    data_new = response.json()
    data_new = data_new['data']['data']['rows']
    data_new = sorted([ i['symbol'] for i in data_new ])
    print(f"Fetched {len(data_new)} stocks")
  except requests.exceptions.RequestException as e:
    print(f"Error fetching data from {url}: {e}")
    return {
      'statusCode': 500,
      'body': json.dumps(f"Error fetching data: {str(e)}")
    }

  try:
    # Fetch file: prev list
    print('Fetch prev list')
    is_newfile = False
    s3_response = s3_client.get_object(Bucket=s3_bucket, Key=s3_file)
    content_old = s3_response['Body'].read().decode('utf-8')
    data_old = json.loads(content_old)
  except Exception:
    print(f"File {s3_file} not found in S3 or invalid. Will create a new file.")
    is_newfile = True

  if not is_newfile:
    # Compare files: Get added and removed
    chgs = [f"+{i}" for i in data_new if i not in data_old] \
            + [f"-{i}" for i in data_old if i not in data_new]
    if chgs:
      print(f"Changes: {chgs}")
    else:
      print("No changes")
    for chg in chgs:
      send_notification(chg)
    # TODO: Append the changes to a history file in S3

  if chgs or is_newfile:
    print("Update the file")
    content_new = json.dumps(data_new, indent=2)
    s3_client.put_object(Bucket=s3_bucket, Key=s3_file, Body=content_new)
    print(f"Uploaded to S3: {s3_bucket}/{s3_file}")

  return {
    'statusCode': 200,
    'body': json.dumps('File updated successfully')
  }
