# Script to run locally, without lambdas
import json
import requests
from datetime import datetime

FILE_DATA = "ndq100.json"
URL_NTFY = "https://ntfy.sh/ndq100chgs"
URL_NDQ = "https://api.nasdaq.com/api/quote/list-type/nasdaq100"
USER_AGENT_HEADER = {
  "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) \
    AppleWebKit/537.36 (KHTML, like Gecko) \
    Chrome/129.0.0.0 Safari/537.36",
  "accept-language": "en-US,en;q=0.9",
}


def send_notification(text):
  log(f"Notify: {text}")
  requests.post(URL_NTFY, data=text.encode(encoding='utf-8'))


def log(msg):
  now = datetime.now()
  now = now.strftime("%Y-%m-%d %H:%M")
  print(f"{now} {msg}")


def main():
  try:
    #log('Fetch ndq100')
    response = requests.get(URL_NDQ, headers=USER_AGENT_HEADER)
    response.raise_for_status()
    data_new = response.json()
    data_new = data_new['data']['data']['rows']
    data_new = sorted([ i['symbol'] for i in data_new ])
    log(f"Fetched {len(data_new)} stocks")
  except requests.exceptions.RequestException as e:
    log(f"Error fetching data from {url}: {e}")
    exit(1)

  try:
    # Get file: prev list
    #log('Fetch prev list')
    is_newfile = False
    with open(FILE_DATA, 'r') as f:
      data_old = json.load(f)
  except Exception:
    log(f"File {FILE_DATA} not found or invalid. Will create a new file.")
    is_newfile = True

  chgs = []
  if not is_newfile:
    # Compare files: Get added and removed
    chgs = [f"+{i}" for i in data_new if i not in data_old] \
            + [f"-{i}" for i in data_old if i not in data_new]
    if chgs:
      log(f"Changes: {chgs}")
    else:
      log("No changes")
    for chg in chgs:
      send_notification(chg)

  if chgs or is_newfile:
    #log("Update the file")
    content_new = json.dumps(data_new, indent=2)
    with open(FILE_DATA, 'w') as f:
      f.write(content_new)
    #log(f"Saved to {FILE_DATA}")


if __name__ == '__main__':
  main()
