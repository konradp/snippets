# finnhub

```
export APIKEY=...
curl "https://finnhub.io/api/v1/stock/symbol?exchange=US&token=$APIKEY" > symbols_us.json
```
this gives 28482 symbols

Note, there's a mic parameter, a MIC code:
```
$ jq -r '.[].mic' symbols_us.json | sort | uniq -c
   1979 ARCX: NYSE ARCA
    579 BATS: CBOE BZX U.S. EQUITIES EXCHANGE
      3 IEXG: INVESTORS EXCANGE
  16259 OOTC: OTHER OTC
    333 XASE: NYSE MKT LLC
   5737 XNAS: NASDAQ - ALL MARKETS
   3592 XNYS: NEW YORK STOCK EXCHANGE
```
ref: https://en.wikipedia.org/wiki/Market_Identifier_Code

of interest is the XNAS, so we can get NASDAQ symbols:
```
curl "https://finnhub.io/api/v1/stock/symbol?exchange=US&mic=XNAS&token=$APIKEY" > symbols_nasdaq.json
```
which gives 5737 symbols, of which types are:
```
$ jq -r '.[].type' symbols_nasdaq.json | sort | uniq -c | sort -n
      1 NY Reg Shrs
      1 Royalty Trst
      2 Ltd Part
      7 Tracking Stk
      9 MLP
     13 UIT
     14 Closed-End Fund
     29 REIT
     41 
     67 Right
    191 PUBLIC
    209 ADR
    435 Unit
    518 ETP
    688 Equity WRT
   3512 Common Stock
```
so we focus on Common Stocks

# Company profile2
```
$ python3 app.py 
{
  "country": "US",
  "currency": "USD",
  "exchange": "NASDAQ NMS - GLOBAL MARKET",
  "finnhubIndustry": "Retail",
  "ipo": "1997-05-15",
  "logo": "https://static2.finnhub.io/file/publicdatany/finnhubimage/stock_logo/AMZN.svg",
  "marketCapitalization": 917944.8412553756,
  "name": "Amazon.com Inc",
  "phone": "12062661000.0",
  "shareOutstanding": 10201.7,
  "ticker": "AMZN",
  "weburl": "https://www.amazon.com/"
}
$ python3 app.py 
{
  "country": "US",
  "currency": "USD",
  "exchange": "NASDAQ NMS - GLOBAL MARKET",
  "finnhubIndustry": "Technology",
  "ipo": "1980-12-12",
  "logo": "https://static2.finnhub.io/file/publicdatany/finnhubimage/stock_logo/AAPL.svg",
  "marketCapitalization": 2219182.454636543,
  "name": "Apple Inc",
  "phone": "14089961010.0",
  "shareOutstanding": 15908.1,
  "ticker": "AAPL",
  "weburl": "https://www.apple.com/"
}
```
