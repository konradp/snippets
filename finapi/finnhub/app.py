import finnhub
import json
import sys
from config import Config

def main():
  cfg = Config()
  api_key = cfg['finnhub']['api_key']
  finnhub_client = finnhub.Client(api_key=api_key)

  profile = finnhub_client.company_profile2(symbol='AMZN')
  print(json.dumps(profile, indent=2))


if __name__ == '__main__':
  sys.exit(main())
