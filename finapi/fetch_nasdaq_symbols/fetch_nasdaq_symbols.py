import json
import sys
import urllib.request

url = 'ftp://ftp.nasdaqtrader.com/symboldirectory/nasdaqlisted.txt'

def is_symbol_valid(symbol_data):
  # Exclude various/invalid companies, e.g. those whose names end with
  # '- Warrants'
  # Return true if symbol is valid, false if not
  if symbol_data['name'].endswith((
    '- Rights',
    '- Subunit',
    '- Subunits',
    '- Trust Preferred Securities',
    '- Unit',
    '- Units',
    '- Warrant',
    '- Warrants',
    )) \
    or '.' in symbol_data['symbol']:
    return False
  return True

def guess_type(stock_data):
  name = stock_data['name']
  stock_data['type'] = 'UNKNOWN'
  if name.endswith(' ETF'):
    # ETF
    stock_data['type'] = 'ETF'
  elif name.lower().endswith(' - common shares') \
    or name.lower().endswith(' - common stock') \
    or name.lower().endswith(' - ordinary share') \
    or name.lower().endswith(' - ordinary shares')  \
    or name.lower().endswith(' - class a common stock') \
    or name.lower().endswith(' - class a common shares') \
    or name.lower().endswith(' - class a common share') \
    or name.lower().endswith(' - class a ordinary shares') \
    or name.lower().endswith(' - class a ordinary share') \
    or name.lower().endswith(' - class a ordinary shre') \
    or name.lower().endswith(' - class a shares') \
    or name.lower().endswith(' -  ordinary shares, class a common stock'):
    # common stock (includes class A)
    stock_data['type'] = 'common stock'
    stock_data['name'] = name.split(' - ')[0]
  elif name.endswith(' - American Depositary Shares') \
    or name.endswith(' - American Depository Shares'):
    # American depositary shares
    stock_data['type'] = 'American depositary shares'
    stock_data['name'] = name.split(' - ')[0]
  elif name.endswith(' Fund'):
    stock_data['type'] = 'fund'
  return stock_data

def main():
  #with open('nasdaqlisted.txt') as f: # DEBUG
  with urllib.request.urlopen(url) as response:
    #debug_text = [line.rstrip('\n') for line in f][1:-1] # DEBUG
    symbols = []
    #for line in debug_text: # DEBUG
    for line in response.read().decode('utf-8').splitlines()[1:-1]:
      fields = line.split('|')
      symbol = {
        'symbol': fields[0],
        'name': fields[1],
      }
      if not is_symbol_valid(symbol):
        continue
      if fields[3] != 'N':
        # Skip test stocks
        continue
      symbol = guess_type(symbol)
      symbols.append(symbol)
    print(json.dumps(symbols))

if __name__ == '__main__':
  sys.exit(main())
