```
python3 fetch_nasdaq_symbols.py | jq '.'
```
gives
```
[
  {
    "symbol": "AACG",
    "name": "ATA Creativity Global - American Depositary Shares, each representing two common shares",
    "type": "UNKNOWN"
  },
  {
    "symbol": "AACI",
    "name": "Armada Acquisition Corp. I",
    "type": "common stock"
  },
  {
    "symbol": "AADI",
    "name": "Aadi Bioscience, Inc.",
    "type": "common stock"
  },
  {
    "symbol": "AADR",
    "name": "AdvisorShares Dorsey Wright ADR ETF",
    "type": "ETF"
[...]
```
also
```
$ python3 fetch_nasdaq_symbols.py | jq -r '.[].type' | sort | uniq -c
    168 American depositary shares
   3481 common stock
    441 ETF
     59 fund
    417 UNKNOWN
```
