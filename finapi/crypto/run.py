#!/usr/bin/env python3
import configparser
import json
import requests

cfg = configparser.ConfigParser()
cfg.read('config.ini')
key = cfg['main']['key']

url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
parameters = {
  'start':'1',
  'limit':'5000',
  'convert':'USD'
}
headers = {
  'Accepts': 'application/json',
  'X-CMC_PRO_API_KEY': key,
}

session = requests.Session()
session.headers.update(headers)

try:
  response = session.get(url, params=parameters)
  data = json.loads(response.text)
  print(json.dumps(data, indent=2))
except (ConnectionError, 
    requests.exceptions.Timeout,
    requests.exceptions.TooManyRedirects) \
as e:
  print(e)


